$(function () {
	var urlData = window.location;
	var baseurl = urlData.origin + "/";
	var section = urlData.pathname.split('/');
	switch (section[2]) {
		case 'keusiswa':
			switch (section[3]) {
				case 'his':
					var table = new Tabulator("#con", {
						height: 375, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
						ajaxURL: baseurl + "ks/data/his", //ajax URL
						ajaxConfig: "post",
						pagination: "local",
						paginationSize: 10,
						paginationSizeSelector: [10, 25, 50], //enable page size select element with these options
						// data:response, //assign data to table
						responsiveLayout: true,
						layout: "fitColumns", //fit columns to width of table (optional)
						columns: [ //Define Table Columns
							{ title: "Tanggal/Waktu Transaksi", field: "tgl_trans", headerFilter: "input", headerFilterPlaceholder: "Tanggal/Waktu Transaksi", },
							{ title: "Nama Siswa", field: "nama_pl", headerFilter: "input", headerFilterPlaceholder: "Nama", },
							{ title: "Sekolah Asal", field: "sch_name", headerFilter: "input", headerFilterPlaceholder: "Sekolah", },
							{ title: "Jenis Pembayaran", field: "tipe", headerFilter: "input", headerFilterPlaceholder: "Jenis Pembayaran", },
							{ title: "Nominal", field: "nominal", headerFilter: "input", headerFilterPlaceholder: "Nominal", },
							{ title: "Pembayaran Bulan", field: "bulan", headerFilter: "input", headerFilterPlaceholder: "Bulan", },
							{ title: "Keterangan Pembayaran", field: "ket_trns", headerFilter: "input", headerFilterPlaceholder: "Keterangan", },
							{ title: "Tahun Ajaran", field: "tahunajaran", headerFilter: "input", headerFilterPlaceholder: "Tahun Ajaran", },
						],
					});
					$('#refresh').on('click', function () {
						table.replaceData();
						toastr.success('Data disegarkan 😇', 'Status');
					});
					break;
				case 'list':
					
					break;
				case 'set':
					//Sekolah
					$.post(baseurl + "ks/data/sch",
						function (data) {
							$('#sekolah').on('change', function () {
								// console.log();
								//Siswa
								$.post(baseurl + "ks/data/siswa",{id:this.value},
									function (data) {
										// console.log(data)
										// let siswa = document.getElementById('sekolah');
										// $.each(data, function (indexInArray, valueOfElement) { 
										// 	let opsi = document.createElement('option')
										// 	opsi.setAttribute('value',valueOfElement.id_schplc)
										// 	opsi.append(valueOfElement.sch_name)
										// 	sekolah.append(opsi)
										// });
									},
									"json"
								);
							});
							let sekolah = document.getElementById('sekolah');
							$.each(data, function (indexInArray, valueOfElement) { 
								let opsi = document.createElement('option')
								opsi.setAttribute('value',valueOfElement.id_schplc)
								opsi.append(valueOfElement.sch_name)
								sekolah.append(opsi)
							});
							
							// console.log(data);
						},
						"json"
					);
					//Tahun Ajaran
					$.post(baseurl + "ks/data/tahun",
						function (data) {
							let tahun = document.getElementById('tahun');
							$.each(data, function (indexInArray, valueOfElement) { 
								let opsi = document.createElement('option')
								opsi.setAttribute('value',valueOfElement.id_tha)
								opsi.append(valueOfElement.tahunajaran)
								tahun.append(opsi)
							});
						},
						"json"
					);
					break;
				default:
					break;
			}
			break;

		default:
			break;
	}
});
