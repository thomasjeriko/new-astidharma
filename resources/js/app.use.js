$(document).ready(function () {
	var urlData = window.location;
	var baseurl = urlData.origin + "/";
    var ceks=$('li').find(".active").attr("data-menue");
    // console.log(ceks);
    toastr.options = {
        "positionClass": "toast-bottom-left",
        "preventDuplicates": true,
    };
    switch (ceks) {
        case 'sekolah':
            $.getJSON(baseurl+"json/kat",
                function (json2) {
                    var table = new Tabulator("#con", {
                        height:375, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
                        ajaxURL:baseurl+"json/sch", //ajax URL
                        ajaxConfig:"get",
                        pagination:"local",
                        paginationSize:10,
                        paginationSizeSelector:[10, 25, 50, 100], //enable page size select element with these options
                        // data:response, //assign data to table
                        reactiveData:true,
                        responsiveLayout:true,
                        layout:"fitColumns", //fit columns to width of table (optional)
                        columns:[ //Define Table Columns
                            {width:5,formatter:"rowSelection", titleFormatter:"rowSelection", align:"center", headerSort:false, cellClick:function(e, cell){
                                cell.getRow().toggleSelect();
                            }},
                            {title:"Sekolah", field:"sch_name",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari sekolah",},
                            {title:"Alamat", field:"sch_addrs",editor:"textarea",headerFilter:"input",headerFilterPlaceholder:"Cari Alamat",},
                            {title:"Kota", field:"kota",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Kota",},
                            {title:"Penanggung Jawab", field:"penjwb",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Penanggung Jawab",},
                            {title:"Kepala", field:"kepala",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Kepala Sekolah",},
                            {title:"Kategori", field:"sch_cat",editor:"select",
                                editorParams:{values:json2},
                                formatter:"lookup", formatterParams:json2,
                                headerFilter:true,
                                headerFilterParams:json2,
                                headerFilterPlaceholder:"Kategori",
                            },
                        ],
                        rowSelectionChanged:function(data, rows){
                            //update selected row counter on selection change
                            $("#seld").text(data.length);
                        },
                        cellEdited:function(cell){
                            $.ajax({
                                type: "POST",
                                url: baseurl+"jsonup/sch",
                                dataType: "json",
                                data: {id:cell.getData()},
                                success: function (ret) {
                                    // console.log(ret.message);
                                    toastr.options = {
                                        "positionClass": "toast-bottom-left",
                                        "preventDuplicates": true,
                                    };
                                    toastr.success(ret.message, 'Berhasil');
                                },
                                error:function (params) {
                                    // console.log(params);
                                    toastr.options = {
                                        "positionClass": "toast-bottom-left",
                                        "preventDuplicates": true,
                                    };
                                    toastr.error(params.responseJSON.message, 'Gagal');
                                },
                                // cache: false,
                                // contentType: false,
                                // processData: false
                            });
                        },
                    });
                    $('#hapus').on('click', function () {
                        var selected=table.getSelectedData();
                        var kirim=new Array();
                        $.each(selected, function (indexInArray, valueOfElement) {
                            var zs={};
                            zs={'id':valueOfElement.id_schplc,'sch':valueOfElement.sch_name};
                            kirim.push(zs);
                        });
                        // console.log(kirim);
                        $.ajax({
                            type: "POST",
                            url: baseurl+"jsonde/sch",
                            dataType: "json",
                            data: {id:kirim},
                            success: function (ret) {
                                var test1=ret.cek;
                                toastr.options.hideDuration="2000";
                                if('gagal' in test1){
                                    var bad=test1.gagal;
                                    if(bad.length>0){
                                        var list2='';
                                        $.each(bad, function (indexInArray, valueOfElement) { 
                                            list2+='<li>'+valueOfElement.data+'</li>';
                                        });
                                        toastr.error('Gagal menghapus data 😕'+'<br>'+'<ul>'+list2+'</ul>', 'Gagal');
                                    }else{
                                        console.log('');
                                    }
                                    
                                }
                                if('berhasil' in test1){
                                    var good=test1.berhasil;
                                    if(good.length>0){
                                        var list='';
                                        $.each(good, function (indexInArray, valueOfElement) { 
                                            list+='<li>'+valueOfElement.data+'</li>';
                                        });
                                        toastr.success('Data telah dihapus 😇'+'<br>'+'<ul>'+list+'</ul>', 'Berhasil'); 
                                    }else{
                                        console.log('');
                                    }
                                }
                                table.replaceData();
                            },
                            error:function (params) {
                                toastr.error(params.responseJSON.message, 'Gagal');
                            },
                            // cache: false,
                            // contentType: false,
                            // processData: false
                        });
                    });
                    $('#refresh').on('click', function () {
                        table.replaceData();
                        toastr.success('Data disegarkan 😇', 'Status'); 
                    });
                    $('form#add').submit(function (e) { 
                        e.preventDefault();
                        var plusdata=new FormData(this);
                        // console.log(plusdata);
                        $.ajax({
                            type: "POST",
                            url: baseurl+"jsonin/sch",
                            ContentType: 'application/json',
                            data: plusdata,
                            success: function (ret) {
                                // console.log(ret);
                                table.replaceData();
                            },
                            error:function (params) {
                                toastr.error(params.responseJSON.message, 'Gagal');
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    });
                }
            );
            break;
        case 'kelas':
            $.getJSON(baseurl+"json/scl",
                function (json2) {
                    var table = new Tabulator("#con", {
                        height:375, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
                        ajaxURL:baseurl+"json/kls", //ajax URL
                        ajaxConfig:"get",
                        pagination:"local",
                        paginationSize:10,
                        paginationSizeSelector:[10, 25, 50, 100], //enable page size select element with these options
                        // data:response, //assign data to table
                        reactiveData:true,
                        responsiveLayout:true,
                        layout:"fitColumns", //fit columns to width of table (optional)
                        columns:[ //Define Table Columns
                            {width:5,formatter:"rowSelection", titleFormatter:"rowSelection", align:"center", headerSort:false, cellClick:function(e, cell){
                                cell.getRow().toggleSelect();
                            }},
                            {title:"Nama Kelas", field:"kelas_ket",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Nama Kelas",},
                            {title:"Nama Sekolah", field:"id_sch",editor:"select",
                                editorParams:{values:json2},
                                formatter:"lookup", formatterParams:json2,
                                headerFilter:true,
                                headerFilterParams:json2,
                                headerFilterPlaceholder:"Nama Sekolah",
                            },
                        ],
                        rowSelectionChanged:function(data, rows){
                            //update selected row counter on selection change
                            $("#seld").text(data.length);
                        },
                        cellEdited:function(cell){
                            $.ajax({
                                type: "POST",
                                url: baseurl+"jsonup/kls",
                                dataType: "json",
                                data: {id:cell.getData()},
                                success: function (ret) {
                                    // console.log(ret.message);
                                    toastr.options = {
                                        "positionClass": "toast-bottom-left",
                                        "preventDuplicates": true,
                                    };
                                    toastr.success(ret.message, 'Berhasil');
                                },
                                error:function (params) {
                                    // console.log(params);
                                    toastr.options = {
                                        "positionClass": "toast-bottom-left",
                                        "preventDuplicates": true,
                                    };
                                    toastr.error(params.responseJSON.message, 'Gagal');
                                },
                                // cache: false,
                                // contentType: false,
                                // processData: false
                            });
                        },
                    });
                    $('#hapus').on('click', function () {
                        var selected=table.getSelectedData();
                        var kirim=new Array();
                        $.each(selected, function (indexInArray, valueOfElement) {
                            var zs={};
                            zs={'id':valueOfElement.id_kelas,'sch':valueOfElement.kelas_ket};
                            kirim.push(zs);
                        });
                        // console.log(kirim);
                        $.ajax({
                            type: "POST",
                            url: baseurl+"jsonde/kls",
                            dataType: "json",
                            data: {id:kirim},
                            success: function (ret) {
                                var test1=ret.cek;
                                toastr.options.hideDuration="2000";
                                if('gagal' in test1){
                                    var bad=test1.gagal;
                                    if(bad.length>0){
                                        var list2='';
                                        $.each(bad, function (indexInArray, valueOfElement) { 
                                            list2+='<li>'+valueOfElement.data+'</li>';
                                        });
                                        toastr.error('Gagal menghapus data 😕'+'<br>'+'<ul>'+list2+'</ul>', 'Gagal');
                                    }else{
                                        console.log('');
                                    }
                                    
                                }
                                if('berhasil' in test1){
                                    var good=test1.berhasil;
                                    if(good.length>0){
                                        var list='';
                                        $.each(good, function (indexInArray, valueOfElement) { 
                                            list+='<li>'+valueOfElement.data+'</li>';
                                        });
                                        toastr.success('Data telah dihapus 😇'+'<br>'+'<ul>'+list+'</ul>', 'Berhasil'); 
                                    }else{
                                        console.log('');
                                    }
                                }
                                table.replaceData();
                            },
                            error:function (params) {
                                toastr.error(params.responseJSON.message, 'Gagal');
                            },
                            // cache: false,
                            // contentType: false,
                            // processData: false
                        });
                    });
                    $('#refresh').on('click', function () {
                        table.replaceData();
                        toastr.success('Data disegarkan 😇', 'Status'); 
                    });
                    $('form#add').submit(function (e) { 
                        e.preventDefault();
                        var plusdata=new FormData(this);
                        // console.log(plusdata);
                        $.ajax({
                            type: "POST",
                            url: baseurl+"jsonin/kls",
                            ContentType: 'application/json',
                            data: plusdata,
                            success: function (ret) {
                                // console.log(ret);
                                table.replaceData();
                                toastr.success('Data ditambahkan 😇', 'Status'); 
                            },
                            error:function (params) {
                                toastr.error(params.responseJSON.message, 'Gagal');
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    });
                }
            );
            break;
        case 'tahun':
            var table = new Tabulator("#con", {
                height:375, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
                ajaxURL:baseurl+"json/th", //ajax URL
                ajaxConfig:"get",
                pagination:"local",
                paginationSize:10,
                paginationSizeSelector:[10, 25, 50, 100], //enable page size select element with these options
                // data:response, //assign data to table
                reactiveData:true,
                responsiveLayout:true,
                layout:"fitColumns", //fit columns to width of table (optional)
                columns:[ //Define Table Columns
                    {width:5,formatter:"rowSelection", titleFormatter:"rowSelection", align:"center", headerSort:false, cellClick:function(e, cell){
                        cell.getRow().toggleSelect();
                    }},
                    {title:"Tahun Ajaran", field:"tahunajaran",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Tahun Ajaran",},
                    {title:"Status", field:"status", editor:"select", 
                        editorParams:{values:{
                            "1":"Aktif",
                            "0":"Non-Aktif",
                        }},
                        formatter:"lookup", 
                        formatterParams:{
                            "1":"Aktif",
                            "0":"Non-Aktif",
                        },
                        headerFilter:true,
                        headerFilterParams:{
                            "1":"Aktif",
                            "0":"Non-Aktif",
                        },
                        headerFilterPlaceholder:"Status",
                    },
                ],
                rowSelectionChanged:function(data, rows){
                    //update selected row counter on selection change
                    $("#seld").text(data.length);
                },
                cellEdited:function(cell){
                    $.ajax({
                        type: "POST",
                        url: baseurl+"jsonup/th",
                        dataType: "json",
                        data: {id:cell.getData()},
                        success: function (ret) {
                            // console.log(ret.message);
                            toastr.success(ret.message, 'Berhasil');
                        },
                        error:function (params) {
                            // console.log(params);
                            toastr.error(params.responseJSON.message, 'Gagal');
                        },
                        // cache: false,
                        // contentType: false,
                        // processData: false
                    });
                },
            });
            $('#hapus').on('click', function () {
                var selected=table.getSelectedData();
                var kirim=new Array();
                $.each(selected, function (indexInArray, valueOfElement) {
                    var zs={};
                    zs={'id':valueOfElement.id_tha,'sch':valueOfElement.tahunajaran};
                    kirim.push(zs);
                });
                // console.log(kirim);
                $.ajax({
                    type: "POST",
                    url: baseurl+"jsonde/th",
                    dataType: "json",
                    data: {id:kirim},
                    success: function (ret) {
                        var test1=ret.cek;
                        toastr.options.hideDuration="2000";
                        if('gagal' in test1){
                            var bad=test1.gagal;
                            if(bad.length>0){
                                var list2='';
                                $.each(bad, function (indexInArray, valueOfElement) { 
                                    list2+='<li>'+valueOfElement.data+'</li>';
                                });
                                toastr.error('Gagal menghapus data 😕'+'<br>'+'<ul>'+list2+'</ul>', 'Gagal');
                            }else{
                                console.log('');
                            }
                            
                        }
                        if('berhasil' in test1){
                            var good=test1.berhasil;
                            if(good.length>0){
                                var list='';
                                $.each(good, function (indexInArray, valueOfElement) { 
                                    list+='<li>'+valueOfElement.data+'</li>';
                                });
                                toastr.success('Data telah dihapus 😇'+'<br>'+'<ul>'+list+'</ul>', 'Berhasil'); 
                            }else{
                                console.log('');
                            }
                        }
                        table.replaceData();
                    },
                    error:function (params) {
                        toastr.error(params.responseJSON.message, 'Gagal');
                    },
                    // cache: false,
                    // contentType: false,
                    // processData: false
                });
            });
            $('#refresh').on('click', function () {
                table.replaceData();
                toastr.success('Data disegarkan 😇', 'Status'); 
            });
            $('form#add').submit(function (e) { 
                e.preventDefault();
                var plusdata=new FormData(this);
                // console.log(plusdata);
                $.ajax({
                    type: "POST",
                    url: baseurl+"jsonin/th",
                    ContentType: 'application/json',
                    data: plusdata,
                    success: function (ret) {
                        // console.log(ret);
                        table.replaceData();
                        toastr.success('Data ditambahkan 😇', 'Status'); 
                    },
                    error:function (params) {
                        toastr.error(params.responseJSON.message, 'Gagal');
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
            break;
        case 'siswa':
            $.getJSON(baseurl+"json/scl",
                function (json2) {
                    var table = new Tabulator("#con", {
                        height:375, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
                        ajaxURL:baseurl+"json/siswa", //ajax URL
                        ajaxConfig:"get",
                        pagination:"local",
                        paginationSize:10,
                        paginationSizeSelector:[10, 25, 50, 100], //enable page size select element with these options
                        // data:response, //assign data to table
                        reactiveData:true,
                        responsiveLayout:true,
                        layout:"fitColumns", //fit columns to width of table (optional)
                        columns:[ //Define Table Columns
                            {width:5,formatter:"rowSelection", titleFormatter:"rowSelection",
                                align:"center", headerSort:false, 
                                cellClick:function(e, cell){
                                    cell.getRow().toggleSelect();
                                }
                            },
                            {title:"NISN", field:"nisn_pl",editor:"number",headerFilter:"input",headerFilterPlaceholder:"Cari NISN",},
                            {title:"Nama Siswa", field:"nama_pl",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Nama Siswa",},
                            {title:"Jenis Kelamin", field:"jk_pl",editor:"select",
                                editorParams:{values:{
                                    "P":"Perempuan",
                                    "L":"Laki-laki",
                                }},
                                formatter:"lookup", 
                                formatterParams:{
                                    "P":"Perempuan",
                                    "L":"Laki-laki",
                                },
                                headerFilter:true,
                                headerFilterParams:{
                                    "P":"Perempuan",
                                    "L":"Laki-laki",
                                },
                                headerFilterPlaceholder:"Jenis Kelamin",
                            },
                            {title:"Alamat", field:"alamat_pl",editor:"textarea",headerFilter:"input",headerFilterPlaceholder:"Cari Alamat",},
                            {title:"Tahun Masuk", field:"tahun_masuk",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Tahun Masuk",},
                            {title:"Tahun Keluar", field:"tahun_keluar",editor:"input",headerFilter:"input",headerFilterPlaceholder:"Cari Tahun Keluar",},
                            {title:"Nama Sekolah", field:"id_schp",editor:"select",
                                editorParams:{values:json2},
                                formatter:"lookup", formatterParams:json2,
                                headerFilter:true,
                                headerFilterParams:json2,
                                headerFilterPlaceholder:"Nama Sekolah",
                            },
                            {title:"Status", field:"status", editor:"select", 
                                editorParams:{values:{
                                    "1":"Aktif",
                                    "0":"Non-Aktif",
                                }},
                                formatter:"lookup", 
                                formatterParams:{
                                    "1":"Aktif",
                                    "0":"Non-Aktif",
                                },
                                headerFilter:true,
                                headerFilterParams:{
                                    "1":"Aktif",
                                    "0":"Non-Aktif",
                                },
                                headerFilterPlaceholder:"Status",
                            },
                        ],
                        rowSelectionChanged:function(data, rows){
                            $("#seld").text(data.length);
                        },
                        cellEdited:function(cell){
                            $.ajax({
                                type: "POST",
                                url: baseurl+"jsonup/siswa",
                                dataType: "json",
                                data: {id:cell.getData()},
                                success: function (ret) {
                                    // console.log(ret.message);
                                    toastr.success(ret.message, 'Berhasil');
                                },
                                error:function (params) {
                                    // console.log(params);
                                    toastr.error(params.responseJSON.message, 'Gagal');
                                },
                                // cache: false,
                                // contentType: false,
                                // processData: false
                            });
                        },
                    });
                    $('#hapus').on('click', function () {
                        var selected=table.getSelectedData();
                        var kirim=new Array();
                        $.each(selected, function (indexInArray, valueOfElement) {
                            var zs={};
                            zs={'id':valueOfElement.id_pl,'sch':valueOfElement.nama_pl};
                            kirim.push(zs);
                        });
                        // console.log(kirim);
                        $.ajax({
                            type: "POST",
                            url: baseurl+"jsonde/siswa",
                            dataType: "json",
                            data: {id:kirim},
                            success: function (ret) {
                                var test1=ret.cek;
                                toastr.options.hideDuration="2000";
                                if('gagal' in test1){
                                    var bad=test1.gagal;
                                    if(bad.length>0){
                                        var list2='';
                                        $.each(bad, function (indexInArray, valueOfElement) { 
                                            list2+='<li>'+valueOfElement.data+'</li>';
                                        });
                                        toastr.error('Gagal menghapus data 😕'+'<br>'+'<ul>'+list2+'</ul>', 'Gagal');
                                    }else{
                                        console.log('');
                                    }
                                    
                                }
                                if('berhasil' in test1){
                                    var good=test1.berhasil;
                                    if(good.length>0){
                                        var list='';
                                        $.each(good, function (indexInArray, valueOfElement) { 
                                            list+='<li>'+valueOfElement.data+'</li>';
                                        });
                                        toastr.success('Data telah dihapus 😇'+'<br>'+'<ul>'+list+'</ul>', 'Berhasil'); 
                                    }else{
                                        console.log('');
                                    }
                                }
                                table.replaceData();
                            },
                            error:function (params) {
                                toastr.error(params.responseJSON.message, 'Gagal');
                            },
                            // cache: false,
                            // contentType: false,
                            // processData: false
                        });
                    });
                    $('#refresh').on('click', function () {
                        table.replaceData();
                        toastr.success('Data disegarkan 😇', 'Status'); 
                    });
                    $('form#add').submit(function (e) { 
                        e.preventDefault();
                        var plusdata=new FormData(this);
                        // console.log(plusdata);
                        $.ajax({
                            type: "POST",
                            url: baseurl+"jsonin/siswa",
                            ContentType: 'application/json',
                            data: plusdata,
                            success: function (ret) {
                                // console.log(ret);
                                table.replaceData();
                                toastr.success('Data ditambahkan 😇', 'Status'); 
                            },
                            error:function (xhr, status, error) {
                                if(error){
                                    toastr.error(error, 'Gagal');
                                }else{
                                    toastr.error(xhr.responseJSON.message, 'Gagal');
                                }
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    });
                }
            );
            break;
        default:
            break;
    }
});
