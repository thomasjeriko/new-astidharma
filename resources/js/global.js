$(document).ready(function() {
	
	if( $('.has-datetimepicker').length ) 
	{
		$('.has-datetimepicker').datetimepicker();
	}
	
	if( $('.has-datepicker').length )
	{
		$('.has-datepicker').datetimepicker({format: 'DD/MM/YYYY'});
	} 
	$('#logout').on('click', function () {
		conf();
	});
	$('#logout1').on('click', function () {
		conf();
	});
});

function conf() {
	$.confirm({
		title: "Konfirmasi Logout",
		content: "Apakah anda yakin ingin keluar ?",
		type: 'red',
		typeAnimated: true,
		buttons: {
			confirm: {
				text: "Ya, Logout",
				btnClass: "btn-red",
				action: function(){
					$.ajax({
						type: "POST",
						url: baseurl+'logout',
						success: function (response) {
							window.location.replace(baseurl);
						},
						error:function(xhr, ajaxOptions, thrownError){
							window.location.replace(baseurl);
						}
					});
				}
			},
			close: function () {
			}
		}
	});
}