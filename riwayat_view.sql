-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 22 Jul 2020 pada 06.30
-- Versi server: 5.7.19
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yastidhar_test`
--

-- --------------------------------------------------------

--
-- Struktur untuk view `riwayat`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `riwayat`  AS  select `pembayaran_siswa`.`idbayar` AS `idbayar`,`pembayaran_siswa`.`tipe` AS `tipe`,`pembayaran_siswa`.`idsiswa` AS `idsiswa`,`pembayaran_siswa`.`nominal` AS `nominal`,`pembayaran_siswa`.`ket_trns` AS `ket_trns`,`pembayaran_siswa`.`tgl_trans` AS `tgl_trans`,`pelajar_list`.`nama_pl` AS `nama_pl`,`pelajar_list`.`nisn_pl` AS `nisn_pl`,`sch_plc`.`sch_name` AS `sch_name`,`sch_category`.`cat_name` AS `cat_name`,`tahun_ajaran`.`tahunajaran` AS `tahunajaran`,`bulan`.`bulan` AS `bulan` from (((((`pembayaran_siswa` left join `pelajar_list` on((`pembayaran_siswa`.`idsiswa` = `pelajar_list`.`id_pl`))) left join `sch_plc` on((`pelajar_list`.`id_schp` = `sch_plc`.`id_schplc`))) left join `sch_category` on((`sch_plc`.`sch_cat` = `sch_category`.`id_schcat`))) join `tahun_ajaran` on((`pembayaran_siswa`.`th_ajaran` = `tahun_ajaran`.`id_tha`))) join `bulan` on((`pembayaran_siswa`.`bulan` = `bulan`.`realmoon`))) order by `pembayaran_siswa`.`idbayar` desc ;

--
-- VIEW  `riwayat`
-- Data: Tidak ada
--

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
