-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jul 2020 pada 08.00
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yastidhar_test`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ad_list`
--

CREATE TABLE `ad_list` (
  `id_adl` int(11) NOT NULL,
  `adl_name` varchar(32) NOT NULL,
  `adl_dwp` varchar(64) NOT NULL,
  `adl_leor` int(11) NOT NULL,
  `adl_sch` bigint(20) NOT NULL,
  `pp` text DEFAULT NULL,
  `suser` int(1) NOT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ad_list`
--

INSERT INTO `ad_list` (`id_adl`, `adl_name`, `adl_dwp`, `adl_leor`, `adl_sch`, `pp`, `suser`, `parent`) VALUES
(1, 'amin', 'kristussertakami', 1, 10, '', 1, NULL),
(48, 'SDPiusWonosobo', 'juleschevalier15', 3, 44, NULL, 1, 36),
(47, 'TKPiusWonosobo', 'juleschevalier14', 3, 43, NULL, 1, 36),
(46, 'SDSantaBulu', 'juleschevalier13', 3, 42, NULL, 1, 36),
(45, 'TKAdeBulu', 'juleschevalier12', 3, 41, NULL, 1, 36),
(44, 'SMPPiusPemalang', 'juleschevalier11', 3, 40, 'smp pius.jpeg', 1, 36),
(43, 'SDPiusPemalang', 'juleschevalier10', 3, 39, NULL, 1, 36),
(39, 'SMPPiusTegal', 'juleschevalier6', 3, 35, NULL, 1, 36),
(38, 'SDPiusTegal', 'juleschevalier5', 3, 34, NULL, 1, 36),
(63, 'AstiDharmaCabangCilacap', 'juleschevalier29', 2, 58, NULL, 1, NULL),
(36, 'AstiDharmaTegal', 'juleschevalier3', 2, 32, NULL, 1, NULL),
(42, 'TKPiusPemalang', 'juleschevalier9', 3, 38, NULL, 1, 36),
(41, 'SMKPiusTegal', 'juleschevalier8', 3, 37, 'LOGO SMK PIUS.jpg', 1, 36),
(40, 'SMAPiusTegal', 'juleschevalier7', 3, 36, 'SMA PIUS TEGAL.jpg', 1, 36),
(35, 'AstiDharmaBogor', 'juleschevalier2', 2, 31, NULL, 1, NULL),
(34, 'AstiDharmaPusat', 'juleschevalier1', 1, 10, NULL, 1, NULL),
(37, 'TKPiusTegal', 'juleschevalier4', 3, 33, NULL, 1, 36),
(49, 'TKMariaPurworejo', 'juleschevalier16', 3, 45, NULL, 1, 36),
(50, 'SDMariaPuworejo', 'juleschevalier17', 3, 46, NULL, 1, 36),
(51, 'TKPiusCilacap', 'juleschevalier18', 3, 47, NULL, 1, 63),
(52, 'SDPiusCilacap', 'juleschevalier19', 3, 48, NULL, 1, 63),
(53, 'SMPPiusCilacap', 'juleschevalier20', 3, 49, NULL, 1, 63),
(54, 'TKBundaGrogol', 'juleschevalier21', 3, 50, NULL, 1, NULL),
(55, 'SDBundaGrogol', 'juleschevalier22', 3, 51, NULL, 1, NULL),
(56, 'SMPBundaGrogol', 'juleschevalier23', 3, 52, NULL, 1, NULL),
(57, 'SMABundaGrogol', 'juleschevalier24', 3, 53, NULL, 1, NULL),
(58, 'TKBundaWisata', 'juleschevalier25', 3, 54, NULL, 1, 35),
(59, 'SDBundaWisata', 'juleschevalier26', 3, 55, NULL, 1, 35),
(60, 'SMPBundaWisata', 'juleschevalier27', 3, 56, NULL, 1, 35),
(61, 'SMABundaWisata', 'juleschevalier28', 3, 57, NULL, 1, 35);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ad_role`
--

CREATE TABLE `ad_role` (
  `id_adr` int(11) NOT NULL,
  `adr_name` varchar(32) NOT NULL,
  `adr_rlcd` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ad_role`
--

INSERT INTO `ad_role` (`id_adr`, `adr_name`, `adr_rlcd`) VALUES
(1, 'Super Admin', 'PST'),
(2, 'Cabang', 'CBG'),
(3, 'Sekolah', 'SKL');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bagainformlr`
--

CREATE TABLE `bagainformlr` (
  `id_bagian` int(11) NOT NULL,
  `ketbagian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bagainformlr`
--

INSERT INTO `bagainformlr` (`id_bagian`, `ketbagian`) VALUES
(1, 'PENDAPATAN'),
(2, 'BIAYA - BIAYA USAHA'),
(3, 'SURPLUS ( DEFISIT )  TAHUN'),
(4, 'SISA LEBIH TAHUN'),
(5, 'Berdasarkan SK Dirjen Pajak Nomor : PER - 44/PJ/2008, taggal 24 Juli 2009 sisa lebih tersebut akan dipergunakan untuk pembangunan gedung dan prasarana pendidikan dan / atau penelitan dan pengembangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku_akun`
--

CREATE TABLE `buku_akun` (
  `id_ops` bigint(20) NOT NULL,
  `ket_ops` text NOT NULL,
  `kode_spek` varchar(20) DEFAULT NULL,
  `kode_bukti` varchar(20) DEFAULT NULL,
  `tipe_akun` int(11) NOT NULL,
  `nominal` double NOT NULL,
  `tipe_buku` int(11) NOT NULL,
  `wktu_input` date NOT NULL,
  `id_schp` bigint(20) NOT NULL,
  `userin` text NOT NULL,
  `real_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku_akun`
--

INSERT INTO `buku_akun` (`id_ops`, `ket_ops`, `kode_spek`, `kode_bukti`, `tipe_akun`, `nominal`, `tipe_buku`, `wktu_input`, `id_schp`, `userin`, `real_time`) VALUES
(95, 'Saldo Bulan August 2019', 'M00', '00', 1, 0, 1, '2019-10-01', 10, 'AstiDharmaPusat', '2019-08-26 09:10:55'),
(96, 'Saldo Bulan January 1970', 'M00', '00', 1, 0, 1, '1970-02-01', 10, 'amin', '2019-12-03 11:38:12'),
(97, 'Saldo Bulan July 2020', 'M00', '00', 1, 0, 2, '2020-08-01', 37, 'amin', '2020-07-22 11:27:01'),
(98, 'Kertas A4 SIDU', 'K07', 'A9097709', 2, 50000, 1, '2020-07-10', 10, 'amin', '2020-07-22 11:50:20'),
(99, 'Ballpoint', 'K06', 'B09299', 2, 10000, 1, '2020-07-08', 10, 'amin', '2020-07-22 11:52:52'),
(100, 'Alkohol', 'Z111', 'zzzz', 2, 1000000, 1, '2020-07-13', 10, 'amin', '2020-07-22 12:01:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bulan`
--

CREATE TABLE `bulan` (
  `id_bulan` int(11) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `realmoon` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bulan`
--

INSERT INTO `bulan` (`id_bulan`, `bulan`, `realmoon`) VALUES
(1, 'Juli', 7),
(2, 'Agustus', 8),
(3, 'September', 9),
(4, 'Oktober', 10),
(5, 'November', 11),
(6, 'Desember', 12),
(7, 'Januari', 1),
(8, 'Februari', 2),
(9, 'Maret', 3),
(10, 'April', 4),
(11, 'Mei', 5),
(12, 'Juni', 6),
(0, 'Kosong', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_bantuan`
--

CREATE TABLE `detail_bantuan` (
  `id_dbn` bigint(20) NOT NULL,
  `id_bantuan` bigint(20) NOT NULL,
  `nominal_tahap` double NOT NULL,
  `id_bulan` int(11) NOT NULL,
  `waktutambah` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dpp_1`
--

CREATE TABLE `dpp_1` (
  `id_dpp` bigint(20) NOT NULL,
  `id_siswa` bigint(20) NOT NULL,
  `nom_dpp` double NOT NULL,
  `status_angs` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dpp_1`
--

INSERT INTO `dpp_1` (`id_dpp`, `id_siswa`, `nom_dpp`, `status_angs`) VALUES
(1, 134, 3500000, 0),
(2, 135, 3500000, 0),
(3, 136, 3500000, 0),
(4, 137, 3500000, 0),
(5, 138, 4000000, 0),
(6, 139, 4000000, 0),
(7, 140, 1000000, 0),
(8, 141, 4000000, 0),
(9, 142, 3500000, 0),
(10, 143, 3000000, 0),
(11, 144, 350000, 0),
(12, 145, 3000000, 0),
(13, 146, 4000000, 0),
(14, 147, 3750000, 0),
(15, 148, 3500000, 0),
(16, 149, 3250000, 0),
(17, 150, 1000000, 0),
(18, 151, 3000000, 0),
(19, 152, 3500000, 0),
(20, 153, 4000000, 0),
(21, 154, 3500000, 0),
(22, 155, 3500000, 0),
(23, 156, 4500000, 0),
(24, 157, 3000000, 0),
(25, 158, 5000000, 0),
(26, 106, 0, 0),
(27, 107, 3000000, 0),
(28, 108, 2500000, 0),
(29, 109, 3000000, 0),
(30, 110, 35000000, 0),
(31, 111, 3500000, 0),
(32, 112, 3000000, 0),
(33, 113, 4500000, 0),
(34, 114, 4500000, 0),
(35, 115, 3000000, 0),
(36, 116, 3500000, 0),
(37, 117, 3500000, 0),
(38, 118, 3500000, 0),
(39, 119, 4000000, 0),
(40, 120, 300000, 0),
(41, 121, 3500000, 0),
(42, 122, 3000000, 0),
(43, 123, 4000000, 0),
(44, 124, 4000000, 0),
(45, 125, 3500000, 0),
(46, 126, 1000000, 0),
(47, 127, 3500000, 0),
(48, 128, 3500000, 0),
(49, 129, 4000000, 0),
(50, 130, 4000000, 0),
(51, 131, 3000000, 0),
(52, 132, 3500000, 0),
(53, 133, 3500000, 0),
(54, 159, 3500000, 0),
(55, 160, 3750000, 0),
(56, 161, 3250000, 0),
(57, 162, 300000, 0),
(58, 163, 3500000, 0),
(59, 164, 3250000, 0),
(60, 165, 3500000, 0),
(61, 166, 2500000, 0),
(62, 167, 3000000, 0),
(63, 168, 3500000, 0),
(64, 169, 3000000, 0),
(65, 170, 3000000, 0),
(66, 171, 3500000, 0),
(67, 172, 3750000, 0),
(68, 173, 2500000, 0),
(69, 174, 3000000, 0),
(70, 175, 3500000, 0),
(71, 176, 3000000, 0),
(72, 177, 300000, 0),
(73, 178, 3300000, 0),
(74, 179, 4500000, 0),
(75, 180, 3250000, 0),
(76, 181, 4500000, 0),
(77, 182, 4000000, 0),
(78, 183, 4500000, 0),
(79, 184, 4000000, 0),
(80, 185, 3250000, 0),
(81, 186, 3500000, 0),
(82, 187, 4500000, 0),
(83, 188, 3500000, 0),
(84, 190, 3500000, 0),
(85, 191, 3500000, 0),
(86, 193, 4000000, 0),
(87, 194, 2500000, 0),
(88, 196, 3000000, 0),
(89, 197, 3500000, 0),
(90, 198, 3000000, 0),
(91, 199, 3000000, 0),
(92, 201, 3500000, 0),
(93, 202, 4500000, 0),
(94, 203, 4500000, 0),
(95, 204, 2500000, 0),
(96, 205, 4000000, 0),
(97, 206, 3000000, 0),
(98, 207, 2500000, 0),
(99, 208, 4500000, 0),
(100, 209, 3250000, 0),
(101, 211, 3500000, 0),
(102, 216, 3250000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dpp_2`
--

CREATE TABLE `dpp_2` (
  `id_dpp2` bigint(20) NOT NULL,
  `id_dpp` bigint(20) NOT NULL,
  `angs_dpp` double NOT NULL,
  `ket_ang` text NOT NULL,
  `tgl_pembayaran` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_lr`
--

CREATE TABLE `form_lr` (
  `id_formlr` int(11) NOT NULL,
  `id_bagian` int(11) NOT NULL,
  `isiformlr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `form_lr`
--

INSERT INTO `form_lr` (`id_formlr`, `id_bagian`, `isiformlr`) VALUES
(1, 1, '1. Dana Penyelengaraan Pendidikan'),
(2, 1, '2. Uang Pendaftaran'),
(3, 1, '3. Uang Sekolah / SPP'),
(4, 1, '4. Bunga Deposito / Jasa Giro / Tabungan'),
(5, 1, '5. Sumbangan lain-lain'),
(6, 1, '6. Subsidi PNS / DPK'),
(7, 1, '7. Laba Penjualan Mobil'),
(8, 2, '1. Beban Gaji Yayasan'),
(9, 2, '2. Iuran Pensiun'),
(10, 2, '3. Beban Gaji PNS'),
(11, 2, '4. Beban Penyusutan'),
(12, 2, '5. Beban PPh 21 dan  PPh 23'),
(13, 2, '6. Beban Pajak Bumi dan Bangunan'),
(14, 2, '7. Pemeliharaan Gedung'),
(15, 2, '8. Pemeliharaan Inventaris'),
(16, 2, '9. Pemeliharaan Kendaraan'),
(17, 2, '10. Keperluan Kantor'),
(18, 2, '11. Beban Listrik dan Air'),
(19, 2, '12. Majalah'),
(20, 2, '13. Outsourcing ( satpam )'),
(21, 2, '14. Pengembangan SDM / Study'),
(22, 2, '15. Transport Dinas'),
(23, 2, '16. Telephone'),
(24, 2, '17. Rapat / Lokakarya'),
(25, 2, '18. Sarana Penyelenggaraan Pendidikan'),
(26, 2, '19. Pelatihan / Pengembangan Siswa'),
(27, 2, '20. Iuran'),
(28, 2, '21. Solidari'),
(29, 2, '22. Koreksi Penyusutan Tahun lalu'),
(30, 2, '23. Penyusutan aktiva tetap yang berasal dari Dana Pembangunan Gedung & Prasarana Pendidikan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kdspek`
--

CREATE TABLE `kdspek` (
  `idspek` int(11) NOT NULL,
  `ketspek` text NOT NULL,
  `kdspek` text NOT NULL,
  `kategoriSpek` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kdspek`
--

INSERT INTO `kdspek` (`idspek`, `ketspek`, `kdspek`, `kategoriSpek`) VALUES
(0, 'SALDO SISA', 'M00', 'M'),
(1, 'SPP', 'M01', NULL),
(2, 'DPP', 'M02', 'M'),
(3, 'KEGIATAN', 'M03', NULL),
(4, 'BANTUAN PEMERINTAH (BOS)', 'M04', NULL),
(5, 'BANTUAN PEMERINTAH (DAK)', 'M05', NULL),
(6, 'BANTUAN PEMERINTAH ( BEASISIWA)', 'M06', NULL),
(7, 'BANTUAN PEMERINTAH ( LAIN-LAIN)', 'M07', NULL),
(8, 'BANTUAN LEMBAGA NON PEMERINTAH', 'M08', NULL),
(9, 'BANTUAN DONATUR (PERORANGAN)', 'M09', NULL),
(10, 'POS SILANG YAYASAN', 'M10', NULL),
(11, 'PENGAMBILAN DI BANK', 'M11', NULL),
(12, 'PENCAIRAN DEPOSITO', 'M12', NULL),
(13, 'Gaji dan tunjangan guru', 'K01', NULL),
(14, 'Gaji dan tunjangan tenaga kependidikan', 'K02', NULL),
(15, 'Biaya pengembangan guru dan tenaga kependidikan', 'K03', NULL),
(16, 'Kegiatan pembelajaran', 'K04', NULL),
(17, 'Kegiatan kesiswaan', 'K05', NULL),
(18, 'Alat tulis sekolah', 'K06', NULL),
(19, 'Bahan habis pakai', 'K07', NULL),
(20, 'Alat habis pakai', 'K08', NULL),
(21, 'Kegiatan rapat', 'K09', NULL),
(22, 'Transport dan perjalanan dinas', 'K10', NULL),
(23, 'Penggandaan soal-soal ulangan/ ujian', 'K11', NULL),
(24, 'Daya dan jasa', 'K12', NULL),
(25, 'Sosial', 'K13', NULL),
(26, 'Pemeliharaan', 'K14', NULL),
(27, 'Pengadaan Inventaris', 'K15', NULL),
(28, 'Pengembangan Kurikulum', 'K16', NULL),
(29, 'Pengembangan Proses pembelajaran', 'K17', NULL),
(30, 'Pengembangan Kompetensi Lulusan', 'K18', NULL),
(31, 'Pengembangan Pendidik dan Tenaga Kependidikan', 'K19', NULL),
(32, 'Pengembangan Sarana dan Prasarana', 'K20', NULL),
(33, 'Pengembangan Manajemen Sekolah', 'K21', NULL),
(34, 'Pengembangan Pembiayaan', 'K22', NULL),
(35, 'Pengembangan Penilaian', 'K23', NULL),
(36, 'Ditabung dibank', 'K24', NULL),
(37, 'Foya-foya', 'Z111', 'K');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kd_lr`
--

CREATE TABLE `kd_lr` (
  `id_lr` int(11) NOT NULL,
  `id_formlr` text NOT NULL,
  `id_spek` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kd_lr`
--

INSERT INTO `kd_lr` (`id_lr`, `id_formlr`, `id_spek`) VALUES
(1, 'K01', 'B1'),
(2, 'K02', 'B1'),
(3, 'K02', 'B13'),
(4, 'K03', 'B14'),
(5, 'K04', 'B18'),
(6, 'K05', 'B19'),
(7, 'K06', 'B10'),
(8, 'K07', 'B10'),
(9, 'K08', 'B10'),
(10, 'K09', 'B17'),
(11, 'K10', 'B15'),
(12, 'K11', 'B18'),
(13, 'K12', 'B11'),
(14, 'K12', 'B12'),
(15, 'K12', 'B16'),
(16, 'K13', 'B20'),
(17, 'K13', 'B21'),
(18, 'K14', 'B7'),
(19, 'K14', 'B8'),
(20, 'K14', 'B9'),
(21, 'K15', 'B18'),
(22, 'K16', 'B18'),
(23, 'K17', 'B18'),
(24, 'K18', 'B18'),
(25, 'K19', 'B18'),
(26, 'K20', 'B18'),
(27, 'K21', 'B18'),
(28, 'K22', 'B18'),
(29, 'K23', 'B18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kd_pemasukan`
--

CREATE TABLE `kd_pemasukan` (
  `id` int(100) NOT NULL,
  `ket` varchar(100) NOT NULL,
  `kd` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kd_pemasukan`
--

INSERT INTO `kd_pemasukan` (`id`, `ket`, `kd`) VALUES
(1, 'SPP', 'M01'),
(2, 'DPP', 'M02'),
(3, 'KEGIATAN', 'M03'),
(4, 'BANTUAN PEMERINTAH (BOS)', 'M04B'),
(5, 'BANTUAN PEMERINTAH (DAK)', 'M05B'),
(6, 'BANTUAN PEMERINTAH (BEASISIWA)', 'M06B'),
(7, 'BANTUAN PEMERINTAH (LAIN-LAIN)', 'M07B'),
(8, 'BANTUAN LEMBAGA NON PEMERINTAH', 'M08B'),
(9, 'BANTUAN DONATUR (PERORANGAN)', 'M09B'),
(10, 'POS SILANG YAYASAN', 'M10'),
(11, 'PENGAMBILAN DI BANK', 'M11'),
(12, 'PENCAIRAN DEPOSITO', 'M12'),
(13, 'LAIN-LAIN', 'M13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kd_pengeluaran`
--

CREATE TABLE `kd_pengeluaran` (
  `id_p` int(11) NOT NULL,
  `ket_p` varchar(100) NOT NULL,
  `kd_p` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kd_pengeluaran`
--

INSERT INTO `kd_pengeluaran` (`id_p`, `ket_p`, `kd_p`) VALUES
(1, 'Gaji dan tunjangan guru', 'K01'),
(2, 'Gaji dan tunjangan tenaga kependidikan', 'K02'),
(3, 'Biaya pengembangan guru dan tenaga kependidikan', 'K03'),
(4, 'Kegiatan pembelajaran', 'K04'),
(5, 'Kegiatan kesiswaan', 'K05'),
(6, 'Alat tulis sekolah', 'K06'),
(7, 'Bahan habis pakai', 'K07'),
(8, 'Alat habis pakai', 'K08'),
(9, 'Kegiatan rapat', 'K09'),
(10, 'Transport dan perjalanan dinas', 'K10'),
(11, 'Penggandaan soal-soal ulangan/ ujian', 'K11'),
(12, 'Daya dan Jasa', 'K12'),
(13, 'Sosial', 'K13'),
(14, 'Pemeliharaan', 'K14'),
(15, 'Pengadaan Inventaris', 'K15'),
(16, 'Pengembangan Kurikulum', 'K16'),
(17, 'Pengembangan Proses pembelajaran', 'K17'),
(18, 'Pengembangan Kompetensi Lulusan', 'K18'),
(19, 'Pengembangan Pendidik dan Tenaga Kependidikan', 'K19'),
(20, 'Pengembangan Sarana dan Prasarana', 'K20'),
(21, 'Pengembangan Manajemen Sekolah', 'K21'),
(22, 'Pengembangan Pembiayaan', 'K22'),
(23, 'Pengembangan Penilaian', 'K23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan_1`
--

CREATE TABLE `kegiatan_1` (
  `id_kgt` bigint(20) NOT NULL,
  `id_siswa` bigint(20) NOT NULL,
  `nom_kgt` double NOT NULL,
  `status_angs` int(20) NOT NULL,
  `th_ukgt` int(10) NOT NULL,
  `status_lap` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan_1`
--

INSERT INTO `kegiatan_1` (`id_kgt`, `id_siswa`, `nom_kgt`, `status_angs`, `th_ukgt`, `status_lap`) VALUES
(1, 134, 350000, 0, 1, 1),
(2, 135, 350000, 0, 1, 1),
(3, 136, 350000, 0, 1, 1),
(4, 137, 350000, 0, 1, 1),
(5, 138, 350000, 0, 1, 1),
(6, 139, 350000, 0, 1, 1),
(7, 140, 350000, 0, 1, 1),
(8, 141, 350000, 0, 1, 1),
(9, 142, 350000, 0, 1, 1),
(10, 143, 350000, 0, 1, 1),
(11, 144, 350000, 0, 1, 1),
(12, 145, 350000, 0, 1, 1),
(13, 146, 350000, 0, 1, 1),
(14, 147, 350000, 0, 1, 1),
(15, 148, 350000, 0, 1, 1),
(16, 149, 350000, 0, 1, 1),
(17, 150, 350000, 0, 1, 1),
(18, 151, 350000, 0, 1, 1),
(19, 152, 350000, 0, 1, 1),
(20, 153, 350000, 0, 1, 1),
(21, 154, 350000, 0, 1, 1),
(22, 155, 350000, 0, 1, 1),
(23, 156, 350000, 0, 1, 1),
(24, 157, 350000, 0, 1, 1),
(25, 158, 350000, 0, 1, 1),
(26, 106, 350000, 0, 1, 1),
(27, 107, 350000, 0, 1, 1),
(28, 108, 350000, 0, 1, 1),
(29, 109, 350000, 0, 1, 1),
(30, 110, 350000, 0, 1, 1),
(31, 111, 350000, 0, 1, 1),
(32, 112, 350000, 0, 1, 1),
(33, 113, 350000, 0, 1, 1),
(34, 114, 350000, 0, 1, 1),
(35, 115, 350000, 0, 1, 1),
(36, 116, 350000, 0, 1, 1),
(37, 117, 350000, 0, 1, 1),
(38, 118, 350000, 0, 1, 1),
(39, 119, 350000, 0, 1, 1),
(40, 120, 350000, 0, 1, 1),
(41, 121, 350000, 0, 1, 1),
(42, 122, 350000, 0, 1, 1),
(43, 123, 350000, 0, 1, 1),
(44, 124, 350000, 0, 1, 1),
(45, 125, 350000, 0, 1, 1),
(46, 126, 350000, 0, 1, 1),
(47, 127, 350000, 0, 1, 1),
(48, 128, 350000, 0, 1, 1),
(49, 129, 350000, 0, 1, 1),
(50, 130, 350000, 0, 1, 1),
(51, 131, 350000, 0, 1, 1),
(52, 132, 350000, 0, 1, 1),
(53, 133, 350000, 0, 1, 1),
(54, 159, 350000, 0, 1, 1),
(55, 160, 350000, 0, 1, 1),
(56, 161, 350000, 0, 1, 1),
(57, 162, 350000, 0, 1, 1),
(58, 163, 350000, 0, 1, 1),
(59, 164, 350000, 0, 1, 1),
(60, 165, 350000, 0, 1, 1),
(61, 166, 350000, 0, 1, 1),
(62, 167, 350000, 0, 1, 1),
(63, 168, 350000, 0, 1, 1),
(64, 169, 350000, 0, 1, 1),
(65, 170, 350000, 0, 1, 1),
(66, 171, 350000, 0, 1, 1),
(67, 172, 350000, 0, 1, 1),
(68, 173, 350000, 0, 1, 1),
(69, 174, 350000, 0, 1, 1),
(70, 175, 350000, 0, 1, 1),
(71, 176, 350000, 0, 1, 1),
(72, 177, 350000, 0, 1, 1),
(73, 178, 350000, 0, 1, 1),
(74, 179, 350000, 0, 1, 1),
(75, 180, 350000, 0, 1, 1),
(76, 181, 350000, 0, 1, 1),
(77, 182, 350000, 0, 1, 1),
(78, 183, 350000, 0, 1, 1),
(79, 184, 350000, 0, 1, 1),
(80, 185, 350000, 0, 1, 1),
(81, 186, 350000, 0, 1, 1),
(82, 187, 350000, 0, 1, 1),
(83, 188, 350000, 0, 1, 1),
(84, 190, 350000, 0, 1, 1),
(85, 191, 350000, 0, 1, 1),
(86, 193, 350000, 0, 1, 1),
(87, 194, 350000, 0, 1, 1),
(88, 196, 350000, 0, 1, 1),
(89, 197, 350000, 0, 1, 1),
(90, 198, 350000, 0, 1, 1),
(91, 199, 350000, 0, 1, 1),
(92, 201, 350000, 0, 1, 1),
(93, 202, 350000, 0, 1, 1),
(94, 203, 350000, 0, 1, 1),
(95, 204, 350000, 0, 1, 1),
(96, 205, 350000, 0, 1, 1),
(97, 206, 350000, 0, 1, 1),
(98, 207, 350000, 0, 1, 1),
(99, 208, 350000, 0, 1, 1),
(100, 209, 350000, 0, 1, 1),
(101, 211, 350000, 0, 1, 1),
(102, 216, 350000, 0, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan_2`
--

CREATE TABLE `kegiatan_2` (
  `id_kgt2` bigint(20) NOT NULL,
  `id_kgt` bigint(20) NOT NULL,
  `angs_kgt` double NOT NULL,
  `ket_ang` text NOT NULL,
  `tgl_pembayaran` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kegiatan_2`
--

INSERT INTO `kegiatan_2` (`id_kgt2`, `id_kgt`, `angs_kgt`, `ket_ang`, `tgl_pembayaran`) VALUES
(14, 4, 350000, '1', '2018-03-26'),
(13, 3, 350000, '1', '2018-03-26'),
(12, 2, 350000, '1', '2018-03-26'),
(11, 1, 350000, '1', '2018-03-26'),
(15, 5, 350000, '1', '2018-03-26'),
(16, 6, 350000, '1', '2018-03-26'),
(17, 7, 350000, '1', '2018-03-26'),
(18, 8, 350000, '1', '2018-03-26'),
(19, 9, 350000, '1', '2018-03-26'),
(20, 10, 350000, '1', '2018-03-26'),
(21, 11, 350000, '1', '2018-03-26'),
(22, 12, 350000, '1', '2018-03-26'),
(23, 13, 350000, '1', '2018-03-26'),
(24, 14, 350000, '1', '2018-03-26'),
(25, 15, 350000, '1', '2018-03-26'),
(26, 16, 350000, '1', '2018-03-26'),
(27, 17, 350000, '1', '2018-03-26'),
(28, 18, 350000, '1', '2018-03-26'),
(29, 19, 350000, '1', '2018-03-26'),
(30, 20, 350000, '1', '2018-03-26'),
(31, 21, 350000, '1', '2018-03-26'),
(32, 22, 350000, '1', '2018-03-26'),
(33, 23, 350000, '1', '2018-03-26'),
(34, 24, 350000, '1', '2018-03-26'),
(35, 25, 350000, '1', '2018-03-26'),
(36, 26, 350000, '1', '2018-03-26'),
(37, 27, 350000, '1', '2018-03-26'),
(38, 28, 350000, '1', '2018-03-26'),
(39, 29, 350000, '1', '2018-03-26'),
(40, 30, 350000, '1', '2018-03-26'),
(41, 31, 350000, '1', '2018-03-26'),
(42, 32, 350000, '1', '2018-03-26'),
(43, 33, 350000, '1', '2018-03-26'),
(44, 34, 350000, '1', '2018-03-26'),
(45, 35, 350000, '1', '2018-03-26'),
(46, 36, 350000, '1', '2018-03-26'),
(47, 37, 350000, '1', '2018-03-26'),
(48, 38, 350000, '1', '2018-03-26'),
(49, 39, 350000, '1', '2018-03-26'),
(50, 40, 350000, '1', '2018-03-26'),
(51, 41, 350000, '1', '2018-03-26'),
(52, 42, 350000, '1', '2018-03-26'),
(53, 43, 350000, '1', '2018-03-26'),
(54, 44, 350000, '1', '2018-03-26'),
(55, 45, 350000, '1', '2018-03-26'),
(56, 47, 350000, '1', '2018-03-26'),
(57, 48, 350000, '1', '2018-03-26'),
(58, 49, 350000, '1', '2018-03-26'),
(59, 50, 350000, '1', '2018-03-26'),
(60, 51, 350000, '1', '2018-03-26'),
(61, 52, 350000, '1', '2018-03-26'),
(62, 53, 350000, '1', '2018-03-26'),
(63, 54, 350000, '1', '2018-03-26'),
(64, 55, 350000, '1', '2018-03-26'),
(65, 56, 350000, '1', '2018-03-26'),
(66, 57, 350000, '1', '2018-03-26'),
(67, 58, 350000, '1', '2018-03-26'),
(68, 59, 350000, '1', '2018-03-26'),
(69, 60, 350000, '1', '2018-03-26'),
(70, 61, 350000, '1', '2018-03-26'),
(71, 62, 350000, '1', '2018-03-26'),
(72, 64, 350000, '1', '2018-03-26'),
(73, 65, 350000, '1', '2018-03-26'),
(74, 67, 350000, '1', '2018-03-26'),
(75, 68, 350000, '1', '2018-03-26'),
(76, 69, 350000, '1', '2018-03-26'),
(77, 70, 350000, '1', '2018-03-26'),
(78, 71, 350000, '1', '2018-03-26'),
(79, 72, 350000, '1', '2018-03-26'),
(80, 73, 350000, '1', '2018-03-26'),
(81, 74, 350000, '1', '2018-03-26'),
(82, 75, 350000, '1', '2018-03-26'),
(83, 76, 350000, '1', '2018-03-26'),
(84, 77, 350000, '1', '2018-03-26'),
(85, 78, 350000, '1', '2018-03-26'),
(86, 63, 350000, '1', '2018-03-26'),
(87, 66, 350000, '1', '2018-03-26'),
(88, 79, 350000, '1', '2018-03-26'),
(89, 80, 350000, '1', '2018-03-26'),
(90, 81, 350000, '1', '2018-03-26'),
(91, 82, 350000, '1', '2018-03-26'),
(92, 83, 350000, '1', '2018-03-26'),
(93, 84, 350000, '1', '2018-03-26'),
(94, 85, 350000, '1', '2018-03-26'),
(95, 102, 350000, '1', '2018-03-26'),
(96, 86, 350000, '1', '2018-03-26'),
(97, 87, 350000, '1', '2018-03-26'),
(98, 88, 350000, '1', '2018-03-26'),
(99, 89, 350000, '1', '2018-03-26'),
(100, 90, 350000, '1', '2018-03-26'),
(101, 91, 350000, '1', '2018-03-26'),
(102, 92, 350000, '1', '2018-03-26'),
(103, 93, 350000, '1', '2018-03-26'),
(104, 94, 350000, '1', '2018-03-26'),
(105, 95, 350000, '1', '2018-03-26'),
(106, 96, 350000, '1', '2018-03-26'),
(107, 97, 350000, '1', '2018-03-26'),
(108, 98, 350000, '1', '2018-03-26'),
(109, 99, 350000, '1', '2018-03-26'),
(110, 100, 350000, '1', '2018-03-26'),
(111, 101, 350000, '1', '2018-03-26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas_ket` varchar(10) NOT NULL,
  `id_sch` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas_ket`, `id_sch`) VALUES
(31, 'X IPS 1', 36),
(30, 'X MIPA', 36),
(28, 'XI IPS 2', 36),
(23, 'Batita', 33),
(6, 'XI IPA ', 36),
(7, 'XI IPS 1', 36),
(29, 'XI IPS 3', 36),
(24, 'KB', 33),
(26, 'TK A', 33),
(27, 'TK B', 33),
(32, 'X IPS 2', 36),
(33, 'X IPS 3', 36),
(34, 'VII A', 40),
(35, 'VII B', 40),
(36, 'VIII A', 40),
(37, 'VIII B', 40),
(38, 'IX A', 40),
(39, 'IX B', 40),
(40, 'TK C', 38),
(41, 'TK B', 38),
(42, 'TK B1', 38),
(43, 'TK A1', 38),
(44, 'TK A', 38),
(45, 'X Akuntans', 37),
(46, 'X Pemasara', 37),
(47, 'XI Akuntan', 37),
(48, 'XI Pemasar', 37),
(49, 'B', 38);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ket_angsur`
--

CREATE TABLE `ket_angsur` (
  `id_angsur` int(11) NOT NULL,
  `ket_angsur` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ket_angsur`
--

INSERT INTO `ket_angsur` (`id_angsur`, `ket_angsur`) VALUES
(1, 'Angsuran Ke-1'),
(2, 'Angsuran Ke-2'),
(3, 'Angsuran Ke-3'),
(4, 'Angsuran Ke-4'),
(5, 'Angsuran Ke-5'),
(6, 'Angsuran Tambahan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kode_pb`
--

CREATE TABLE `kode_pb` (
  `id_pb` int(11) NOT NULL,
  `ket_pb` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kode_pb`
--

INSERT INTO `kode_pb` (`id_pb`, `ket_pb`) VALUES
(1, 'Pendapatan'),
(2, 'Biaya-Biaya Usaha'),
(3, 'Surplus(Defisit)'),
(4, 'Sisa Lebih');

-- --------------------------------------------------------

--
-- Struktur dari tabel `last_log`
--

CREATE TABLE `last_log` (
  `id_ll` bigint(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `role` varchar(30) NOT NULL,
  `sklh` varchar(100) NOT NULL,
  `wkt_login` datetime DEFAULT NULL,
  `wkt_logout` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `last_log`
--

INSERT INTO `last_log` (`id_ll`, `username`, `role`, `sklh`, `wkt_login`, `wkt_logout`) VALUES
(1, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-17 18:02:58', '2018-03-17 18:56:49'),
(2, 'cumacoba', 'Moderator', 'Yayasan Asti Dharma', '2018-03-17 18:57:15', '2018-03-17 18:57:29'),
(3, 'cumacoba', 'Moderator', 'Yayasan Asti Dharma', '2018-03-17 18:59:46', '2018-03-17 19:12:07'),
(4, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-17 19:12:29', '2018-03-17 19:22:09'),
(5, 'asdaasda', 'Operator', 'Yayasan Asti Dharma', '2018-03-17 19:22:19', '2018-03-17 19:22:37'),
(6, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-17 19:31:35', NULL),
(7, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-17 21:52:45', NULL),
(8, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-18 13:13:52', NULL),
(9, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-18 16:08:11', NULL),
(10, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-18 17:04:36', '2018-03-18 17:50:34'),
(11, 'amin', 'Super Admin', 'Yayasan Asti Dharma', '2018-03-18 17:18:00', '2018-03-18 18:55:40'),
(12, 'AstiDharmaPusat', 'Moderator', 'Asti Dharma Pusat', '2018-03-18 17:54:01', '2018-03-18 19:44:19'),
(13, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-18 20:12:17', '2018-03-18 20:21:02'),
(14, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-18 23:24:09', '2018-03-18 23:27:28'),
(15, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-19 07:04:44', NULL),
(16, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-19 18:29:31', NULL),
(17, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-19 20:28:58', '2018-03-19 20:34:54'),
(18, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-20 07:28:00', NULL),
(19, 'smapiustegal', 'Moderator', 'SMA Pius Tegal', '2018-03-20 09:37:12', '2018-03-20 09:49:34'),
(20, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-20 09:49:58', '2018-03-20 09:51:55'),
(21, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-20 10:02:14', '2018-03-20 10:19:41'),
(22, 'smapiustegal', 'Moderator', 'SMA Pius Tegal', '2018-03-20 10:18:48', NULL),
(23, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-20 10:23:47', NULL),
(24, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-20 10:29:43', '2018-03-20 10:38:12'),
(25, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-20 10:40:28', NULL),
(26, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-20 11:18:42', NULL),
(27, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-20 12:37:45', NULL),
(28, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-20 13:07:20', '2018-03-20 13:09:23'),
(29, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 07:47:20', NULL),
(30, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 09:01:33', NULL),
(31, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 09:32:33', NULL),
(32, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-21 10:20:07', '2018-03-21 10:27:34'),
(33, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 10:47:48', '2018-03-21 11:05:48'),
(34, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 11:06:35', NULL),
(35, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 12:28:29', '2018-03-21 13:25:54'),
(36, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-21 12:44:01', '2018-03-21 13:46:09'),
(37, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 13:27:03', NULL),
(38, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 13:55:26', NULL),
(39, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-21 15:00:18', NULL),
(40, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-22 07:31:31', '2018-03-22 08:10:16'),
(41, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-22 08:13:33', '2018-03-22 08:44:02'),
(42, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-22 10:04:48', NULL),
(43, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-23 10:14:52', NULL),
(44, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-24 08:01:35', NULL),
(45, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-24 09:04:06', '2018-03-24 11:05:57'),
(46, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-26 07:09:58', '2018-03-26 09:18:33'),
(47, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-26 09:19:06', '2018-03-26 09:32:38'),
(48, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-26 09:33:05', NULL),
(49, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-26 12:55:25', NULL),
(50, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-27 07:19:36', NULL),
(51, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-27 09:50:41', NULL),
(52, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-27 11:47:18', NULL),
(53, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-03-27 12:04:21', '2018-03-27 12:42:53'),
(54, 'tkpiustegal', 'Moderator', 'TK Pius Tegal', '2018-03-28 11:34:50', NULL),
(55, 'SMPPIUSPemalang', 'Moderator', 'SMP Pius Pemalang', '2018-03-29 08:20:44', '2018-03-29 08:23:47'),
(56, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-03-29 10:05:27', NULL),
(57, 'SMPPIUSPemalang', 'Moderator', 'SMP Pius Pemalang', '2018-03-29 10:44:50', '2018-03-29 11:53:15'),
(58, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-03 08:03:20', NULL),
(59, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-04-03 12:57:42', '2018-04-03 12:58:20'),
(60, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-03 13:55:28', NULL),
(61, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-04 08:16:09', '2018-04-04 08:28:17'),
(62, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-04 08:29:24', '2018-04-04 08:32:18'),
(63, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-04 08:33:48', '2018-04-04 09:18:20'),
(64, 'SMPPIUSPemalang', 'Moderator', 'SMP Pius Pemalang', '2018-04-04 19:31:47', '2018-04-04 19:43:22'),
(65, 'TKPiuspemalang', 'Moderator', 'TK Pius Pemalang', '2018-04-04 21:01:21', '2018-04-04 21:27:07'),
(66, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-05 08:57:38', '2018-04-05 09:00:22'),
(67, 'SMPPIUSPemalang', 'Moderator', 'SMP Pius Pemalang', '2018-04-06 08:58:16', '2018-04-06 09:00:54'),
(68, 'TKPiuspemalang', 'Moderator', 'TK Pius Pemalang', '2018-04-07 21:56:42', '2018-04-07 21:59:14'),
(69, 'SMPPIUSPemalang', 'Moderator', 'SMP Pius Pemalang', '2018-04-09 08:10:27', '2018-04-09 08:25:07'),
(70, 'SMKPiusTegal', 'Moderator', 'SMK Pius Tegal', '2018-04-09 11:09:20', NULL),
(71, 'SMKPiusTegal', 'Moderator', 'SMK Pius Tegal', '2018-04-13 07:35:06', NULL),
(72, 'SMKPiusTegal', 'Moderator', 'SMK Pius Tegal', '2018-04-13 07:44:19', '2018-04-13 07:46:19'),
(73, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-16 09:52:19', NULL),
(74, 'TKPiuspemalang', 'Moderator', 'TK Pius Pemalang', '2018-04-17 22:22:22', '2018-04-17 22:24:30'),
(75, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-20 10:02:54', NULL),
(76, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-23 14:11:01', NULL),
(77, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-04-24 08:31:19', '2018-04-24 08:34:58'),
(78, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-04-24 09:20:08', '2018-04-24 09:21:56'),
(79, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-04-24 09:30:08', NULL),
(80, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-04-24 10:00:17', '2018-04-24 10:04:52'),
(81, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2018-04-24 10:58:16', NULL),
(82, 'SMKPiusTegal', 'Moderator', 'SMK Pius Tegal', '2018-04-25 09:36:21', '2018-04-25 09:40:23'),
(83, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-05-02 14:14:39', '2018-05-02 14:53:27'),
(84, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-05-03 14:08:18', '2018-05-03 14:28:19'),
(85, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2018-05-08 08:08:58', '2018-05-08 08:11:45'),
(86, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-08-10 09:34:06', '2019-08-10 09:34:21'),
(87, 'AstiDharmaPusat', 'Super Admin', 'Asti Dharma Pusat', '2019-08-26 09:05:48', NULL),
(88, 'AstiDharmaPusat', 'Super Admin', 'Asti Dharma Pusat', '2019-08-26 11:06:29', NULL),
(89, 'AstiDharmaPusat', 'Super Admin', 'Asti Dharma Pusat', '2019-08-26 11:32:10', NULL),
(90, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-09-02 10:56:12', '2019-09-02 12:11:06'),
(91, 'SMAPiusTegal', 'Moderator', 'SMA Pius Tegal', '2019-09-02 12:11:57', NULL),
(92, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-09-10 13:03:31', NULL),
(93, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-09-13 15:05:11', '2019-09-13 15:05:21'),
(94, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-10-10 15:53:18', NULL),
(95, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-11-27 21:37:11', NULL),
(96, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-11-28 11:02:45', NULL),
(97, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2019-12-03 11:37:41', NULL),
(98, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2020-07-16 19:03:50', '2020-07-16 19:11:56'),
(99, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2020-07-17 09:21:15', '2020-07-17 10:02:07'),
(100, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2020-07-18 11:32:38', NULL),
(101, 'amin', 'Super Admin', 'Asti Dharma Pusat', '2020-07-22 11:16:19', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelajar_list`
--

CREATE TABLE `pelajar_list` (
  `id_pl` bigint(20) NOT NULL,
  `nisn_pl` varchar(64) NOT NULL,
  `nama_pl` varchar(100) NOT NULL,
  `jk_pl` varchar(20) NOT NULL,
  `alamat_pl` text NOT NULL,
  `tahun_masuk` varchar(4) NOT NULL,
  `tahun_keluar` varchar(4) DEFAULT NULL,
  `id_schp` bigint(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelajar_list`
--

INSERT INTO `pelajar_list` (`id_pl`, `nisn_pl`, `nama_pl`, `jk_pl`, `alamat_pl`, `tahun_masuk`, `tahun_keluar`, `id_schp`, `status`) VALUES
(1, '20027193', 'ADELINE BERNADETA', 'P', 'Jl. Pala Barat 3 Blok N.23 Mejasem\r\n', '2016', '', 36, 1),
(2, '11832405', 'ANG SATRIYO WIJAYA', 'L', 'Jl. Glatik No. 48 Tegal', '2016', NULL, 36, 1),
(3, '11832418', 'ANGELINA GABRIELLA SULIYANTO', 'P', 'Jl. Manyar No. 4 Tegal\r\n', '2016', '', 36, 1),
(4, '11832411', 'CANESYA HELGA RIANTI', 'P', 'Jl. Rambutan 16 Blok D10 Tegal\r\n', '2016', '', 36, 1),
(5, '11832373', 'CARENT IMANUELLA MERRIBETH', 'P', 'Jl. Villa Garden I No. 12 Tegal\r\n', '2016', '', 36, 1),
(6, '11832353', 'CHRISTOPHER WILLIAM KOESOEMO', 'L', 'Perum. Citra Bahari 13/12 Tegal\r\n\r\n', '2017', '', 36, 1),
(7, '11832415', 'DANIEL ALFARO WIRAYUDANTO', 'L', 'Jl. Sultan Agung No. 71 Tegal\r\n\r\n', '2016', '', 36, 1),
(8, '11832381', 'ELIZABETH KATHERINE TANZIL', 'P', 'Jl. MT. Haryono No. 20 Tegal\r\n\r\n', '2016', '', 36, 1),
(9, '11832410', 'ESTHER THETRIANI PUSPA OCTAVIA SUNJOYO', 'P', 'Jl. KH. Dahlan No. 17 Tegal\r\n\r\n', '2016', '', 36, 1),
(10, '11832371', 'EVAN CHRISTIAN GUNARTO', 'L', 'Jl. P. Diponegoro No. 32 Brebes\r\n\r\n', '2016', '', 36, 1),
(11, '12276368', 'EVELYN HARTONO', 'P', 'Jl. Raya Padaharja KM 5 No. 160/88 Tegal\r\n\r\n', '2016', '', 36, 1),
(12, '11441255', 'FANNY YULIANA', 'P', 'Perum. Griya Estetika No. 13 Mejasem\r\n\r\n', '2016', '', 36, 1),
(13, '11832349', 'FEDELLA STEPHANIE SURYATA', 'P', 'Jl. Kaloran No. 3 Tegal\r\n\r\n', '2016', '', 36, 1),
(14, '16400628', 'GRACIA WIJAYA', 'P', 'Perum. Sapphire C17 Jl. HOS. Cokroaminoto Slawi\r\n\r\n', '2016', '', 36, 1),
(15, '11493036', 'JENNIFER ANGELINA', 'P', 'Perum. Citraland B2 No. 25 Tegal\r\n\r\n', '2016', '', 36, 1),
(16, '11832367', 'MARCELINA ABEL DEWANTI', 'P', 'Jl. Kauman Tengah No. 32 Tegal\r\n\r\n', '2016', '', 36, 1),
(17, '19824518', 'MARIO YULIANTO', 'L', 'Perum Palm Town House Blok R1 Tegal\r\n', '2016', '', 36, 1),
(18, '20175029', 'MICHELLE ANGELA', 'P', 'Jl. Barito No. 55 Tegal\r\n\r\n', '2016', '', 36, 1),
(19, '4085345', 'NICHOLAS VINANDITO', 'L', 'Jl. Nanas Gg. 27 No. 17 Tegal\r\n', '2016', '', 36, 1),
(21, '11832422', 'NOLA', 'P', 'Jl. Setia Budi Mo. 38 Tegal\r\n\r\n', '2016', '', 36, 1),
(22, '11832417', 'STEVANNO ANDREW IRAWAN', 'L', 'Jl. A. Yani No. 19 Slawi\r\n\r\n', '2016', '', 36, 1),
(23, '12234393', 'SURYADI JOYOPRANOTO', 'L', 'Jl. Jendral Sudirman No. 181 Pemalang\r\n\r\n', '2016', '', 36, 1),
(24, '11832361', 'TALISA VALENIA HANJOYO', 'P', 'Jl. Jendral Sudirman No. 60 Slawi\r\n', '2016', '', 36, 1),
(25, '13923934', 'AGNES MEIFANY', 'P', 'Jl. Arimbi I No. 16 Tegal\r\n\r\n\r\n', '2016', '', 36, 1),
(26, '11832357', 'AGUSTINUS ALVITO HARDIMAN WIRYANTO', 'L', 'Jl. Pala Barat Raya No. 11 Mejasem\r\n\r\n', '2016', '', 36, 1),
(27, '11832377', 'ALVINO NICOLAS', 'L', 'Jl. KH. Achmad Dahlan No. 12 Slawi\r\n\r\n', '2016', '', 36, 1),
(28, '15393122', 'ANATASYA PRIBADI ', 'P', 'Jl. P. Diponegoro No. 356 Brebes\r\n\r\n', '2016', '', 36, 1),
(29, '11795938', 'ANGELINA NOVIANTI WINARLIE', 'P', 'Jl. Kurma I No. 8 Tegal\r\n\r\n', '2016', '', 36, 1),
(30, '11832386', 'BONIFASIUS RENALDI', 'L', 'Jl. Waru No. 9 Tegal\r\n\r\n', '2016', '', 36, 1),
(31, '13162748', 'DAVEY OBBI ABEDNEGO KUNTORO', 'L', 'Jl. Gandaria No. 4-6 Tegal\r\n\r\n', '2016', '', 36, 1),
(32, '11832402', 'DESSY PERMATA SARI', 'P', 'Jl. Setia Budi No. 122 Tegal\r\n', '2016', '', 36, 1),
(33, '20152909', 'ELEONORA FEBRIATI TRINITA', 'P', 'Jl. P. Kemerdekaan Gg. Raharjo II No. 7 Tegal\r\n', '2016', '', 36, 1),
(34, '11832396', 'EUGENIA JANEDA DARMADINATA', 'P', 'Jl. Asemtiga Gg. 3 No. 11 Tegal\r\n\r\n', '2016', '', 36, 1),
(35, '18799387', 'EVAN YUDI APRILIANTO', 'L', 'Jl. Pala Raya Ruko Green House No. B1 Mejasem\r\n\r\n', '2016', '', 36, 1),
(36, '11832350', 'GABRIELA CALISTA SUNDJOJO', 'P', 'Jl. Kapten Ismail No. 36 Tegal\r\n\r\n', '2016', '', 36, 1),
(37, '12215833', 'GRACIA FERNANDA CHRISTINA', 'P', 'Jl. Rambutan 17 No. 4 Tegal\r\n\r\n', '2016', '', 36, 1),
(38, '11832354', 'JUAN PERMANA', 'L', 'Jl. Flores Gg. 3 No. 45 Tegal \r\n\r\n', '2016', '', 36, 1),
(39, '15451330', 'KALVIN FRANCISCO SILALAHI S. ', 'L', 'Perum. Mejasem Baru 2 Jl. Cut Mutia No. 25\r\n\r\n', '2016', '', 36, 1),
(40, '11441373', 'KANISIA DEFIKA LOCITA', 'P', 'Jl. Pala Barat I Blok B45 Mejasem\r\n\r\n', '2016', '', 36, 1),
(41, '13736392', 'KRISTINA BELA LARASATI', 'P', 'Jl. Kyai Yusuf No. 22 Suradadi\r\n\r\n', '2016', '', 36, 1),
(42, '12174350', 'MAJOLICA ERIKA FAE', 'P', 'Perum. Griya Baruna Asri B87 Tegal\r\n\r\n', '2016', '', 36, 1),
(43, '10322115', 'NEILSON HERLIE MALEAKHI', 'L', 'Jl. A. Yani No.28 Slawi\r\n\r\n', '2016', '', 36, 1),
(44, '17834648', 'NICHOLAS RAY', 'L', 'Jl. Raya Barat No. 17 Jatibarang\r\n\r\n', '2016', '', 36, 1),
(45, '11832372', 'RICHIE KENCANA', 'L', 'Perum. Citraland A1 No. 1 Tegal\r\n\r\n', '2016', '', 36, 1),
(46, '3610826', 'STEFANUS DWISETYA PANDUJAYA', 'L', 'Jl. Udang No.16 Tegal\r\n\r\n', '2016', '', 36, 1),
(47, '11832369', 'VANIANANDA KUSUMO', 'P', 'Jl. Sawo Barat B17 Tegal\r\n\r\n', '2016', '', 36, 1),
(48, '19014925', 'VINCENT GUNAWAN', 'L', 'Jl. Pala 14 No. 239 Mejasem\r\n', '2016', NULL, 36, 1),
(49, '11832397', 'YF. DAVIN SEAN INDRAJAYA', 'L', 'Jl. Layur No. 39 Tegal\r\n', '2016', NULL, 36, 1),
(50, '7273894', 'YOHANES ELANG SAMUDERA', 'L', 'Jl. Segarawana 4 No. 15 Mejasem\r\n', '2016', NULL, 36, 1),
(55, '14187423', 'ADITHYA CANDRA KUSUMA', 'L', 'Jl. Badak Raya 19 Mejasem\r\n', '2016', NULL, 36, 1),
(56, '11832398', 'ALVIN IGO SASONGKO', 'L', 'Jl. Arum Indah V No. 1 Tegal\r\n', '2016', NULL, 36, 1),
(57, '11485114', 'ANASTASHA TANAKA', 'P', 'Jl. Durian No. 1 Tegal\r\n\r\n', '2016', '', 36, 1),
(58, '11832416', 'ANGELICA SARAH NOVIANTI', 'P', 'Perum. Taman Sejahtera II B8 Tegal\r\n', '2016', '', 36, 1),
(59, '5612872', 'BENEDICTUS FIRMAN NOVANTO', 'L', 'Jl. Blimbing Gg. Klengkeng 2 No. 52 Tegal\r\n', '2016', NULL, 36, 1),
(60, '5558600', 'BERLIANA DEWI FITRIYANI', 'P', 'Jl. Saparua Gg. 2 No. 7 Tegal\r\n', '2016', '', 36, 1),
(61, '11832365', 'BRIAN KENNETH SANTOSO', 'L', 'Jl. Rajawali Raya No. 22 Tegal\r\n', '2016', NULL, 36, 1),
(62, '1455592', 'CANDRAWATI  SARINA SIALLAGAN ', 'P', 'Jl. Bung Tomo No. 20 Mejasem\r\n', '2016', NULL, 36, 1),
(63, '5046259', 'DARMAHTINI', 'P', 'Perum. Citra Bahari D56 Tegal\r\n', '2016', NULL, 36, 1),
(64, '11832360', 'DEVINA LIANI UNTORO', 'P', 'Jl. Batanghari No. 35 Tegal\r\n', '2016', NULL, 36, 1),
(65, '10322102', 'ERA ANDREA', 'P', 'Jl. Imam Bonjol No. 24 Slawi\r\n', '2016', NULL, 36, 1),
(66, '28546637', 'HENDRA WIJAYANTO', 'L', 'Jl. Kapten Ismail Gg. Mundu Tegal\r\n', '2016', NULL, 36, 1),
(67, '20219282', 'JASSEN WIJAYA', 'L', 'Jl. Raya Utara No. 514 Adiwerna\r\n', '2016', NULL, 36, 1),
(68, '11832408', 'LUKAS WILLIAM WIJAYA', 'L', 'Jl. Tawes Gg. II No. 5 Tegal\r\n', '2016', NULL, 36, 1),
(69, '12276366', 'MARCEL SEBASTIAN HARTONO', 'L', 'Jl. Pala Barat I A17 Mejasem\r\n', '2016', NULL, 36, 1),
(70, '11832393', 'PEDRO MARCIANO SETIAWAN LIE', 'L', 'Jl. HOS. Cokroaminoto No. 75 Tegal\r\n', '2016', NULL, 36, 1),
(71, '12732694', 'PRISCILA JULIA ANGGELIA WENBEN', 'P', 'Sarongsong II-Ling II Tonsea Airmadidi Menado\r\n', '2016', NULL, 36, 1),
(72, '12276369', 'REZAGITA KARUNA', 'P', 'Jl. Paweden No. 11 Tegal\r\n', '2016', NULL, 36, 1),
(73, '12176250', 'SELVIANA DIANA MULYANI', 'P', 'Jl. Batam No. 71 Tegal\r\n', '2016', NULL, 36, 1),
(74, '20151510', 'TAKEMASA SHINKAI', 'L', 'Jl. Delima No. 47 Tegal\r\n', '2016', NULL, 36, 1),
(75, '20173428', 'TANDRIKA KAYANA WIDODO', 'L', 'Jl. Panggung Timur No. 26 Tegal\r\n', '2016', NULL, 36, 1),
(76, '11832400', 'TANIA MUJAYANI', 'P', 'Jl. Agus Salim No. 2 Slawi\r\n', '2016', NULL, 36, 1),
(77, '11832406', 'TIMOTHIUS OLIVER GILBERT SILALAHI', 'L', 'Jl. Raya Klampok No. 165 Brebes\r\n', '2016', NULL, 36, 1),
(78, '11441286', 'VISSILIUS GERALDI KRISTIAWAN ', 'L', 'Jl. Pala Barat I Blok C42 Mejasem\r\n', '2016', NULL, 36, 1),
(79, '11931214', 'YOHANES HANJAYA', 'L', 'Jl. Cinde Gg. Pelita No. 2 Tegal\r\n', '2016', NULL, 36, 1),
(80, '2218654', 'ANTONIUS FAJAR HANINDIKA', 'L', 'Jl. Dahlia No. 14, Brebes\r\n', '2016', NULL, 36, 1),
(81, '19590246', 'AGNES PERMATA PUTRI', 'P', 'Jl. Pala Barat I Blok E6 Mejasem\r\n', '2016', '', 36, 1),
(82, '11441085', 'ANTONIUS DAVID HENDRAWAN', 'L', 'Jl. Semanggi III No. 16 Mejasem\r\n', '2016', '', 36, 1),
(83, '11832370', 'AURELIA IVANA ARLIJANTO', 'P', 'Jl. Raya Dampyak No. 18 Tegal\r\n', '2016', NULL, 36, 1),
(84, '8946070', 'BILLY CHRISTIANTO SUTAWIJAYA', 'L', 'Jl. Wader No. 6 Tegal\r\n', '2016', NULL, 36, 1),
(85, '11860074', 'CALVIN VRILENTIANTO EKA WIJAYA', 'L', 'Jl. Mayjend Sutoyo No. 8 Slawi\r\n', '2016', NULL, 36, 1),
(86, '1047379', 'CANKA DEWA LOKANANTA', 'L', 'Jl. Dr. Sutomo No. 8 Slawi\r\n', '2016', NULL, 36, 1),
(87, '15455841', 'CHINTIA', 'P', 'Jl. Raya Bulakamba No. 93 Brebes\r\n', '2016', NULL, 36, 1),
(88, '11832345', 'CINDY LEONYTA PRAMONO', 'P', 'Jl. Kartini No. 64 Tegal\r\n', '2016', NULL, 36, 1),
(89, '11832404', 'CLARINTA AGRIPPINA EDGINA', 'P', 'Jl. Waringin No. 41 Tegal\r\n', '2016', NULL, 36, 1),
(90, '11832390', 'FRANSISKUS HADIYANTO CHRISTIONO', 'L', 'Jl. Rambutan 14A No. 143 Tegal\r\n', '2016', NULL, 36, 1),
(91, '11441313', 'GREGORIUS PATRIA ARMANTO', 'L', 'Jl. Pala Barat I Blok J6 Mejasem\r\n', '2016', NULL, 36, 1),
(92, '5612873', 'IGNATIUS RICKY SURYAJAYA', 'L', 'Jl. P. Diponegoro No. 87 Tegal\r\n', '2016', NULL, 36, 1),
(93, '16436726', 'MARCELINUS RENALDI', 'L', 'Komplek Baleendah Permai No. 15A Bandung\r\n', '2016', NULL, 36, 1),
(94, '19306331', 'MEILANI INDARWIDIYANTI LIMMAWAN', 'P', 'Jl. Raya Pegirikan Ambo No. 37 Banjaran\r\n', '2016', NULL, 36, 1),
(95, '3482919', 'ROGEST OKI WIJAYA', 'L', 'Jl. Budi Mulya No. 61 Slawi \r\n', '2016', NULL, 36, 1),
(96, '11441423', 'SEBASTIAN PETER PRANADENTA', 'L', 'Jl. Pala Barat I Blok F16 Mejasem\r\n', '2016', NULL, 36, 1),
(97, '12157852', 'SHERENA ALDA LYDIA BASA BR MANURUNG', 'P', 'Jl. Bali No. 202 Limbangan-Brebes\r\n', '2016', NULL, 36, 1),
(98, '11832376', 'SIMON VIJAY PRATAMA SIMARMATA', 'L', 'Perum. Bimantara Blok A9 Mejasem\r\n', '2016', NULL, 36, 1),
(99, '11441433', 'VALENTINE DWIASIH SARWONO', 'P', 'Perum. Mejasem Baru 2 Jl. Cut Nyak Dien No. 28\r\n', '2016', NULL, 36, 1),
(100, '11790426', 'VANNI MIHARDJA', 'P', 'Perum. Sangrilla Blokn A12 Mejasem\r\n', '2016', NULL, 36, 1),
(101, '12174353', 'VINENCIA MONICA ANDREA', 'P', 'Jl. Pala Barat 3 Blok M24 Mejasem\r\n', '2016', NULL, 36, 1),
(102, '11832379', 'WINDA ARTHA WIGUNA', 'L', 'Jl. Rambutan 17 No. 17 Tegal\r\n', '2016', NULL, 36, 1),
(103, '12750840', 'YONGKY SEPTIAN SAPUTRA', 'L', 'Jl. Stasiun No. 27 Adiwerna\r\n', '2016', NULL, 36, 1),
(104, '11832375', 'YULIANA SETYOWATI', 'P', 'Jl. Masjid No. 38 Tegal\r\n', '2016', NULL, 36, 1),
(105, '15682511', 'YULIYANTI', 'P', 'Gg. Kembang I No. 28 Banjaran\r\n', '2016', NULL, 36, 1),
(106, '28731625', 'AILEEN FEBRIANI WIJAYA', 'P', 'Jl. Kapten Sudibyo No. 83 Tegal\r\n', '2017', '', 36, 1),
(107, '28718488', 'ALBERTUS ANDIKA', 'L', 'Jl. Layur No. 67 Tegal\r\n', '2017', '', 36, 1),
(108, '22311629', 'ALEXANDER HENDRY WIJAYA', 'L', 'Jl. Pala Barat I Blok L34 Mejasem\r\n', '2017', NULL, 36, 1),
(109, '28718490', 'ALEXANDER PRASETYA', 'L', 'Jl. Layur No. 67 Tegal\r\n', '2017', NULL, 36, 1),
(110, '28717766', 'ANDREAS ADIEL SETIAWAN, SIE', 'L', 'Jl. Hang Tuah No. 49/55 Tegal\r\n', '2017', NULL, 36, 1),
(111, '28731650', 'ANGELINE MEYLIANA PUTRI', 'P', 'Jl. Cemara No. 96 Tegal\r\n', '2017', NULL, 36, 1),
(112, '10902517', 'CALISTA HARTONO', 'P', 'Jl. Arum Indah V / III No. 15 Tegal\r\n', '2017', NULL, 36, 1),
(113, '11247574', 'CARMELLA', 'P', 'Perum. Taman Sejahtera No. 25 Tegal\r\n', '2017', NULL, 36, 1),
(114, '28718203', 'CHRISTIAN GANDA', 'L', 'Jl. Letjend. Suprapto No. 14 Brebes\r\n', '2017', NULL, 36, 1),
(115, '28718615', 'CHRISTINA AMELIA SUNARSO', 'P', 'Jl. Kapten Sudibyo No. 53 Tegal\r\n', '2017', NULL, 36, 1),
(116, '28731667', 'DANIEL JOSES FERDIAN', 'L', 'Jl. Rambutan 16 No. D130 Tegal\r\n', '2017', NULL, 36, 1),
(117, '28718208', 'DEVARA LIKO IVANDER ', 'L', 'Jl. Bawal Barat RT III RW IIIA\r\n', '2017', NULL, 36, 1),
(118, '28718237', 'FERDINAND JULIUS HARTOTO', 'L', 'Jl. Rambutan 16 No. 15 Tegal\r\n', '2017', NULL, 36, 1),
(119, '28731694', 'FERRIN ANGELICA HONGGOWASITO', 'P', 'Perum. Citra Land B1 No. 3 Tegal\r\n', '2017', NULL, 36, 1),
(120, '20028035', 'FIDHELIS FERDINANDA PRABAWA', 'L', 'Jl. Semanggi Raya No. 17 Mejasem\r\n', '2017', NULL, 36, 1),
(121, '10902510', 'GANDHI HARTOKO PUTERA SADIKIN', 'L', 'Jl. Raya No. 8 Margasari\r\n', '2017', NULL, 36, 1),
(122, '21597852', 'IE, MICHAEL KURNIAWAN HARIANTO', 'L', 'Jl. Wahid Hasyim No. 48 Dukuh Tengah\r\n', '2017', NULL, 36, 1),
(123, '28718273', 'JASON SUHARYONO', 'L', 'Jl. Asemtiga Gg. 9 No. 11 Tegal\r\n', '2017', NULL, 36, 1),
(124, '28731724', 'JONATHAN HALIM', 'L', 'Perum. Taman Sejahtera Gg. 7 No. 3 Tegal\r\n', '2017', NULL, 36, 1),
(125, '28718297', 'MARCELLO VIERI LIANAJAYA', 'L', 'Jl. Veteran Gg. II no. 2 Tegal\r\n', '2017', NULL, 36, 1),
(126, '11249606', 'MARIA CRISTIANA DEWI', 'P', 'Jl. Blimbing Gg. 2 No. 33 Tegal\r\n', '2017', NULL, 36, 1),
(127, '20398075', 'MICHAEL ADRIAN WINARLIE', 'L', 'Jl. Kurma I No. 8 Tegal\r\n', '2017', NULL, 36, 1),
(128, '28731736', 'NAOMI ANGELA', 'P', 'Jl. A. Yani No. 27 Tegal\r\n', '2017', '', 36, 1),
(129, '28835595', 'NATASYA ANJELITA SUGIANTO', 'P', 'Jl. Letjend. Suprapto No. 76 Tegal\r\n', '2017', NULL, 36, 1),
(130, '33696034', 'OKA JORDAN', 'L', 'Jl. A. Yani No. 59B Brebes\r\n', '2017', NULL, 36, 1),
(131, '28731757', 'PRISCILLA TRACY PAULANA LIE', 'P', 'Jl. Yos Sudarso Ruko Nirmala Square Blok C4 Tegal\r\n', '2017', NULL, 36, 1),
(132, '29825730', 'VISAKHA VIJAYANTI AVYAPADA', 'P', 'Jl. Gunung Kelir III RT 02 RW 01\r\n', '2017', NULL, 36, 1),
(133, '22251506', 'WILLIAM SUNJAYA', 'L', 'Jl. Tengku Umar No. 214 Tegal\r\n', '2017', NULL, 36, 1),
(134, '21197418', 'ADRIEL SHEVA SETYAWAN', 'L', 'Jl. Arum Indah III No. 35 Tegal\r\n', '2017', '', 36, 1),
(135, '26059472', 'ALBERT KURNIAWAN', 'L', 'Jl. P.Diponegoro No. 59 Brebes\r\n', '2017', NULL, 36, 1),
(136, '28718528', 'ANGELINE MARSIANDHA', 'P', 'Jl. Kaloran No. 35 Tegal\r\n', '2017', NULL, 36, 1),
(137, '29035787', 'CAROLUS ARDI SILALAHI', 'L', 'Jl. Raya Tanjung Bringin , Sumbul, Sidikalang\r\n', '2017', NULL, 36, 1),
(138, '28718670', 'DELLA NOVITA', 'P', 'Jl. Raya Pegirikan No. 24 Tegal\r\n', '2017', NULL, 36, 1),
(139, '20663603', 'FEBRYAN VALENTINO ONGKOWIDJOJO', 'L', 'Ds. Balamoa RT 03/01 Balamoa, Pangkah\r\n', '2017', NULL, 36, 1),
(140, '28718233', 'FELLY OLIVIA', 'P', 'Jl. Blimbing No. 2 Tegal\r\n', '2017', NULL, 36, 1),
(141, '28731714', 'GRISHELDA THE', 'P', 'Jl. Ababil Indah No 46 Tegal\r\n', '2017', NULL, 36, 1),
(142, '28835479', 'INGE SEPTINA', 'P', 'Jl. Mujaher Gg. III No. 17 Tegal\r\n', '2017', NULL, 36, 1),
(143, '11442060', 'INTAN YOHANA SIDAURUK', 'P', 'Jl. Pala 16 No. 314 Mejasem\r\n', '2017', NULL, 36, 1),
(144, '28731587', 'IVANDER ADRIAN DJAYA', 'L', 'Jl. Martoloyo No. 106 Tegal\r\n', '2017', NULL, 36, 1),
(145, '28731589', 'KRISTY MARELDY', 'P', 'Jl. Brantas II No. 20 Tegal\r\n', '2017', NULL, 36, 1),
(146, '20028269', 'LEONARDO BRANDON', 'L', 'Jl. Pala Barat 3 No. 851 Mejasem\r\n', '2017', NULL, 36, 1),
(147, '26657990', 'M. ROY SAMSUL AKBAR', 'L', 'Grogol RT 7 RW 3 Dukuhturi\r\n', '2017', NULL, 36, 1),
(148, '27113396', 'MAJOLICA OCARINA FAE', 'P', 'Perum. Griya Baruna Asri B87 Tegal\r\n', '2017', NULL, 36, 1),
(149, '27138682', 'MARIA YOVITA WIDYANA', 'P', 'Jl. Waringin Raya No. 19A Slawi\r\n', '2017', NULL, 36, 1),
(150, '27138680', 'MARVEL EVANDI BUDIONO', 'L', 'Jl. Wijaya Kusuma No. 3 Kudaile\r\n', '2017', NULL, 36, 1),
(151, '11790210', 'MOSCHASKA LAIZZANO SANJAYA', 'L', 'Nirmala Square Blok C4 Tegal\r\n', '2017', NULL, 36, 1),
(152, '36372063', 'OLIVIA JANE ORLEN', 'P', 'Jl. Blimbing Gg. Kluwih No. 1 Tegal\r\n', '2017', NULL, 36, 1),
(153, '28731603', 'SAMUEL STEVEN YAP', 'L', 'Perum. Citra Bahari Blok C22 Tegal\r\n', '2017', NULL, 36, 1),
(154, '11860077', 'SAMUEL VIRYATAMA', 'L', 'Jl. Letjend Suprapto No. 33 Slawi\r\n', '2017', NULL, 36, 1),
(155, '38750789', 'TAM, MARIA YOSHELIN WIJAYA SANTOSO', 'P', 'Jl. Layur No. 8 Tegal\r\n', '2017', NULL, 36, 1),
(156, '27114916', 'TIRANI HARNA TRI BANOWATI', 'P', 'Perum. Griya Santika Blok P14 Mejasem\r\n', '2017', NULL, 36, 1),
(157, '20041211', 'VIOLA BR SIMARMATA', 'P', 'Perum. Sapphire Residence Blok C21 Tegal\r\n', '2017', NULL, 36, 1),
(158, '11832346', 'LAURENCIA AUDREY TJAN', 'P', 'Perum. Taman Sejahtera Gg. 7 No. 4 Tegal\r\n', '2017', NULL, 36, 1),
(159, '28717617', 'AGNES PRADITA SOEHARSO', 'P', 'Jl. Jendral Sudirman No. 44 Tegal\r\n', '2017', NULL, 36, 1),
(160, '32618574', 'AMELIA ANGELICA LAYS', 'P', 'Jl. Raya Bulakamba No. 9 RT 01  RW 2 Brebes\r\n', '2017', NULL, 36, 1),
(161, '23825256', 'CLARESTA JESLYN IRAWAN', 'P', 'Perum. Griya Baruna Asri B78 Tegal\r\n', '2017', NULL, 36, 1),
(162, '20028016', 'ELRED DITO ARI WIBOWO', 'L', 'Jl. Pala Barat 5A No. 1404 Mejasem\r\n', '2017', NULL, 36, 1),
(163, '28731693', 'EVANDER HUSEIN', 'L', 'Jl. Yos Sudarso No. 44 Tegal\r\n', '2017', NULL, 36, 1),
(164, '33692755', 'FALEN ANASTASIA ADELLA', 'P', 'Jl. Pala Barat 2 No. 950 Mejasem\r\n', '2017', NULL, 36, 1),
(165, '36631781', 'FAYOLA TANAKA', 'P', 'Jl. Durian No. 1 Tegal\r\n', '2017', NULL, 36, 1),
(166, '11860056', 'GIOVANNI ZELIG SUPRIYADI', 'L', 'Jl. Imam Bonjol Gg. 28 RT 01 RW 01 Kudaile\r\n', '2017', NULL, 36, 1),
(167, '37027968', 'JESSICA KAREN NATHANYA', 'P', 'Perum. Villa Garden No. 15 Tegal\r\n', '2017', NULL, 36, 1),
(168, '20260344', 'JOSE RIZAL LUCIANO', 'L', 'Jl. Raya Utara Gg. Kampung Baru No. 19 Banjaran\r\n', '2017', NULL, 36, 1),
(169, '28835587', 'KERIN ANDRIANI', 'P', 'Jl. Kaloran No. 12 Tegal\r\n', '2017', NULL, 36, 1),
(170, '12346442', 'MELINDA OKTIA ANGGORO', 'P', 'Jl. Rambutan 15 No. 8 Tegal\r\n', '2017', NULL, 36, 1),
(171, '28718356', 'MICHAEL', 'L', 'Jl. Serayu No. 151 Tegal\r\n', '2017', NULL, 36, 1),
(172, '5516154', 'MICHAEL HERMAWAN', 'L', 'Jl. Kol. Sugiono No. 147 Tegal\r\n', '2017', NULL, 36, 1),
(173, '37514299', 'MICHELLE WIJAYA', 'P', 'Perum. Sapphire Blok C3 Slawi\r\n', '2017', NULL, 36, 1),
(174, '27110736', 'RASTA NADILA DWIJAYA', 'P', 'Jl. Kapuas Gg. 5 RT 5 RW 7 Tegal\r\n', '2017', NULL, 36, 1),
(175, '20028578', 'RAYNALDO KRISTIADI', 'L', 'Perum. Griya Estetika No. 13 Mejasem\r\n', '2017', NULL, 36, 1),
(176, '11929612', 'RENDY HANATIA', 'L', 'Perum. Citraland Blok CI No. 17 Tegal\r\n', '2017', NULL, 36, 1),
(177, '21855425', 'SANIA', 'P', 'Jl. Segarawana II Blok E13 Mejasem\r\n', '2017', NULL, 36, 1),
(178, '6396117', 'SHERWINO KUSUMAWIDJAYA', 'L', 'Jl. Kauman Tengah No. 45 Tegal\r\n', '2017', NULL, 36, 1),
(179, '28718443', 'TIMOTHY JAMES ELBERT YUTANTO', 'L', 'Jl. AR. Hakim No. 65 Tegal\r\n', '2017', NULL, 36, 1),
(180, '26800921', 'VALENT JEANE PATRICIA', 'P', 'Perum. Graha Bahari Jl. Mangga Blok B2 No.5 Dampyak\r\n', '2017', NULL, 36, 1),
(181, '25994386', 'VIGO LEIF VAMOS', 'L', 'Jl. KH. Syahroni No. 4 Jatibarang\r\n', '2017', NULL, 36, 1),
(182, '28718452', 'VIORA STEVANIA', 'P', 'Jl. Garuda No. 22 Tegal\r\n', '2017', NULL, 36, 1),
(183, '28732536', 'WILSON LUKITO', 'L', 'Jl. Delima No. 9 Tegal\r\n', '2017', NULL, 36, 1),
(184, '28717761', 'ALVIN MARCELO', 'L', 'Jl. Pala 21 No. 62 Mejasem\r\n', '2017', NULL, 36, 1),
(185, '21908897', 'ANDREAS OSCAR APRILIYANTO', 'L', 'Jl. Sawo No. 6 Tegal\r\n', '2017', NULL, 36, 1),
(186, '28717831', 'ANDRONIKUS MULYONO', 'L', 'Jl. Batanghari Gg. Kimto No. 3 Tegal\r\n', '2017', NULL, 36, 1),
(187, '28718535', 'CARMENITA CERELIA', 'P', 'Jl. Pramuka No. 15B Jatibarang\r\n', '2017', NULL, 36, 1),
(188, '28718098', 'CHARISSA AURELIA SANTOPO', 'P', 'Jl. Letjend. Suprapto No. 91 Tegal\r\n', '2017', NULL, 36, 1),
(189, '664', 'Dominikus Bryan Sugiharto', 'L', 'Jl. Cendrawasih No.16 Tegal', '2017', '', 33, 1),
(190, '28718132', 'CHARLES BUDIMAN SUTRISNO', 'L', 'Jl. Raya Utara No. 8 Banjaran\r\n', '2017', NULL, 36, 1),
(191, '34924303', 'CHATARINA FEBRIOLA', 'P', 'Perum. Baruna Asri Blok B65 Tegal\r\n', '2017', NULL, 36, 1),
(192, '665', 'Emeryk Tyaga Alfaro', 'L', 'Griya Pisma Asri Mejasem', '2017', '', 33, 1),
(193, '28718540', 'CHRISTIAN LIMAWAN', 'L', 'Perum. Nirmala No. 9 Tegal\r\n\r\n', '2017', NULL, 36, 1),
(194, '27110754', 'FEBBY VANESIA', 'P', 'Peum. Pondok Martoloyo Gg.4 No. 25 Tegal\r\n', '2017', NULL, 36, 1),
(195, '666', 'Emily Elvinia Casidy Tan', 'P', 'Jl. Asem Tiga Gg.3 No.3 Tegal', '2017', '', 33, 1),
(196, '25342702', 'FIDELIA AMANDA', 'P', 'Jl. Arum Indah V Gg. I No. 9 Tegal\r\n', '2017', NULL, 36, 1),
(197, '28835395', 'FRANSISKA ENDAH DEWI', 'P', 'Jl. Cendrawasih No. 270 Tanjung-Brebes\r\n', '2017', NULL, 36, 1),
(198, '28718267', 'FREDERICA GRETTA NOVENA TANTRI', 'P', 'Jl. Arum Indah V / B-19 Tegal\r\n', '2017', NULL, 36, 1),
(199, '28130972', 'GLENN RICARDO', 'L', 'Perum. Emerald Jl. Kol. Sudiarto\r\n', '2017', NULL, 36, 1),
(200, '658', 'Fernandus Frenky', 'L', 'Jl. Ciliwung No.29 Tegal', '2017', '', 33, 1),
(201, '11248010', 'HUAN CARLOS BANUAREA TOBING', 'L', 'Jl. Abdinegara No. 19 Tegal\r\n', '2017', NULL, 36, 1),
(202, '25994411', 'JOVAN CONSTANTINO', 'L', 'Jl. Raya Timur No. 125 Jatibarang\r\n', '2017', NULL, 36, 1),
(203, '10902496', 'LEA MARVELIN PURNOMO', 'P', 'Jl. Astina No. 88 Margasari\r\n', '2017', NULL, 36, 1),
(204, '20173429', 'MARCELLA LORENCIA', 'P', 'Jl. Layang No. 15 Tegal\r\n', '2017', NULL, 36, 1),
(205, '28718375', 'MICHAEL ARIANTO SUBAGYO', 'L', 'Jl. Nanas No. 101 Tegal\r\n', '2017', NULL, 36, 1),
(206, '25847519', 'NJO, FULVIAN ANDRIYANTO', 'L', 'Jl. KS. Tubun No. 30 Tegal\r\n', '2017', NULL, 36, 1),
(207, '20327579', 'NORMA SILVIA SIEMANJAYA', 'P', 'Jl. Poso Gg. 17 No. 30 Tegal\r\n', '2017', NULL, 36, 1),
(208, '28731602', 'SABRINA ADIASVARA', 'P', 'Jl. Sawo Barat No. 97 Tegal\r\n', '2017', NULL, 36, 1),
(209, '28731899', 'VANESSA HEIDY', 'P', 'Jl. Todak No. 18 Tegal\r\n', '2017', NULL, 36, 1),
(210, '667', 'Gilbert Giovanno Sinurat', 'L', 'Perum Bimantara 7 Mejasem Tegal', '2017', '', 33, 1),
(211, '28731619', 'VANIA ANGELINA PUTRI', 'P', 'Jl. P. Kemerdekaan Gg. 17 No. 40 Tegal\r\n', '2017', NULL, 36, 1),
(212, '668', 'Gissel Christevani', 'P', 'Perum Tegal Residence Gg.4 No.16', '2017', '', 33, 1),
(213, '670', 'Ignatius Fabrizio Bunyamin', 'L', 'Jl. Mayjend Sutoyo No.20 Tegal', '2017', '', 33, 1),
(215, '671', 'Ivander Adiey Wijaya', 'L', 'Jl. Pala Barat 5A No1407 Mejasem Tegal', '2017', '', 33, 1),
(216, '28718159', 'CHAVIA ROSSYERIN PRABOWO SUTJIADI', 'P', 'Jl. Cemara No. 22 Tegal\r\n', '2017', NULL, 36, 1),
(217, '672', 'Jayden Austin Immanuel', 'L', 'Jl. Waringin Gg.8 No.3 Tegal', '2017', '', 33, 1),
(218, '673', 'Liborius Julian Krismiyanto', 'L', 'Jl. Pala Barat Blok C No.22 Mejasem Tegal ', '2017', '', 33, 1),
(219, '674', 'Marcello Dave Hanantha', 'L', 'Perum Villa Garden X.24 Tegal', '2017', '', 33, 1),
(220, '675', 'Maria Richka Elena', 'P', 'Jl. Bawal Buntu No.11 Tegal', '2017', '', 33, 1),
(221, '676', 'Michelle Angelia', 'P', 'Jl. Sawo Barat No24 Tegal', '2017', '', 33, 1),
(222, '677', 'Rafael Bagas Adiwijaya', 'L', 'Jl. Sepat Gg. Mujijaya', '2017', '', 33, 1),
(223, '678', 'Sebastianus Krishna Sefa', 'L', 'Jl. Pala Mejasem Barat', '2017', '', 33, 1),
(224, '679', 'Sergie Winnerdi', 'L', 'Jl. Kol. Sigiono 221 Tegal', '2017', '', 33, 1),
(225, '680', 'Sofia Eunike Helena', 'L', 'Jl. Lele No.15 Tegal', '2017', '', 33, 1),
(226, '681', 'Stefanus Dimas Radithya', 'L', 'Jl. Delima No.30 RT 04/RW 02 Dampyak Tegal', '2017', '', 33, 1),
(227, '682', 'Brilliant Marcello Evan', 'L', 'Jl. ', '2017', '', 33, 1),
(228, '683', 'Christopher Wicaksono', 'L', 'Jl. Sipelem Tegal', '2017', '', 33, 1),
(229, '3369', 'Abdiel Rehum Mokodongan', 'L', 'Jl. Jalak', '2016', NULL, 33, 1),
(230, '3370', 'Callista Olivia Sugianto', 'L', 'Jl. Kol. Sugiono No.83 Tegal', '2016', NULL, 33, 1),
(231, '3371', 'Dave Easterico Kuncoro', 'L', 'Jl. Pala 27 Gg.II C No.1', '2016', NULL, 33, 1),
(232, '3372', 'Felicia Bellvania Agna', 'L', 'Jl. Citarum Gg Malik III Tegal', '2016', NULL, 33, 1),
(233, '3374', 'Felicia Naga Tanjaya', 'P', 'Jl. Halmahera Gg Masjid', '2016', NULL, 33, 1),
(234, '3374', 'Gabriella Immanuel S', 'P', 'Jl. Sipelem Tegal', '2016', NULL, 33, 1),
(235, '3375', 'Gabrielle Putri Ciquita', 'P', 'Jl. Kol. Sugiono 44 Tegal', '2016', NULL, 33, 1),
(236, '3376', 'George Rafer Rudianto', 'L', 'Perum Villa Garden tegal', '2016', NULL, 33, 1),
(237, '3377', 'Gisela Ivena Wijaya', 'P', 'Jl. Griya Zahira No.12 Tegal', '2016', NULL, 33, 1),
(238, '3378', 'Grace Zilla Putianti', 'P', 'Jl. Waringin No.17 Tegal', '2016', NULL, 33, 1),
(239, '3380', 'Kenzo jetta Agustino', 'L', 'Jl. Jati Gg.1 No.30 Tegal', '2016', NULL, 33, 1),
(240, '3381', 'Maria Regina Caeli A', 'P', 'Jl. Mayjend Suprapto 96 Tegal', '2016', NULL, 33, 1),
(241, '3382', 'Mikael Edric', 'L', 'Jl. Blimbing No.92 Tegal', '2016', NULL, 33, 1),
(242, '3383', 'Nicholas Austeen H', 'L', 'Jl. Letjend S Parman Tegal', '2016', NULL, 33, 1),
(243, '3384', 'Marchellyno Dimitri MS', 'L', 'Aspol Soeprapto No.39 Tegal', '2016', NULL, 33, 1),
(244, '3385', 'Sean Collin Abiel K', 'L', 'Jl. Ruslani HS No.5 Tegal', '2016', NULL, 33, 1),
(245, '3386', 'Theresia Levina W', 'P', 'Griya Mutiara D Blok E1 Tegal', '2016', NULL, 33, 1),
(246, '3387', 'Zazkia Putri Apriliani', 'P', 'Jl. Pisang Tegal', '2016', NULL, 33, 1),
(247, '3388', 'Calysta Earlena', 'P', 'Jl. Mangkukusuman Tegal', '2016', NULL, 33, 1),
(248, '3389', 'Christian Hartatnto', 'L', 'Kramat, Mejasem Timur Tegal', '2016', NULL, 33, 1),
(249, '3390', 'Clairin Ellisha Stevianne', 'P', 'Jl. Setia Budi No.91 Tegal', '2016', '', 33, 1),
(250, '3391', 'Clarence Aletha', 'P', 'Perum Villa Garden I No.5', '2016', NULL, 33, 1),
(251, '3392', 'Davin Nathanael Pranoto', 'L', 'Jl. Panggung Baru No.73 Tegal', '2016', NULL, 33, 1),
(252, '3393', 'Debby Aurelia Simarmata', 'P', 'JL.', '2016', NULL, 33, 1),
(253, '3394', 'Evander Vincent Setyawan', 'L', 'Jl. Wader No.27 Tegal', '2016', NULL, 33, 1),
(254, '3395', 'James Christopher Suyanto', 'L', 'Perum Griya Pamenang', '2016', NULL, 33, 1),
(255, '3396', 'Jane Antonia', 'P', 'Bimantara Estate Blok F 14', '2016', NULL, 33, 1),
(256, '3397', 'Janette Emilia De Rodat', 'P', 'Jl. Mangga Blok B2 No.1 Dampyak', '2016', NULL, 33, 1),
(257, '3398', 'Jesslyn Queen Angelina', 'P', 'Jl. Merpati 89', '2016', NULL, 33, 1),
(258, '3399', 'Mischa Avery Reemer', 'P', 'Sapphire Residence Blok J No.6', '2016', NULL, 33, 1),
(259, '3400', 'Shanika Marcelline Setiawan', 'P', 'Perum Eka Bahari C2 No.4', '2016', NULL, 33, 1),
(260, '3401', 'Thomas Wijaya', 'L', 'Jl. Sawo Barat No.26', '2016', NULL, 33, 1),
(261, '3402', 'Yohanes Andretti Sugiarto S', 'L', 'Villa Garden F32 No.8', '2016', NULL, 33, 1),
(262, '3403', 'Yohanes Dave Susanto', 'L', 'Perum Mutiara Estate No.5', '2016', NULL, 33, 1),
(263, '3404', 'Yosias Kristian Setiadi', 'L', 'Jl. Pisang No.36 Tegal', '2016', NULL, 33, 1),
(264, '3405', 'Risky Wijaya', 'L', 'Jl. Raya Suradadi RT 02 Rw 06', '2016', NULL, 33, 1),
(265, '3406', 'Zane Witanto', 'L', 'Kolonel Sugiono 98', '2016', NULL, 33, 1),
(266, '3336', 'Ananda Kevin A', 'L', 'Jl. Alor No.1 Tegal', '2015', NULL, 33, 1),
(267, '3366', 'Angelita Maria Th', 'P', 'Jl. Pala 13 No.221 Mejasem', '2015', NULL, 33, 1),
(268, '3337', 'Christian Steven J', 'L', 'Jl. AR. Hakim 177 Tegal', '2015', NULL, 33, 1),
(269, '3338', 'Christopher Marchello H', 'L', 'Jl. Blimbing No.36', '2015', NULL, 33, 1),
(270, '3367', 'Felicia Tifanna Osadhi', 'P', 'Jl. Supriyadi Slawi', '2015', NULL, 33, 1),
(271, '3339', 'Fransiscus Apriliano', 'L', 'Jl. Semanggi V/18 Mejasem', '2015', NULL, 33, 1),
(272, '3340', 'Griselda Johana Laureta', 'P', 'Jl. Kresna 31 Tegal', '2015', NULL, 33, 1),
(273, '3341', 'Jesslyn Abigail Xaveria', 'P', 'Jl. Delima 46 Tegal', '2015', NULL, 33, 1),
(274, '32568110', 'Angela Diana Djoyopranoto', 'P', 'Jl. Jendral Sudirman 181 Pemalang', '2015', NULL, 40, 1),
(275, '32908521', 'Angelia Priselia Tyasnaomi', 'P', 'Jl. Ternate RT 02/02 Bojongbata Pemalang', '2015', NULL, 40, 1),
(276, '39604739', 'Aurelia Sharon Asahelina', 'P', 'Jl. Sindoro No 75 Pemalang', '2015', NULL, 40, 1),
(277, '34534141', 'Aurellia Dwi Maharani', 'P', 'Jl. Agung No 6 Pemalang', '2015', NULL, 40, 1),
(278, '34534142', 'Candra Eka Wibowo', 'L', 'Jl. Mujaer No 469 Pr Sugihwaras Pemalang', '2015', NULL, 40, 1),
(279, '34534131', 'Dimas Ardhi Widiyandoko', 'L', 'Mulyoharjo RT 02 RW 15 Pemalang', '2015', NULL, 40, 1),
(280, '32846677', 'Efrina Elisabeth Sidauruk', 'P', 'Taman Mandiri RT 08/03 Taman Pemalang', '2015', NULL, 40, 1),
(281, '27911155', 'Geoffrey Gaudent Leif', 'L', 'Jl. Mawar No 21 Pemalang', '2015', NULL, 40, 1),
(282, '40332412', 'Gregory Wilson Wijaya', 'L', 'Cokra RT 03/06 Mulyoharjo Pemalang', '2015', NULL, 40, 1),
(283, '20200918', 'Hezron Yosua', 'L', 'Taman Asri Blok Munas No 19 Pemalang', '2015', NULL, 40, 1),
(285, '39782420', 'Kevin Putra Kawilarang', 'L', 'Jl. Bromo No 43 Pemalang', '2015', NULL, 40, 1),
(286, '34534134', 'Louise Hendrik Saputra Malau', 'L', 'Taman Agung Blok E 42 Taman Pemalang', '2015', NULL, 40, 1),
(287, '33686054', 'Michael Christian Hartono', 'L', 'Bojongbata RT 01/12 Jl. Ir. Sutami Pemalang', '2015', NULL, 40, 1),
(288, '40332413', 'Michelle Benedicta Widyaningtyas Purbandari', 'P', 'Jl. Gatot Subroto 105 Pemalang', '2015', NULL, 40, 1),
(289, '34534144', 'Naomi Angela', 'P', 'Jl. Jendral Sudirman No 27 Pemalang', '2015', NULL, 40, 1),
(290, '28347797', 'Natalia Kurniawati Bundoyo', 'P', 'Jl. Melati No 16 Pemalang', '2015', NULL, 40, 1),
(291, '34534132', 'Nevin Leonard Christanto', 'L', 'Jl. Agung No 1 Pemalang', '2015', NULL, 40, 1),
(292, '35088291', 'Nikolaus Haris Pramono', 'L', 'Pegatungan RT 05/09 Mulyoharjo Pemalang', '2015', NULL, 40, 1),
(293, '345541132', 'Radot Sahat Panghihutan S', 'L', 'Taman Agung Blok E 42 Taman Pemalang', '2015', NULL, 40, 1),
(294, '223605532', 'Veronika Habeahan', 'P', 'Taman Asri Blok A2 No 2A Pemalang', '2015', NULL, 40, 1),
(295, '34534133', 'William Sutarto', 'L', 'Mulyoharjo RT 02/15 Pemalang', '2015', NULL, 40, 1),
(296, '39552466', 'Yabes Hendra Putra', 'L', 'Jl. Anyelir Beji Pemalang', '2015', NULL, 40, 1),
(297, '12234402', 'Yongki Wilianto Pratama', 'L', 'Jl Jendral Sudirman 47 Pemalang', '2015', NULL, 40, 1),
(298, '23285087', 'Adrianus Daniel Silalahi', 'L', 'Perumahan Taman Agung Blok B 01', '2015', NULL, 40, 1),
(299, '34530472', 'Esteria Br. Turnip', 'P', 'Randusari RT 07/04 Petarukan Pemalang', '2015', NULL, 40, 1),
(300, '34532998', 'Ika Yunita Airmatatuzzakiya', 'P', 'Cokra RT 03/06 Mulyoharjo Pemalang', '2015', NULL, 40, 1),
(301, '34537107', 'Jian Vandiva Aurora', 'P', 'Jl. Rinjani RT 03/09 Wanarejan Taman Pemalang', '2015', NULL, 40, 1),
(302, '39060145', 'Karisma Diah Islamiati', 'P', 'Jl Tangkuban Perahu RT 04/04 Wanarejan Taman', '2015', NULL, 40, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_siswa`
--

CREATE TABLE `pembayaran_siswa` (
  `idbayar` bigint(20) NOT NULL,
  `tipe` varchar(30) NOT NULL,
  `idsiswa` bigint(20) NOT NULL,
  `nominal` double NOT NULL,
  `bulan` int(11) NOT NULL,
  `ket_trns` varchar(100) NOT NULL,
  `tgl_trans` datetime NOT NULL,
  `th_ajaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran_siswa`
--

INSERT INTO `pembayaran_siswa` (`idbayar`, `tipe`, `idsiswa`, `nominal`, `bulan`, `ket_trns`, `tgl_trans`, `th_ajaran`) VALUES
(1, 'SPP', 134, 0, 0, '0', '2018-03-21 15:06:15', 1),
(2, 'Kegiatan', 134, 0, 0, '0', '2018-03-21 15:06:15', 1),
(3, 'DPP', 134, 0, 0, '0', '2018-03-21 15:06:15', 1),
(7, 'SPP', 135, 0, 0, '0', '2018-03-22 07:45:56', 1),
(8, 'Kegiatan', 135, 0, 0, '0', '2018-03-22 07:45:56', 1),
(9, 'DPP', 135, 0, 0, '0', '2018-03-22 07:45:56', 1),
(10, 'SPP', 136, 0, 0, '0', '2018-03-22 07:47:55', 1),
(11, 'Kegiatan', 136, 0, 0, '0', '2018-03-22 07:47:55', 1),
(12, 'DPP', 136, 0, 0, '0', '2018-03-22 07:47:55', 1),
(14, 'SPP', 137, 0, 0, '0', '2018-03-22 07:52:10', 1),
(15, 'Kegiatan', 137, 0, 0, '0', '2018-03-22 07:52:10', 1),
(16, 'DPP', 137, 0, 0, '0', '2018-03-22 07:52:10', 1),
(17, 'SPP', 138, 0, 0, '0', '2018-03-22 07:53:55', 1),
(18, 'Kegiatan', 138, 0, 0, '0', '2018-03-22 07:53:55', 1),
(19, 'DPP', 138, 0, 0, '0', '2018-03-22 07:53:55', 1),
(20, 'SPP', 139, 0, 0, '0', '2018-03-22 07:56:06', 1),
(21, 'Kegiatan', 139, 0, 0, '0', '2018-03-22 07:56:06', 1),
(22, 'DPP', 139, 0, 0, '0', '2018-03-22 07:56:06', 1),
(23, 'SPP', 140, 0, 0, '0', '2018-03-22 07:56:55', 1),
(24, 'Kegiatan', 140, 0, 0, '0', '2018-03-22 07:56:55', 1),
(25, 'DPP', 140, 0, 0, '0', '2018-03-22 07:56:55', 1),
(26, 'SPP', 141, 0, 0, '0', '2018-03-22 07:58:27', 1),
(27, 'Kegiatan', 141, 0, 0, '0', '2018-03-22 07:58:27', 1),
(28, 'DPP', 141, 0, 0, '0', '2018-03-22 07:58:28', 1),
(29, 'SPP', 142, 0, 0, '0', '2018-03-22 07:59:34', 1),
(30, 'Kegiatan', 142, 0, 0, '0', '2018-03-22 07:59:34', 1),
(31, 'DPP', 142, 0, 0, '0', '2018-03-22 07:59:34', 1),
(32, 'SPP', 143, 0, 0, '0', '2018-03-22 08:02:10', 1),
(33, 'Kegiatan', 143, 0, 0, '0', '2018-03-22 08:02:10', 1),
(34, 'DPP', 143, 0, 0, '0', '2018-03-22 08:02:10', 1),
(35, 'SPP', 144, 0, 0, '0', '2018-03-22 08:04:38', 1),
(36, 'Kegiatan', 144, 0, 0, '0', '2018-03-22 08:04:38', 1),
(37, 'DPP', 144, 0, 0, '0', '2018-03-22 08:04:38', 1),
(38, 'SPP', 145, 0, 0, '0', '2018-03-22 08:05:43', 1),
(39, 'Kegiatan', 145, 0, 0, '0', '2018-03-22 08:05:43', 1),
(40, 'DPP', 145, 0, 0, '0', '2018-03-22 08:05:43', 1),
(41, 'SPP', 146, 0, 0, '0', '2018-03-22 08:07:34', 1),
(42, 'Kegiatan', 146, 0, 0, '0', '2018-03-22 08:07:34', 1),
(43, 'DPP', 146, 0, 0, '0', '2018-03-22 08:07:34', 1),
(44, 'SPP', 147, 0, 0, '0', '2018-03-22 08:09:13', 1),
(45, 'Kegiatan', 147, 0, 0, '0', '2018-03-22 08:09:13', 1),
(46, 'DPP', 147, 0, 0, '0', '2018-03-22 08:09:13', 1),
(47, 'SPP', 148, 0, 0, '0', '2018-03-22 08:15:15', 1),
(48, 'Kegiatan', 148, 0, 0, '0', '2018-03-22 08:15:15', 1),
(49, 'DPP', 148, 0, 0, '0', '2018-03-22 08:15:15', 1),
(50, 'SPP', 149, 0, 0, '0', '2018-03-22 08:16:15', 1),
(51, 'Kegiatan', 149, 0, 0, '0', '2018-03-22 08:16:15', 1),
(52, 'DPP', 149, 0, 0, '0', '2018-03-22 08:16:15', 1),
(53, 'SPP', 150, 0, 0, '0', '2018-03-22 08:17:43', 1),
(54, 'Kegiatan', 150, 0, 0, '0', '2018-03-22 08:17:43', 1),
(55, 'DPP', 150, 0, 0, '0', '2018-03-22 08:17:43', 1),
(56, 'SPP', 151, 0, 0, '0', '2018-03-22 08:18:37', 1),
(57, 'Kegiatan', 151, 0, 0, '0', '2018-03-22 08:18:37', 1),
(58, 'DPP', 151, 0, 0, '0', '2018-03-22 08:18:37', 1),
(59, 'SPP', 152, 0, 0, '0', '2018-03-22 08:19:32', 1),
(60, 'Kegiatan', 152, 0, 0, '0', '2018-03-22 08:19:32', 1),
(61, 'DPP', 152, 0, 0, '0', '2018-03-22 08:19:32', 1),
(62, 'SPP', 153, 0, 0, '0', '2018-03-22 08:20:56', 1),
(63, 'Kegiatan', 153, 0, 0, '0', '2018-03-22 08:20:56', 1),
(64, 'DPP', 153, 0, 0, '0', '2018-03-22 08:20:56', 1),
(65, 'SPP', 154, 0, 0, '0', '2018-03-22 08:21:47', 1),
(66, 'Kegiatan', 154, 0, 0, '0', '2018-03-22 08:21:47', 1),
(67, 'DPP', 154, 0, 0, '0', '2018-03-22 08:21:47', 1),
(68, 'SPP', 155, 0, 0, '0', '2018-03-22 08:22:37', 1),
(69, 'Kegiatan', 155, 0, 0, '0', '2018-03-22 08:22:37', 1),
(70, 'DPP', 155, 0, 0, '0', '2018-03-22 08:22:37', 1),
(71, 'SPP', 156, 0, 0, '0', '2018-03-22 08:24:07', 1),
(72, 'Kegiatan', 156, 0, 0, '0', '2018-03-22 08:24:07', 1),
(73, 'DPP', 156, 0, 0, '0', '2018-03-22 08:24:07', 1),
(74, 'SPP', 157, 0, 0, '0', '2018-03-22 08:25:56', 1),
(75, 'Kegiatan', 157, 0, 0, '0', '2018-03-22 08:25:56', 1),
(76, 'DPP', 157, 0, 0, '0', '2018-03-22 08:25:56', 1),
(77, 'SPP', 158, 0, 0, '0', '2018-03-22 08:26:55', 1),
(78, 'Kegiatan', 158, 0, 0, '0', '2018-03-22 08:26:55', 1),
(79, 'DPP', 158, 0, 0, '0', '2018-03-22 08:26:55', 1),
(134, 'SPP', 106, 0, 0, '0', '2018-03-22 11:17:28', 1),
(135, 'Kegiatan', 106, 0, 0, '0', '2018-03-22 11:17:28', 1),
(136, 'DPP', 106, 0, 0, '0', '2018-03-22 11:17:28', 1),
(137, 'SPP', 107, 0, 0, '0', '2018-03-22 11:20:16', 1),
(138, 'Kegiatan', 107, 0, 0, '0', '2018-03-22 11:20:16', 1),
(139, 'DPP', 107, 0, 0, '0', '2018-03-22 11:20:16', 1),
(140, 'SPP', 108, 0, 0, '0', '2018-03-22 11:23:09', 1),
(141, 'Kegiatan', 108, 0, 0, '0', '2018-03-22 11:23:09', 1),
(142, 'DPP', 108, 0, 0, '0', '2018-03-22 11:23:09', 1),
(143, 'SPP', 109, 0, 0, '0', '2018-03-22 11:24:39', 1),
(144, 'Kegiatan', 109, 0, 0, '0', '2018-03-22 11:24:39', 1),
(145, 'DPP', 109, 0, 0, '0', '2018-03-22 11:24:39', 1),
(146, 'SPP', 110, 0, 0, '0', '2018-03-22 11:26:21', 1),
(147, 'Kegiatan', 110, 0, 0, '0', '2018-03-22 11:26:22', 1),
(148, 'DPP', 110, 0, 0, '0', '2018-03-22 11:26:23', 1),
(149, 'SPP', 111, 0, 0, '0', '2018-03-22 11:30:47', 1),
(150, 'Kegiatan', 111, 0, 0, '0', '2018-03-22 11:30:47', 1),
(151, 'DPP', 111, 0, 0, '0', '2018-03-22 11:30:47', 1),
(152, 'SPP', 112, 0, 0, '0', '2018-03-26 07:12:33', 1),
(153, 'Kegiatan', 112, 0, 0, '0', '2018-03-26 07:12:33', 1),
(154, 'DPP', 112, 0, 0, '0', '2018-03-26 07:12:33', 1),
(155, 'SPP', 113, 0, 0, '0', '2018-03-26 07:14:21', 1),
(156, 'Kegiatan', 113, 0, 0, '0', '2018-03-26 07:14:21', 1),
(157, 'DPP', 113, 0, 0, '0', '2018-03-26 07:14:21', 1),
(158, 'SPP', 114, 0, 0, '0', '2018-03-26 07:15:09', 1),
(159, 'Kegiatan', 114, 0, 0, '0', '2018-03-26 07:15:09', 1),
(160, 'DPP', 114, 0, 0, '0', '2018-03-26 07:15:09', 1),
(161, 'SPP', 115, 0, 0, '0', '2018-03-26 07:15:55', 1),
(162, 'Kegiatan', 115, 0, 0, '0', '2018-03-26 07:15:55', 1),
(163, 'DPP', 115, 0, 0, '0', '2018-03-26 07:15:55', 1),
(164, 'SPP', 116, 0, 0, '0', '2018-03-26 07:16:50', 1),
(165, 'Kegiatan', 116, 0, 0, '0', '2018-03-26 07:16:50', 1),
(166, 'DPP', 116, 0, 0, '0', '2018-03-26 07:16:50', 1),
(167, 'SPP', 117, 0, 0, '0', '2018-03-26 07:17:42', 1),
(168, 'Kegiatan', 117, 0, 0, '0', '2018-03-26 07:17:42', 1),
(169, 'DPP', 117, 0, 0, '0', '2018-03-26 07:17:42', 1),
(170, 'SPP', 118, 0, 0, '0', '2018-03-26 07:18:39', 1),
(171, 'Kegiatan', 118, 0, 0, '0', '2018-03-26 07:18:39', 1),
(172, 'DPP', 118, 0, 0, '0', '2018-03-26 07:18:39', 1),
(173, 'SPP', 119, 0, 0, '0', '2018-03-26 07:20:44', 1),
(174, 'Kegiatan', 119, 0, 0, '0', '2018-03-26 07:20:44', 1),
(175, 'DPP', 119, 0, 0, '0', '2018-03-26 07:20:44', 1),
(176, 'SPP', 120, 0, 0, '0', '2018-03-26 07:29:59', 1),
(177, 'Kegiatan', 120, 0, 0, '0', '2018-03-26 07:29:59', 1),
(178, 'DPP', 120, 0, 0, '0', '2018-03-26 07:29:59', 1),
(179, 'SPP', 121, 0, 0, '0', '2018-03-26 07:34:01', 1),
(180, 'Kegiatan', 121, 0, 0, '0', '2018-03-26 07:34:01', 1),
(181, 'DPP', 121, 0, 0, '0', '2018-03-26 07:34:01', 1),
(182, 'SPP', 122, 0, 0, '0', '2018-03-26 07:36:46', 1),
(183, 'Kegiatan', 122, 0, 0, '0', '2018-03-26 07:36:46', 1),
(184, 'DPP', 122, 0, 0, '0', '2018-03-26 07:36:46', 1),
(185, 'SPP', 123, 0, 0, '0', '2018-03-26 07:37:47', 1),
(186, 'Kegiatan', 123, 0, 0, '0', '2018-03-26 07:37:47', 1),
(187, 'DPP', 123, 0, 0, '0', '2018-03-26 07:37:47', 1),
(188, 'SPP', 124, 0, 0, '0', '2018-03-26 07:38:31', 1),
(189, 'Kegiatan', 124, 0, 0, '0', '2018-03-26 07:38:31', 1),
(190, 'DPP', 124, 0, 0, '0', '2018-03-26 07:38:31', 1),
(191, 'SPP', 125, 0, 0, '0', '2018-03-26 07:42:22', 1),
(192, 'Kegiatan', 125, 0, 0, '0', '2018-03-26 07:42:22', 1),
(193, 'DPP', 125, 0, 0, '0', '2018-03-26 07:42:22', 1),
(194, 'SPP', 126, 0, 0, '0', '2018-03-26 07:44:43', 1),
(195, 'Kegiatan', 126, 0, 0, '0', '2018-03-26 07:44:43', 1),
(196, 'DPP', 126, 0, 0, '0', '2018-03-26 07:44:43', 1),
(197, 'SPP', 127, 0, 0, '0', '2018-03-26 07:46:46', 1),
(198, 'Kegiatan', 127, 0, 0, '0', '2018-03-26 07:46:46', 1),
(199, 'DPP', 127, 0, 0, '0', '2018-03-26 07:46:46', 1),
(200, 'SPP', 128, 0, 0, '0', '2018-03-26 07:47:43', 1),
(201, 'Kegiatan', 128, 0, 0, '0', '2018-03-26 07:47:43', 1),
(202, 'DPP', 128, 0, 0, '0', '2018-03-26 07:47:43', 1),
(203, 'SPP', 129, 0, 0, '0', '2018-03-26 07:49:04', 1),
(204, 'Kegiatan', 129, 0, 0, '0', '2018-03-26 07:49:04', 1),
(205, 'DPP', 129, 0, 0, '0', '2018-03-26 07:49:04', 1),
(206, 'SPP', 130, 0, 0, '0', '2018-03-26 07:50:14', 1),
(207, 'Kegiatan', 130, 0, 0, '0', '2018-03-26 07:50:14', 1),
(208, 'DPP', 130, 0, 0, '0', '2018-03-26 07:50:14', 1),
(209, 'SPP', 131, 0, 0, '0', '2018-03-26 07:53:16', 1),
(210, 'Kegiatan', 131, 0, 0, '0', '2018-03-26 07:53:16', 1),
(211, 'DPP', 131, 0, 0, '0', '2018-03-26 07:53:16', 1),
(212, 'SPP', 132, 0, 0, '0', '2018-03-26 07:59:10', 1),
(213, 'Kegiatan', 132, 0, 0, '0', '2018-03-26 07:59:10', 1),
(214, 'DPP', 132, 0, 0, '0', '2018-03-26 07:59:10', 1),
(215, 'SPP', 133, 0, 0, '0', '2018-03-26 08:01:49', 1),
(216, 'Kegiatan', 133, 0, 0, '0', '2018-03-26 08:01:49', 1),
(217, 'DPP', 133, 0, 0, '0', '2018-03-26 08:01:49', 1),
(218, 'SPP', 159, 0, 0, '0', '2018-03-26 08:06:20', 1),
(219, 'Kegiatan', 159, 0, 0, '0', '2018-03-26 08:06:20', 1),
(220, 'DPP', 159, 0, 0, '0', '2018-03-26 08:06:20', 1),
(221, 'SPP', 160, 0, 0, '0', '2018-03-26 08:14:40', 1),
(222, 'Kegiatan', 160, 0, 0, '0', '2018-03-26 08:14:40', 1),
(223, 'DPP', 160, 0, 0, '0', '2018-03-26 08:14:40', 1),
(224, 'SPP', 161, 0, 0, '0', '2018-03-26 08:16:38', 1),
(225, 'Kegiatan', 161, 0, 0, '0', '2018-03-26 08:16:38', 1),
(226, 'DPP', 161, 0, 0, '0', '2018-03-26 08:16:38', 1),
(227, 'SPP', 162, 0, 0, '0', '2018-03-26 08:18:15', 1),
(228, 'Kegiatan', 162, 0, 0, '0', '2018-03-26 08:18:15', 1),
(229, 'DPP', 162, 0, 0, '0', '2018-03-26 08:18:15', 1),
(230, 'SPP', 163, 0, 0, '0', '2018-03-26 08:19:14', 1),
(231, 'Kegiatan', 163, 0, 0, '0', '2018-03-26 08:19:14', 1),
(232, 'DPP', 163, 0, 0, '0', '2018-03-26 08:19:14', 1),
(233, 'SPP', 164, 0, 0, '0', '2018-03-26 08:21:03', 1),
(234, 'Kegiatan', 164, 0, 0, '0', '2018-03-26 08:21:04', 1),
(235, 'DPP', 164, 0, 0, '0', '2018-03-26 08:21:04', 1),
(236, 'SPP', 165, 0, 0, '0', '2018-03-26 08:22:03', 1),
(237, 'Kegiatan', 165, 0, 0, '0', '2018-03-26 08:22:03', 1),
(238, 'DPP', 165, 0, 0, '0', '2018-03-26 08:22:03', 1),
(239, 'SPP', 166, 0, 0, '0', '2018-03-26 08:24:21', 1),
(240, 'Kegiatan', 166, 0, 0, '0', '2018-03-26 08:24:21', 1),
(241, 'DPP', 166, 0, 0, '0', '2018-03-26 08:24:21', 1),
(242, 'SPP', 167, 0, 0, '0', '2018-03-26 08:25:30', 1),
(243, 'Kegiatan', 167, 0, 0, '0', '2018-03-26 08:25:30', 1),
(244, 'DPP', 167, 0, 0, '0', '2018-03-26 08:25:30', 1),
(245, 'SPP', 168, 0, 0, '0', '2018-03-26 08:26:18', 1),
(246, 'Kegiatan', 168, 0, 0, '0', '2018-03-26 08:26:18', 1),
(247, 'DPP', 168, 0, 0, '0', '2018-03-26 08:26:18', 1),
(251, 'SPP', 169, 0, 0, '0', '2018-03-26 08:34:25', 1),
(252, 'Kegiatan', 169, 0, 0, '0', '2018-03-26 08:34:25', 1),
(253, 'DPP', 169, 0, 0, '0', '2018-03-26 08:34:25', 1),
(254, 'SPP', 170, 0, 0, '0', '2018-03-26 08:35:10', 1),
(255, 'Kegiatan', 170, 0, 0, '0', '2018-03-26 08:35:10', 1),
(256, 'DPP', 170, 0, 0, '0', '2018-03-26 08:35:10', 1),
(257, 'SPP', 171, 0, 0, '0', '2018-03-26 08:37:12', 1),
(258, 'Kegiatan', 171, 0, 0, '0', '2018-03-26 08:37:12', 1),
(259, 'DPP', 171, 0, 0, '0', '2018-03-26 08:37:12', 1),
(260, 'SPP', 172, 0, 0, '0', '2018-03-26 08:38:40', 1),
(261, 'Kegiatan', 172, 0, 0, '0', '2018-03-26 08:38:40', 1),
(262, 'DPP', 172, 0, 0, '0', '2018-03-26 08:38:40', 1),
(263, 'SPP', 173, 0, 0, '0', '2018-03-26 08:39:30', 1),
(264, 'Kegiatan', 173, 0, 0, '0', '2018-03-26 08:39:30', 1),
(265, 'DPP', 173, 0, 0, '0', '2018-03-26 08:39:30', 1),
(266, 'SPP', 174, 0, 0, '0', '2018-03-26 08:41:00', 1),
(267, 'Kegiatan', 174, 0, 0, '0', '2018-03-26 08:41:00', 1),
(268, 'DPP', 174, 0, 0, '0', '2018-03-26 08:41:00', 1),
(269, 'SPP', 175, 0, 0, '0', '2018-03-26 08:42:24', 1),
(270, 'Kegiatan', 175, 0, 0, '0', '2018-03-26 08:42:24', 1),
(271, 'DPP', 175, 0, 0, '0', '2018-03-26 08:42:24', 1),
(272, 'SPP', 176, 0, 0, '0', '2018-03-26 08:44:38', 1),
(273, 'Kegiatan', 176, 0, 0, '0', '2018-03-26 08:44:38', 1),
(274, 'DPP', 176, 0, 0, '0', '2018-03-26 08:44:38', 1),
(275, 'SPP', 177, 0, 0, '0', '2018-03-26 08:45:26', 1),
(276, 'Kegiatan', 177, 0, 0, '0', '2018-03-26 08:45:26', 1),
(277, 'DPP', 177, 0, 0, '0', '2018-03-26 08:45:26', 1),
(278, 'SPP', 178, 0, 0, '0', '2018-03-26 08:46:15', 1),
(279, 'Kegiatan', 178, 0, 0, '0', '2018-03-26 08:46:15', 1),
(280, 'DPP', 178, 0, 0, '0', '2018-03-26 08:46:15', 1),
(281, 'SPP', 179, 0, 0, '0', '2018-03-26 08:51:07', 1),
(282, 'Kegiatan', 179, 0, 0, '0', '2018-03-26 08:51:07', 1),
(283, 'DPP', 179, 0, 0, '0', '2018-03-26 08:51:07', 1),
(284, 'SPP', 180, 0, 0, '0', '2018-03-26 08:52:24', 1),
(285, 'Kegiatan', 180, 0, 0, '0', '2018-03-26 08:52:24', 1),
(286, 'DPP', 180, 0, 0, '0', '2018-03-26 08:52:24', 1),
(287, 'SPP', 181, 0, 0, '0', '2018-03-26 08:53:27', 1),
(288, 'Kegiatan', 181, 0, 0, '0', '2018-03-26 08:53:27', 1),
(289, 'DPP', 181, 0, 0, '0', '2018-03-26 08:53:27', 1),
(290, 'SPP', 182, 0, 0, '0', '2018-03-26 08:54:07', 1),
(291, 'Kegiatan', 182, 0, 0, '0', '2018-03-26 08:54:07', 1),
(292, 'DPP', 182, 0, 0, '0', '2018-03-26 08:54:07', 1),
(293, 'SPP', 183, 0, 0, '0', '2018-03-26 08:56:14', 1),
(294, 'Kegiatan', 183, 0, 0, '0', '2018-03-26 08:56:14', 1),
(295, 'DPP', 183, 0, 0, '0', '2018-03-26 08:56:14', 1),
(296, 'SPP', 184, 0, 0, '0', '2018-03-26 08:59:04', 1),
(297, 'Kegiatan', 184, 0, 0, '0', '2018-03-26 08:59:04', 1),
(298, 'DPP', 184, 0, 0, '0', '2018-03-26 08:59:04', 1),
(299, 'SPP', 185, 0, 0, '0', '2018-03-26 09:01:14', 1),
(300, 'Kegiatan', 185, 0, 0, '0', '2018-03-26 09:01:14', 1),
(301, 'DPP', 185, 0, 0, '0', '2018-03-26 09:01:14', 1),
(302, 'SPP', 186, 0, 0, '0', '2018-03-26 09:02:19', 1),
(303, 'Kegiatan', 186, 0, 0, '0', '2018-03-26 09:02:19', 1),
(304, 'DPP', 186, 0, 0, '0', '2018-03-26 09:02:19', 1),
(305, 'SPP', 187, 0, 0, '0', '2018-03-26 09:07:40', 1),
(306, 'Kegiatan', 187, 0, 0, '0', '2018-03-26 09:07:40', 1),
(307, 'DPP', 187, 0, 0, '0', '2018-03-26 09:07:40', 1),
(308, 'SPP', 188, 0, 0, '0', '2018-03-26 09:08:39', 1),
(309, 'Kegiatan', 188, 0, 0, '0', '2018-03-26 09:08:39', 1),
(310, 'DPP', 188, 0, 0, '0', '2018-03-26 09:08:39', 1),
(311, 'SPP', 190, 0, 0, '0', '2018-03-26 09:09:25', 1),
(312, 'Kegiatan', 190, 0, 0, '0', '2018-03-26 09:09:25', 1),
(313, 'DPP', 190, 0, 0, '0', '2018-03-26 09:09:25', 1),
(314, 'SPP', 191, 0, 0, '0', '2018-03-26 09:10:55', 1),
(315, 'Kegiatan', 191, 0, 0, '0', '2018-03-26 09:10:55', 1),
(316, 'DPP', 191, 0, 0, '0', '2018-03-26 09:10:55', 1),
(317, 'SPP', 193, 0, 0, '0', '2018-03-26 09:12:39', 1),
(318, 'Kegiatan', 193, 0, 0, '0', '2018-03-26 09:12:39', 1),
(319, 'DPP', 193, 0, 0, '0', '2018-03-26 09:12:39', 1),
(320, 'SPP', 194, 0, 0, '0', '2018-03-26 09:14:36', 1),
(321, 'Kegiatan', 194, 0, 0, '0', '2018-03-26 09:14:36', 1),
(322, 'DPP', 194, 0, 0, '0', '2018-03-26 09:14:36', 1),
(323, 'SPP', 196, 0, 0, '0', '2018-03-26 09:15:46', 1),
(324, 'Kegiatan', 196, 0, 0, '0', '2018-03-26 09:15:46', 1),
(325, 'DPP', 196, 0, 0, '0', '2018-03-26 09:15:46', 1),
(326, 'SPP', 197, 0, 0, '0', '2018-03-26 09:17:09', 1),
(327, 'Kegiatan', 197, 0, 0, '0', '2018-03-26 09:17:09', 1),
(328, 'DPP', 197, 0, 0, '0', '2018-03-26 09:17:09', 1),
(329, 'SPP', 198, 0, 0, '0', '2018-03-26 09:20:19', 1),
(330, 'Kegiatan', 198, 0, 0, '0', '2018-03-26 09:20:19', 1),
(331, 'DPP', 198, 0, 0, '0', '2018-03-26 09:20:19', 1),
(332, 'SPP', 199, 0, 0, '0', '2018-03-26 09:22:11', 1),
(333, 'Kegiatan', 199, 0, 0, '0', '2018-03-26 09:22:11', 1),
(334, 'DPP', 199, 0, 0, '0', '2018-03-26 09:22:11', 1),
(335, 'SPP', 201, 0, 0, '0', '2018-03-26 09:23:04', 1),
(336, 'Kegiatan', 201, 0, 0, '0', '2018-03-26 09:23:04', 1),
(337, 'DPP', 201, 0, 0, '0', '2018-03-26 09:23:04', 1),
(338, 'SPP', 202, 0, 0, '0', '2018-03-26 09:24:17', 1),
(339, 'Kegiatan', 202, 0, 0, '0', '2018-03-26 09:24:17', 1),
(340, 'DPP', 202, 0, 0, '0', '2018-03-26 09:24:17', 1),
(341, 'SPP', 203, 0, 0, '0', '2018-03-26 09:25:18', 1),
(342, 'Kegiatan', 203, 0, 0, '0', '2018-03-26 09:25:18', 1),
(343, 'DPP', 203, 0, 0, '0', '2018-03-26 09:25:18', 1),
(344, 'SPP', 204, 0, 0, '0', '2018-03-26 09:26:11', 1),
(345, 'Kegiatan', 204, 0, 0, '0', '2018-03-26 09:26:11', 1),
(346, 'DPP', 204, 0, 0, '0', '2018-03-26 09:26:11', 1),
(347, 'SPP', 205, 0, 0, '0', '2018-03-26 09:27:04', 1),
(348, 'Kegiatan', 205, 0, 0, '0', '2018-03-26 09:27:04', 1),
(349, 'DPP', 205, 0, 0, '0', '2018-03-26 09:27:04', 1),
(350, 'SPP', 206, 0, 0, '0', '2018-03-26 09:28:04', 1),
(351, 'Kegiatan', 206, 0, 0, '0', '2018-03-26 09:28:04', 1),
(352, 'DPP', 206, 0, 0, '0', '2018-03-26 09:28:04', 1),
(353, 'SPP', 207, 0, 0, '0', '2018-03-26 09:29:06', 1),
(354, 'Kegiatan', 207, 0, 0, '0', '2018-03-26 09:29:06', 1),
(355, 'DPP', 207, 0, 0, '0', '2018-03-26 09:29:06', 1),
(356, 'SPP', 208, 0, 0, '0', '2018-03-26 09:29:49', 1),
(357, 'Kegiatan', 208, 0, 0, '0', '2018-03-26 09:29:49', 1),
(358, 'DPP', 208, 0, 0, '0', '2018-03-26 09:29:49', 1),
(359, 'SPP', 209, 0, 0, '0', '2018-03-26 09:34:47', 1),
(360, 'Kegiatan', 209, 0, 0, '0', '2018-03-26 09:34:47', 1),
(361, 'DPP', 209, 0, 0, '0', '2018-03-26 09:34:47', 1),
(362, 'SPP', 211, 0, 0, '0', '2018-03-26 09:35:39', 1),
(363, 'Kegiatan', 211, 0, 0, '0', '2018-03-26 09:35:39', 1),
(364, 'DPP', 211, 0, 0, '0', '2018-03-26 09:35:39', 1),
(365, 'SPP', 216, 0, 0, '0', '2018-03-26 09:38:32', 1),
(366, 'Kegiatan', 216, 0, 0, '0', '2018-03-26 09:38:32', 1),
(367, 'DPP', 216, 0, 0, '0', '2018-03-26 09:38:32', 1),
(368, 'Kegiatan', 134, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:42:11', 1),
(369, 'Kegiatan', 135, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:42:54', 1),
(370, 'Kegiatan', 136, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:43:14', 1),
(371, 'Kegiatan', 137, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:43:47', 1),
(372, 'Kegiatan', 138, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:44:16', 1),
(373, 'Kegiatan', 139, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:44:43', 1),
(374, 'Kegiatan', 140, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:45:03', 1),
(375, 'Kegiatan', 141, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:45:32', 1),
(376, 'Kegiatan', 142, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:45:54', 1),
(377, 'Kegiatan', 143, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:46:17', 1),
(378, 'Kegiatan', 144, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:46:42', 1),
(379, 'Kegiatan', 145, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:47:08', 1),
(380, 'Kegiatan', 146, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:47:40', 1),
(381, 'Kegiatan', 147, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:48:00', 1),
(382, 'Kegiatan', 148, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:48:33', 1),
(383, 'Kegiatan', 149, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:48:55', 1),
(384, 'Kegiatan', 150, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:50:24', 1),
(385, 'Kegiatan', 151, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:56:31', 1),
(386, 'Kegiatan', 152, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:56:52', 1),
(387, 'Kegiatan', 153, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:57:15', 1),
(388, 'Kegiatan', 154, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:57:30', 1),
(389, 'Kegiatan', 155, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:57:50', 1),
(390, 'Kegiatan', 156, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:58:08', 1),
(391, 'Kegiatan', 157, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:58:36', 1),
(392, 'Kegiatan', 158, 350000, 0, 'Angsuran Ke-1', '2018-03-26 09:59:10', 1),
(393, 'Kegiatan', 106, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:03:24', 1),
(394, 'Kegiatan', 107, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:03:56', 1),
(395, 'Kegiatan', 108, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:04:59', 1),
(396, 'Kegiatan', 109, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:05:24', 1),
(397, 'Kegiatan', 110, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:06:08', 1),
(398, 'Kegiatan', 111, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:07:03', 1),
(399, 'Kegiatan', 112, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:07:44', 1),
(400, 'Kegiatan', 113, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:08:40', 1),
(401, 'Kegiatan', 114, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:09:07', 1),
(402, 'Kegiatan', 115, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:09:32', 1),
(403, 'Kegiatan', 116, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:09:54', 1),
(404, 'Kegiatan', 117, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:10:21', 1),
(405, 'Kegiatan', 118, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:10:52', 1),
(406, 'Kegiatan', 119, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:12:59', 1),
(407, 'Kegiatan', 120, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:19:52', 1),
(408, 'Kegiatan', 121, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:22:51', 1),
(409, 'Kegiatan', 122, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:23:23', 1),
(410, 'Kegiatan', 123, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:23:42', 1),
(411, 'Kegiatan', 124, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:24:07', 1),
(412, 'Kegiatan', 125, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:24:40', 1),
(413, 'Kegiatan', 127, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:25:12', 1),
(414, 'Kegiatan', 128, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:25:32', 1),
(415, 'Kegiatan', 129, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:26:20', 1),
(416, 'Kegiatan', 130, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:26:38', 1),
(417, 'Kegiatan', 131, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:26:58', 1),
(418, 'Kegiatan', 132, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:27:29', 1),
(419, 'Kegiatan', 133, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:27:53', 1),
(420, 'Kegiatan', 159, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:29:51', 1),
(421, 'Kegiatan', 160, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:30:14', 1),
(422, 'Kegiatan', 161, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:30:37', 1),
(423, 'Kegiatan', 162, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:31:18', 1),
(424, 'Kegiatan', 163, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:31:56', 1),
(425, 'Kegiatan', 164, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:32:15', 1),
(426, 'Kegiatan', 165, 350000, 0, 'Angsuran Ke-1', '2018-03-26 10:32:42', 1),
(427, 'Kegiatan', 166, 350000, 0, 'Angsuran Ke-1', '2018-03-26 12:58:07', 1),
(428, 'Kegiatan', 167, 350000, 0, 'Angsuran Ke-1', '2018-03-26 12:58:33', 1),
(429, 'Kegiatan', 169, 350000, 0, 'Angsuran Ke-1', '2018-03-26 12:58:59', 1),
(430, 'Kegiatan', 170, 350000, 0, 'Angsuran Ke-1', '2018-03-26 12:59:18', 1),
(431, 'Kegiatan', 172, 350000, 0, 'Angsuran Ke-1', '2018-03-26 12:59:45', 1),
(432, 'Kegiatan', 173, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:01:12', 1),
(433, 'Kegiatan', 174, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:01:43', 1),
(434, 'Kegiatan', 175, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:03:29', 1),
(435, 'Kegiatan', 176, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:04:36', 1),
(436, 'Kegiatan', 177, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:04:57', 1),
(437, 'Kegiatan', 178, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:05:22', 1),
(438, 'Kegiatan', 179, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:05:47', 1),
(439, 'Kegiatan', 180, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:06:43', 1),
(440, 'Kegiatan', 181, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:07:16', 1),
(441, 'Kegiatan', 182, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:07:40', 1),
(442, 'Kegiatan', 183, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:08:07', 1),
(443, 'Kegiatan', 168, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:09:10', 1),
(444, 'Kegiatan', 171, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:10:13', 1),
(445, 'Kegiatan', 184, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:12:02', 1),
(446, 'Kegiatan', 185, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:12:46', 1),
(447, 'Kegiatan', 186, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:13:14', 1),
(448, 'Kegiatan', 187, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:14:18', 1),
(449, 'Kegiatan', 188, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:14:57', 1),
(450, 'Kegiatan', 190, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:15:22', 1),
(451, 'Kegiatan', 191, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:16:32', 1),
(452, 'Kegiatan', 216, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:17:17', 1),
(453, 'Kegiatan', 193, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:17:39', 1),
(454, 'Kegiatan', 194, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:18:45', 1),
(455, 'Kegiatan', 196, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:19:44', 1),
(456, 'Kegiatan', 197, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:21:20', 1),
(457, 'Kegiatan', 198, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:22:18', 1),
(458, 'Kegiatan', 199, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:25:46', 1),
(459, 'Kegiatan', 201, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:26:10', 1),
(460, 'Kegiatan', 202, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:27:03', 1),
(461, 'Kegiatan', 203, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:27:26', 1),
(462, 'Kegiatan', 204, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:28:27', 1),
(463, 'Kegiatan', 205, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:29:23', 1),
(464, 'Kegiatan', 206, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:30:05', 1),
(465, 'Kegiatan', 207, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:30:29', 1),
(466, 'Kegiatan', 208, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:30:49', 1),
(467, 'Kegiatan', 209, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:31:23', 1),
(468, 'Kegiatan', 211, 350000, 0, 'Angsuran Ke-1', '2018-03-26 13:31:49', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pl_kelas`
--

CREATE TABLE `pl_kelas` (
  `id_plk` bigint(20) NOT NULL,
  `id_pelajar` bigint(20) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `th_ajar` varchar(4) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pl_kelas`
--

INSERT INTO `pl_kelas` (`id_plk`, `id_pelajar`, `id_kelas`, `th_ajar`, `status`) VALUES
(1, 1, 6, '2016', 1),
(2, 2, 6, '2016', 1),
(3, 3, 6, '2016', 1),
(4, 4, 6, '2016', 1),
(5, 5, 6, '2016', 1),
(6, 6, 6, '2017', 1),
(7, 7, 6, '2016', 1),
(8, 8, 6, '2016', 1),
(9, 9, 6, '2016', 1),
(10, 10, 6, '2016', 1),
(11, 11, 6, '2016', 1),
(12, 12, 6, '2016', 1),
(13, 13, 6, '2016', 1),
(14, 14, 6, '2016', 1),
(15, 15, 6, '2016', 1),
(16, 16, 6, '2016', 1),
(17, 17, 6, '2016', 1),
(18, 18, 6, '2016', 1),
(19, 19, 6, '2016', 1),
(57, 57, 28, '2016', 1),
(21, 21, 6, '2016', 1),
(22, 22, 6, '2016', 1),
(23, 23, 6, '2106', 1),
(24, 24, 6, '2016', 1),
(25, 25, 7, '2016', 1),
(26, 26, 7, '2016', 1),
(27, 27, 7, '2016', 1),
(28, 28, 7, '2016', 1),
(29, 29, 7, '2016', 1),
(30, 30, 7, '2016', 1),
(31, 31, 7, '2016', 1),
(32, 32, 7, '2016', 1),
(33, 33, 7, '2016', 1),
(34, 34, 7, '2016', 1),
(35, 35, 7, '2016', 1),
(36, 36, 7, '2016', 1),
(37, 37, 7, '2016', 1),
(38, 38, 7, '2016', 1),
(39, 39, 7, '2016', 1),
(40, 40, 7, '2016', 1),
(41, 41, 7, '2016', 1),
(42, 42, 7, '2016', 1),
(43, 43, 7, '2016', 1),
(44, 44, 7, '2016', 1),
(45, 45, 7, '2016', 1),
(46, 46, 7, '2016', 1),
(47, 47, 7, '2016', 1),
(48, 48, 7, '2016', 1),
(49, 49, 7, '2016', 1),
(50, 50, 7, '2016', 1),
(56, 56, 28, '2016', 1),
(55, 55, 28, '2016', 1),
(58, 58, 28, '2016', 1),
(59, 59, 28, '2016', 1),
(60, 60, 28, '2016', 1),
(61, 61, 28, '2016', 1),
(62, 62, 28, '2016', 1),
(63, 63, 28, '2016', 1),
(64, 64, 28, '2016', 1),
(65, 65, 28, '2016', 1),
(66, 66, 28, '2016', 1),
(67, 67, 28, '2016', 1),
(68, 68, 28, '2016', 1),
(69, 69, 28, '2016', 1),
(70, 70, 28, '2016', 1),
(71, 71, 28, '2016', 1),
(72, 72, 28, '2016', 1),
(73, 73, 28, '2016', 1),
(74, 74, 28, '2016', 1),
(75, 75, 28, '2016', 1),
(76, 76, 28, '2016', 1),
(77, 77, 28, '2016', 1),
(78, 78, 28, '2016', 1),
(79, 79, 28, '2016', 1),
(80, 80, 29, '2016', 1),
(81, 81, 29, '2016', 1),
(82, 82, 29, '2016', 1),
(83, 83, 29, '2016', 1),
(84, 84, 29, '2016', 1),
(85, 85, 29, '2016', 1),
(86, 86, 29, '2016', 1),
(87, 87, 29, '2016', 1),
(88, 88, 29, '2016', 1),
(89, 89, 29, '2016', 1),
(90, 90, 29, '2016', 1),
(91, 91, 29, '2016', 1),
(92, 92, 29, '2016', 1),
(93, 93, 29, '2016', 1),
(94, 94, 29, '2016', 1),
(95, 95, 29, '2016', 1),
(96, 96, 29, '2016', 1),
(97, 97, 29, '2016', 1),
(98, 98, 29, '2016', 1),
(99, 99, 29, '2016', 1),
(100, 100, 29, '2016', 1),
(101, 101, 29, '2016', 1),
(102, 102, 29, '2016', 1),
(103, 103, 29, '2016', 1),
(104, 104, 29, '2016', 1),
(105, 105, 29, '2016', 1),
(106, 106, 30, '2016', 1),
(107, 107, 30, '2016', 1),
(108, 108, 30, '2017', 1),
(109, 109, 30, '2017', 1),
(110, 110, 30, '2017', 1),
(111, 111, 30, '2017', 1),
(112, 112, 30, '1', 1),
(113, 113, 30, '1', 1),
(114, 114, 30, '1', 1),
(115, 115, 30, '2017', 1),
(116, 116, 30, '1', 1),
(117, 117, 30, '1', 1),
(118, 118, 30, '1', 1),
(119, 119, 30, '2017', 1),
(120, 120, 30, '2017', 1),
(121, 121, 30, '2017', 1),
(122, 122, 30, '2017', 1),
(123, 123, 30, '2017', 1),
(124, 124, 30, '2017', 1),
(125, 125, 30, '2017', 1),
(126, 126, 30, '2017', 1),
(127, 127, 30, '2017', 1),
(128, 128, 30, '2017', 1),
(129, 129, 30, '2017', 1),
(130, 130, 30, '2017', 1),
(131, 131, 30, '2017', 1),
(132, 132, 30, '2017', 1),
(133, 133, 30, '2017', 1),
(134, 134, 31, '1', 1),
(135, 135, 31, '2017', 1),
(136, 136, 31, '2017', 1),
(137, 137, 31, '1', 1),
(138, 138, 31, '2017', 1),
(139, 139, 31, '2017', 1),
(140, 140, 31, '2017', 1),
(141, 141, 31, '2017', 1),
(142, 142, 31, '2017', 1),
(143, 143, 31, '2017', 1),
(144, 144, 31, '2017', 1),
(145, 145, 31, '2017', 1),
(146, 146, 31, '2017', 1),
(147, 147, 31, '2017', 1),
(148, 148, 31, '2017', 1),
(149, 149, 31, '2017', 1),
(150, 150, 31, '2017', 1),
(151, 151, 31, '2017', 1),
(152, 152, 31, '2017', 1),
(153, 153, 31, '2017', 1),
(154, 154, 31, '2017', 1),
(155, 155, 31, '2017', 1),
(156, 156, 31, '2017', 1),
(157, 157, 31, '2017', 1),
(158, 158, 31, '2017', 1),
(159, 159, 32, '2017', 1),
(160, 160, 32, '2017', 1),
(161, 161, 32, '2017', 1),
(162, 162, 32, '2017', 1),
(163, 163, 32, '2017', 1),
(164, 164, 32, '2017', 1),
(165, 165, 32, '2017', 1),
(166, 166, 32, '2017', 1),
(167, 167, 32, '2017', 1),
(168, 168, 32, '2017', 1),
(169, 169, 32, '2017', 1),
(170, 170, 32, '2017', 1),
(171, 171, 32, '2017', 1),
(172, 172, 32, '2017', 1),
(173, 173, 32, '2017', 1),
(174, 174, 32, '2017', 1),
(175, 175, 32, '2017', 1),
(176, 176, 32, '2017', 1),
(177, 177, 32, '2017', 1),
(178, 178, 32, '2017', 1),
(179, 179, 32, '2017', 1),
(180, 180, 32, '2017', 1),
(181, 181, 32, '2017', 1),
(182, 182, 32, '2017', 1),
(183, 183, 32, '2017', 1),
(184, 184, 33, '2017', 1),
(185, 185, 33, '2017', 1),
(186, 186, 33, '2017', 1),
(187, 187, 33, '2017', 1),
(188, 188, 33, '2017', 1),
(189, 189, 24, '2017', 1),
(190, 190, 33, '2017', 1),
(191, 191, 33, '2017', 1),
(192, 192, 24, '2017', 1),
(193, 193, 33, '2017', 1),
(194, 194, 33, '2017', 1),
(195, 195, 24, '2017', 1),
(196, 196, 33, '2017', 1),
(197, 197, 33, '2017', 1),
(198, 198, 33, '1', 1),
(199, 199, 33, '2017', 1),
(200, 200, 24, '2017', 1),
(201, 201, 33, '2017', 1),
(202, 202, 33, '2017', 1),
(203, 203, 33, '2017', 1),
(204, 204, 33, '2017', 1),
(205, 205, 33, '2017', 1),
(206, 206, 33, '2017', 1),
(207, 207, 33, '2017', 1),
(208, 208, 33, '2017', 1),
(209, 209, 33, '2017', 1),
(210, 210, 24, '2017', 1),
(211, 211, 33, '2017', 1),
(212, 212, 24, '2017', 1),
(213, 213, 24, '2017', 1),
(215, 215, 24, '2017', 1),
(216, 216, 33, '2017', 1),
(217, 217, 24, '2017', 1),
(218, 218, 24, '2017', 1),
(219, 219, 24, '2017', 1),
(220, 220, 24, '2017', 1),
(221, 221, 24, '2017', 1),
(222, 222, 24, '2017', 1),
(223, 223, 24, '2017', 1),
(224, 224, 24, '2017', 1),
(225, 225, 24, '2017', 1),
(226, 226, 24, '2017', 1),
(227, 227, 24, '2017', 1),
(228, 228, 24, '2017', 1),
(229, 229, 26, '2016', 1),
(230, 230, 26, '2016', 1),
(231, 231, 26, '2016', 1),
(232, 232, 26, '2016', 1),
(233, 233, 26, '2016', 1),
(234, 234, 26, '2016', 1),
(235, 235, 26, '2016', 1),
(236, 236, 26, '2016', 1),
(237, 237, 26, '2016', 1),
(238, 238, 26, '2016', 1),
(239, 239, 26, '2016', 1),
(240, 240, 26, '2016', 1),
(241, 241, 26, '2016', 1),
(242, 242, 26, '2016', 1),
(243, 243, 26, '2016', 1),
(244, 244, 26, '2016', 1),
(245, 245, 26, '2016', 1),
(246, 246, 26, '2016', 1),
(247, 247, 26, '2016', 1),
(248, 248, 26, '2016', 1),
(249, 249, 26, '2016', 1),
(250, 250, 26, '2016', 1),
(251, 251, 26, '2016', 1),
(252, 252, 26, '2016', 1),
(253, 253, 26, '2016', 1),
(254, 254, 26, '2016', 1),
(255, 255, 26, '2016', 1),
(256, 256, 26, '2016', 1),
(257, 257, 26, '2016', 1),
(258, 258, 26, '2016', 1),
(259, 259, 26, '2016', 1),
(260, 260, 26, '2016', 1),
(261, 261, 26, '2016', 1),
(262, 262, 26, '2016', 1),
(263, 263, 26, '2016', 1),
(264, 264, 26, '2016', 1),
(265, 265, 26, '2016', 1),
(266, 266, 27, '2015', 1),
(267, 267, 27, '2015', 1),
(268, 268, 27, '2015', 1),
(269, 269, 27, '2015', 1),
(270, 270, 27, '2015', 1),
(271, 271, 27, '2015', 1),
(272, 272, 27, '2015', 1),
(273, 273, 27, '2015', 1),
(274, 274, 38, '2015', 1),
(275, 275, 38, '2015', 1),
(276, 276, 38, '2015', 1),
(277, 277, 38, '2015', 1),
(278, 278, 38, '2015', 1),
(279, 279, 38, '2015', 1),
(280, 280, 38, '2015', 1),
(281, 281, 38, '2015', 1),
(282, 282, 38, '2015', 1),
(283, 283, 38, '2015', 1),
(285, 285, 38, '2015', 1),
(286, 286, 38, '2015', 1),
(287, 287, 38, '2015', 1),
(288, 288, 38, '2015', 1),
(289, 289, 38, '2015', 1),
(290, 290, 38, '2015', 1),
(291, 291, 38, '2015', 1),
(292, 292, 38, '2015', 1),
(293, 293, 38, '2015', 1),
(294, 294, 38, '2015', 1),
(295, 295, 38, '2015', 1),
(296, 296, 38, '2015', 1),
(297, 297, 38, '2015', 1),
(298, 298, 39, '2015', 1),
(299, 299, 39, '2015', 1),
(300, 300, 39, '2015', 1),
(301, 301, 39, '2015', 1),
(302, 302, 39, '2015', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rapbs1`
--

CREATE TABLE `rapbs1` (
  `idsap` int(11) NOT NULL,
  `ketrapbs` varchar(30) NOT NULL,
  `jenisrapbs` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rapbs1`
--

INSERT INTO `rapbs1` (`idsap`, `ketrapbs`, `jenisrapbs`) VALUES
(1, 'Anggaran Pemerintah', 'Pendapatan'),
(2, 'Dana Masyarakat', 'Pendapatan'),
(3, 'Donasi', 'Pendapatan'),
(4, 'Lain - lain', 'Pendapatan'),
(5, 'Langsung pada sekolah', 'Pengeluaran'),
(6, 'Tidak Langsung pada sekolah', 'Pengeluaran'),
(7, 'Pengeluaran Lainnya', 'Pengeluaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rapbs2`
--

CREATE TABLE `rapbs2` (
  `idsap2` int(11) NOT NULL,
  `idsap` int(11) NOT NULL,
  `urut` varchar(3) NOT NULL,
  `ketsap2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rapbs2`
--

INSERT INTO `rapbs2` (`idsap2`, `idsap`, `urut`, `ketsap2`) VALUES
(1, 1, 'a.', 'APBN'),
(2, 1, 'b.', 'APBD Provinsi'),
(3, 1, 'c.', 'APBD Kabupaten/Kota'),
(4, 1, 'd.', 'Sumber anggaran pemerintah lainnya'),
(5, 2, 'a.', 'Biaya Pendidikan Siswa'),
(6, 2, 'b.', 'Penerimaan Peserta Didik Baru'),
(7, 2, 'c.', 'Sumbangan orang tua siswa / DPP'),
(8, 2, 'd.', 'Sumber Dana Lainnya'),
(9, 3, 'a.', 'Yayasan'),
(10, 3, 'b.', 'Hibah (Block Grant)'),
(11, 3, 'c.', 'Sumber Donasi lainnya'),
(12, 4, 'a.', 'Pendapatan lainnya'),
(13, 4, 'b.', 'Penjualan Hasil Produksi'),
(14, 4, 'c.', 'Sumber Pendapatan lainnya'),
(15, 5, 'a.', 'Gaji dan tunjangan tenaga kependidikan'),
(16, 5, 'b.', 'Gaji dan tunjangan guru'),
(17, 5, 'c.', 'Biaya pengembangan guru dan tenaga kependidikan'),
(18, 5, 'd.', 'Kegiatan pembelajaran'),
(19, 5, 'e.', 'Kegiatan kesiswaan'),
(20, 5, 'f.', 'Alat tulis sekolah'),
(21, 5, 'g.', 'Bahan habis pakai'),
(22, 5, 'h.', 'Alat habis pakai'),
(23, 5, 'i.', 'Kegiatan rapat'),
(24, 5, 'j.', 'Transport dan perjalanan dinas'),
(25, 5, 'k.', 'Penggandaan soal-soal ulangan/ ujian'),
(26, 5, 'l.', 'Daya dan jasa'),
(27, 5, 'm.', 'Sosial'),
(28, 5, 'n.', 'Pemeliharaan'),
(29, 5, 'o.', 'Pengadaan Inventaris'),
(30, 5, 'p.', 'Pengembangan Kurikulum'),
(31, 5, 'q.', 'Pengembangan Proses pembelajaran'),
(32, 5, 'r.', 'Pengembangan Kompetensi Lulusan'),
(33, 5, 's.', 'Pengembangan Pendidik dan Tenaga Kependidikan'),
(34, 5, 't.', 'Pengembangan Sarana dan Prasarana'),
(35, 5, 'u.', 'Pengembangan Manajemen Sekolah'),
(36, 5, 'v.', 'Pengembangan Pembiayaan'),
(37, 5, 'w.', 'Pengembangan Penilaian'),
(47, 6, 'a.', 'Investasi untuk program sekolah'),
(48, 6, 'b.', 'Lainnya'),
(50, 7, 'a.', 'Pengeluaran Lainnya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rapbs_form`
--

CREATE TABLE `rapbs_form` (
  `id_rapbs` bigint(20) NOT NULL,
  `idsaplist` int(11) NOT NULL,
  `idsap2` int(11) NOT NULL,
  `volume` double NOT NULL,
  `jumlah` double NOT NULL,
  `nominal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rapbs_list`
--

CREATE TABLE `rapbs_list` (
  `idsaplist` bigint(20) NOT NULL,
  `idsekolah` bigint(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `tglin` datetime NOT NULL,
  `th_ajaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sch_category`
--

CREATE TABLE `sch_category` (
  `id_schcat` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sch_category`
--

INSERT INTO `sch_category` (`id_schcat`, `cat_name`) VALUES
(22, 'TK'),
(21, 'SMA'),
(20, 'SMK'),
(19, 'SMP'),
(18, 'SD'),
(12, 'Yayasan Pusat'),
(23, 'Yayasan Cabang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sch_plc`
--

CREATE TABLE `sch_plc` (
  `id_schplc` bigint(20) NOT NULL,
  `sch_name` text NOT NULL,
  `sch_addrs` text NOT NULL,
  `sch_cat` int(11) NOT NULL,
  `header` text DEFAULT NULL,
  `penjwb` text NOT NULL,
  `kota` text NOT NULL,
  `kepala` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sch_plc`
--

INSERT INTO `sch_plc` (`id_schplc`, `sch_name`, `sch_addrs`, `sch_cat`, `header`, `penjwb`, `kota`, `kepala`, `status`) VALUES
(10, 'Asti Dharma Pusat', 'Jl Jend. Sudirman 20', 12, '', 'Sr. Albertine', 'Jakarta', 'Sr. Albertine', 0),
(52, 'SMP Bunda Hati Kudus Grogol', 'Grogol', 19, '', 'a', 'Grogol', 'a', 0),
(53, 'SMA Bunda Hati Kudus Grogol', 'Grogol', 21, '', 'a', 'Grogol', 'a', 0),
(54, 'TK Bunda Hati Kudus Kota Wisata', 'Kota Wisata', 22, '', 'a', 'Kota Wisata', 'a', 0),
(55, 'SD Bunda Hati Kudus  Kota Wisata', 'Kota Wisata', 18, '', 'a', 'Kota Wisata', 'a', 0),
(51, 'SD Bunda Hati Kudus Grogol', 'Grogol', 18, '', 'a', 'Grogol', 'a', 0),
(50, 'TK Bunda Hati Kudus Grogol', 'Grogol', 22, '', 'a', 'Grogol', 'a', 0),
(48, 'SD Pius Cilacap', 'Cilacap', 18, '', 'a', 'Cilacap', 'a', 0),
(49, 'SMP Pius Cilacap', 'Cilacap', 19, '', 'a', 'Cilacap', 'a', 0),
(47, 'TK Pius Cilacap', 'Cilacap', 22, '', 'a', 'Cilacap', 'a', 0),
(45, 'TK Maria Purworejo', 'Purworejo', 22, '', 'a', 'Purworejo', 'a', 0),
(46, 'SD Maria Purworejo', 'Purworejo', 18, '', 'a', 'Purworejo', 'a', 0),
(43, 'TK Pius Wonosobo', 'Wonosobo', 22, '', 'a', 'Wonosobo', 'a', 0),
(44, 'SD Pius Wonosobo', 'Wonosobo', 18, '', 'a', 'Wonosobo', 'a', 0),
(42, 'SD Santa Maria Bulu', 'Temanggung', 18, '', 'a', 'Temanggung', 'a', 0),
(40, 'SMP Pius Pemalang', 'Jl. Pemuda No 20', 19, NULL, 'MM Sumargiyati', 'Pemalang', 'Sr M Karmelia, PBHK', 0),
(41, 'TK Ade Irma Suryani Bulu', 'Temanggung', 22, '', 'a', 'Temanggung', 'a', 0),
(38, 'TK Pius Pemalang', 'Pemalang', 22, '', 'a', 'Pemalang', 'a', 0),
(39, 'SD Pius Pemalang', 'Pemalang', 18, '', 'a', 'Pemalang', 'a', 0),
(36, 'SMA Pius Tegal', 'Jl. Kapten Ismail 120', 21, NULL, 'a', 'Tegal', '', 0),
(37, 'SMK Pius Tegal', 'Tegal', 20, '', 'a', 'Tegal', 'a', 0),
(34, 'SD Pius Tegal', 'Tegal', 18, '', 'a', 'Tegal', 'a', 0),
(35, 'SMP Pius Tegal', 'Tegal', 19, '', 'a', 'Tegal', 'a', 0),
(33, 'TK Pius Tegal', 'Tegal', 22, '', 'a', 'Tegal', 'a', 0),
(31, 'Asti Dharma Pusat Cabang Bogor', 'Bogor', 23, '', 'a', 'Bogor', 'a', 0),
(32, 'Asti Dharma Cabang Tegal', 'Tegal', 23, '', 'a', 'Tegal', 'a', 0),
(56, 'SMP Bunda Hati Kudus Kota Wisata', 'Kota Wisata', 19, '', 'a', 'Kota Wisata', 'a', 0),
(57, 'SMA Bunda Hati Kudus Kota Wisata', 'Kota Wisata', 21, '', 'a', 'Kota Wisata', 'a', 0),
(58, 'Asti Dharma Cabang Cilacap', 'Jalan Jendral Sudirman 30, Cilacap Selatan', 23, '', 'kosong', 'Cilacap', 'kosong', 0),
(59, 'District-12', '12', 21, NULL, '12', '12', '12', 1),
(60, 'TEST', 'TEST', 12, NULL, 'TEST', 'TEST', 'TEST', 1),
(61, 'OK', 'OK', 22, NULL, 'OK', 'OK', 'OK', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spp_1`
--

CREATE TABLE `spp_1` (
  `id_spp` bigint(20) NOT NULL,
  `id_siswa` bigint(20) NOT NULL,
  `nominal_bln` double NOT NULL,
  `th_ajaran` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `status_lap` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spp_1`
--

INSERT INTO `spp_1` (`id_spp`, `id_siswa`, `nominal_bln`, `th_ajaran`, `status`, `status_lap`) VALUES
(1, 134, 350000, 1, 1, 1),
(2, 135, 350000, 1, 1, 1),
(3, 136, 325000, 1, 1, 1),
(4, 137, 400000, 1, 1, 1),
(5, 138, 350000, 1, 1, 1),
(6, 139, 350000, 1, 1, 1),
(7, 140, 350000, 1, 1, 1),
(8, 141, 425000, 1, 1, 1),
(9, 142, 350000, 1, 1, 1),
(10, 143, 310000, 1, 1, 1),
(11, 144, 375000, 1, 1, 1),
(12, 145, 310000, 1, 1, 1),
(13, 146, 350000, 1, 1, 1),
(14, 147, 375000, 1, 1, 1),
(15, 148, 350000, 1, 1, 1),
(16, 149, 325000, 1, 1, 1),
(17, 150, 400000, 1, 1, 1),
(18, 151, 350000, 1, 1, 1),
(19, 152, 350000, 1, 1, 1),
(20, 153, 400000, 1, 1, 1),
(21, 154, 360000, 1, 1, 1),
(22, 155, 350000, 1, 1, 1),
(23, 156, 450000, 1, 1, 1),
(24, 157, 400000, 1, 1, 1),
(25, 158, 500000, 1, 1, 1),
(26, 106, 410000, 1, 1, 1),
(27, 107, 300000, 1, 1, 1),
(28, 108, 250000, 1, 1, 1),
(29, 109, 300000, 1, 1, 1),
(30, 110, 350000, 1, 1, 1),
(31, 111, 350000, 1, 1, 1),
(32, 112, 310000, 1, 1, 1),
(33, 113, 450000, 1, 1, 1),
(34, 114, 500000, 1, 1, 1),
(35, 115, 300000, 1, 1, 1),
(36, 116, 350000, 1, 1, 1),
(37, 117, 350000, 1, 1, 1),
(38, 118, 400000, 1, 1, 1),
(39, 119, 400000, 1, 1, 1),
(40, 120, 60000, 1, 1, 1),
(41, 121, 400000, 1, 1, 1),
(42, 122, 350000, 1, 1, 1),
(43, 123, 400000, 1, 1, 1),
(44, 124, 400000, 1, 1, 1),
(45, 125, 350000, 1, 1, 1),
(46, 126, 60000, 1, 1, 1),
(47, 127, 375000, 1, 1, 1),
(48, 128, 350000, 1, 1, 1),
(49, 129, 400000, 1, 1, 1),
(50, 130, 500000, 1, 1, 1),
(51, 131, 350000, 1, 1, 1),
(52, 132, 350000, 1, 1, 1),
(53, 133, 350000, 1, 1, 1),
(54, 159, 360000, 1, 1, 1),
(55, 160, 375000, 1, 1, 1),
(56, 161, 325000, 1, 1, 1),
(57, 162, 60000, 1, 1, 1),
(58, 163, 345000, 1, 1, 1),
(59, 164, 325000, 1, 1, 1),
(60, 165, 350000, 1, 1, 1),
(61, 166, 300000, 1, 1, 1),
(62, 167, 300000, 1, 1, 1),
(63, 168, 335000, 1, 1, 1),
(64, 169, 350000, 1, 1, 1),
(65, 170, 350000, 1, 1, 1),
(66, 171, 375000, 1, 1, 1),
(67, 172, 400000, 1, 1, 1),
(68, 173, 250000, 1, 1, 1),
(69, 174, 300000, 1, 1, 1),
(70, 175, 400000, 1, 1, 1),
(71, 176, 500000, 1, 1, 1),
(72, 177, 350000, 1, 1, 1),
(73, 178, 310000, 1, 1, 1),
(74, 179, 450000, 1, 1, 1),
(75, 180, 300000, 1, 1, 1),
(76, 181, 450000, 1, 1, 1),
(77, 182, 400000, 1, 1, 1),
(78, 183, 500000, 1, 1, 1),
(79, 184, 500000, 1, 1, 1),
(80, 185, 325000, 1, 1, 1),
(81, 186, 370000, 1, 1, 1),
(82, 187, 450000, 1, 1, 1),
(83, 188, 350000, 1, 1, 1),
(84, 190, 350000, 1, 1, 1),
(85, 191, 350000, 1, 1, 1),
(86, 193, 400000, 1, 1, 1),
(87, 194, 350000, 1, 1, 1),
(88, 196, 350000, 1, 1, 1),
(89, 197, 350000, 1, 1, 1),
(90, 198, 310000, 1, 1, 1),
(91, 199, 350000, 1, 1, 1),
(92, 201, 360000, 1, 1, 1),
(93, 202, 450000, 1, 1, 1),
(94, 203, 4500000, 1, 1, 1),
(95, 204, 300000, 1, 1, 1),
(96, 205, 400000, 1, 1, 1),
(97, 206, 325000, 1, 1, 1),
(98, 207, 300000, 1, 1, 1),
(99, 208, 450000, 1, 1, 1),
(100, 209, 350000, 1, 1, 1),
(101, 211, 350000, 1, 1, 1),
(102, 216, 350000, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spp_2`
--

CREATE TABLE `spp_2` (
  `id_spp2` bigint(20) NOT NULL,
  `id_spp` bigint(20) NOT NULL,
  `nom_spp` double NOT NULL,
  `bulan` int(20) NOT NULL,
  `tgl_pembayaran` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id_tha` bigint(20) NOT NULL,
  `tahunajaran` varchar(12) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_tha`, `tahunajaran`, `status`) VALUES
(1, '2017 - 2018', 0),
(2, '2018 - 2019', 0),
(3, '2019 - 2020', 1),
(4, '2020 - 2021', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `terima_bantu`
--

CREATE TABLE `terima_bantu` (
  `id_bantu` bigint(20) NOT NULL,
  `id_jenis` varchar(3) NOT NULL,
  `sumber_bantu` text NOT NULL,
  `ket_bantu` text DEFAULT NULL,
  `nom_bntu` double NOT NULL,
  `wkt_bntu` datetime NOT NULL,
  `id_schp` bigint(20) NOT NULL,
  `id_thajar` int(2) NOT NULL,
  `idthnang` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `thnanggar`
--

CREATE TABLE `thnanggar` (
  `idthnang` int(11) NOT NULL,
  `tahun` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `thnanggar`
--

INSERT INTO `thnanggar` (`idthnang`, `tahun`) VALUES
(1, '2017'),
(2, '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_akun`
--

CREATE TABLE `tipe_akun` (
  `id_akun` int(11) NOT NULL,
  `ket_akun` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipe_akun`
--

INSERT INTO `tipe_akun` (`id_akun`, `ket_akun`) VALUES
(1, 'Penerimaan'),
(2, 'Pengeluaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_buku`
--

CREATE TABLE `tipe_buku` (
  `id_buku` int(11) NOT NULL,
  `ket_buku` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipe_buku`
--

INSERT INTO `tipe_buku` (`id_buku`, `ket_buku`) VALUES
(1, 'Operasional'),
(2, 'Kegiatan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ad_list`
--
ALTER TABLE `ad_list`
  ADD PRIMARY KEY (`id_adl`),
  ADD KEY `adl_leor` (`adl_leor`),
  ADD KEY `adl_sch` (`adl_sch`);

--
-- Indeks untuk tabel `ad_role`
--
ALTER TABLE `ad_role`
  ADD PRIMARY KEY (`id_adr`);

--
-- Indeks untuk tabel `bagainformlr`
--
ALTER TABLE `bagainformlr`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indeks untuk tabel `buku_akun`
--
ALTER TABLE `buku_akun`
  ADD PRIMARY KEY (`id_ops`),
  ADD KEY `tipe_akun` (`tipe_akun`),
  ADD KEY `tipe_buku` (`tipe_buku`),
  ADD KEY `id_schp` (`id_schp`);

--
-- Indeks untuk tabel `bulan`
--
ALTER TABLE `bulan`
  ADD PRIMARY KEY (`id_bulan`);

--
-- Indeks untuk tabel `detail_bantuan`
--
ALTER TABLE `detail_bantuan`
  ADD PRIMARY KEY (`id_dbn`);

--
-- Indeks untuk tabel `dpp_1`
--
ALTER TABLE `dpp_1`
  ADD PRIMARY KEY (`id_dpp`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indeks untuk tabel `dpp_2`
--
ALTER TABLE `dpp_2`
  ADD PRIMARY KEY (`id_dpp2`),
  ADD KEY `id_dpp` (`id_dpp`);

--
-- Indeks untuk tabel `form_lr`
--
ALTER TABLE `form_lr`
  ADD PRIMARY KEY (`id_formlr`);

--
-- Indeks untuk tabel `kdspek`
--
ALTER TABLE `kdspek`
  ADD PRIMARY KEY (`idspek`);

--
-- Indeks untuk tabel `kd_lr`
--
ALTER TABLE `kd_lr`
  ADD PRIMARY KEY (`id_lr`);

--
-- Indeks untuk tabel `kd_pemasukan`
--
ALTER TABLE `kd_pemasukan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indeks untuk tabel `kd_pengeluaran`
--
ALTER TABLE `kd_pengeluaran`
  ADD PRIMARY KEY (`id_p`);

--
-- Indeks untuk tabel `kegiatan_1`
--
ALTER TABLE `kegiatan_1`
  ADD PRIMARY KEY (`id_kgt`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indeks untuk tabel `kegiatan_2`
--
ALTER TABLE `kegiatan_2`
  ADD PRIMARY KEY (`id_kgt2`),
  ADD KEY `id_kgt` (`id_kgt`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_sch` (`id_sch`);

--
-- Indeks untuk tabel `ket_angsur`
--
ALTER TABLE `ket_angsur`
  ADD PRIMARY KEY (`id_angsur`);

--
-- Indeks untuk tabel `kode_pb`
--
ALTER TABLE `kode_pb`
  ADD PRIMARY KEY (`id_pb`);

--
-- Indeks untuk tabel `last_log`
--
ALTER TABLE `last_log`
  ADD PRIMARY KEY (`id_ll`);

--
-- Indeks untuk tabel `pelajar_list`
--
ALTER TABLE `pelajar_list`
  ADD PRIMARY KEY (`id_pl`),
  ADD KEY `id_schp` (`id_schp`);

--
-- Indeks untuk tabel `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  ADD PRIMARY KEY (`idbayar`);

--
-- Indeks untuk tabel `pl_kelas`
--
ALTER TABLE `pl_kelas`
  ADD PRIMARY KEY (`id_plk`),
  ADD KEY `id_pelajar` (`id_pelajar`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indeks untuk tabel `rapbs1`
--
ALTER TABLE `rapbs1`
  ADD PRIMARY KEY (`idsap`);

--
-- Indeks untuk tabel `rapbs2`
--
ALTER TABLE `rapbs2`
  ADD PRIMARY KEY (`idsap2`);

--
-- Indeks untuk tabel `rapbs_form`
--
ALTER TABLE `rapbs_form`
  ADD PRIMARY KEY (`id_rapbs`);

--
-- Indeks untuk tabel `rapbs_list`
--
ALTER TABLE `rapbs_list`
  ADD PRIMARY KEY (`idsaplist`);

--
-- Indeks untuk tabel `sch_category`
--
ALTER TABLE `sch_category`
  ADD PRIMARY KEY (`id_schcat`);

--
-- Indeks untuk tabel `sch_plc`
--
ALTER TABLE `sch_plc`
  ADD PRIMARY KEY (`id_schplc`),
  ADD KEY `sch_cat` (`sch_cat`);

--
-- Indeks untuk tabel `spp_1`
--
ALTER TABLE `spp_1`
  ADD PRIMARY KEY (`id_spp`),
  ADD KEY `id_siswa` (`id_siswa`);

--
-- Indeks untuk tabel `spp_2`
--
ALTER TABLE `spp_2`
  ADD PRIMARY KEY (`id_spp2`),
  ADD KEY `id_spp` (`id_spp`),
  ADD KEY `bulan` (`bulan`);

--
-- Indeks untuk tabel `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id_tha`);

--
-- Indeks untuk tabel `terima_bantu`
--
ALTER TABLE `terima_bantu`
  ADD PRIMARY KEY (`id_bantu`),
  ADD KEY `id_schp` (`id_schp`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indeks untuk tabel `thnanggar`
--
ALTER TABLE `thnanggar`
  ADD PRIMARY KEY (`idthnang`);

--
-- Indeks untuk tabel `tipe_akun`
--
ALTER TABLE `tipe_akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indeks untuk tabel `tipe_buku`
--
ALTER TABLE `tipe_buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ad_list`
--
ALTER TABLE `ad_list`
  MODIFY `id_adl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT untuk tabel `ad_role`
--
ALTER TABLE `ad_role`
  MODIFY `id_adr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `bagainformlr`
--
ALTER TABLE `bagainformlr`
  MODIFY `id_bagian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `buku_akun`
--
ALTER TABLE `buku_akun`
  MODIFY `id_ops` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT untuk tabel `bulan`
--
ALTER TABLE `bulan`
  MODIFY `id_bulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `detail_bantuan`
--
ALTER TABLE `detail_bantuan`
  MODIFY `id_dbn` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `dpp_1`
--
ALTER TABLE `dpp_1`
  MODIFY `id_dpp` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `dpp_2`
--
ALTER TABLE `dpp_2`
  MODIFY `id_dpp2` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT untuk tabel `form_lr`
--
ALTER TABLE `form_lr`
  MODIFY `id_formlr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `kd_lr`
--
ALTER TABLE `kd_lr`
  MODIFY `id_lr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `kd_pemasukan`
--
ALTER TABLE `kd_pemasukan`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `kd_pengeluaran`
--
ALTER TABLE `kd_pengeluaran`
  MODIFY `id_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `kegiatan_1`
--
ALTER TABLE `kegiatan_1`
  MODIFY `id_kgt` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `kegiatan_2`
--
ALTER TABLE `kegiatan_2`
  MODIFY `id_kgt2` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT untuk tabel `ket_angsur`
--
ALTER TABLE `ket_angsur`
  MODIFY `id_angsur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kode_pb`
--
ALTER TABLE `kode_pb`
  MODIFY `id_pb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `last_log`
--
ALTER TABLE `last_log`
  MODIFY `id_ll` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT untuk tabel `pelajar_list`
--
ALTER TABLE `pelajar_list`
  MODIFY `id_pl` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=306;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_siswa`
--
ALTER TABLE `pembayaran_siswa`
  MODIFY `idbayar` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=469;

--
-- AUTO_INCREMENT untuk tabel `pl_kelas`
--
ALTER TABLE `pl_kelas`
  MODIFY `id_plk` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT untuk tabel `rapbs1`
--
ALTER TABLE `rapbs1`
  MODIFY `idsap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `rapbs2`
--
ALTER TABLE `rapbs2`
  MODIFY `idsap2` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `rapbs_form`
--
ALTER TABLE `rapbs_form`
  MODIFY `id_rapbs` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rapbs_list`
--
ALTER TABLE `rapbs_list`
  MODIFY `idsaplist` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sch_category`
--
ALTER TABLE `sch_category`
  MODIFY `id_schcat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `sch_plc`
--
ALTER TABLE `sch_plc`
  MODIFY `id_schplc` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `spp_1`
--
ALTER TABLE `spp_1`
  MODIFY `id_spp` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `spp_2`
--
ALTER TABLE `spp_2`
  MODIFY `id_spp2` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT untuk tabel `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  MODIFY `id_tha` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `terima_bantu`
--
ALTER TABLE `terima_bantu`
  MODIFY `id_bantu` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `thnanggar`
--
ALTER TABLE `thnanggar`
  MODIFY `idthnang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tipe_akun`
--
ALTER TABLE `tipe_akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tipe_buku`
--
ALTER TABLE `tipe_buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
