<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class LoginModel extends CI_Model {
    
        public function Login($user,$pwd)
        {
            $this->db->select('id_adl,
                adl_name,
                adl_sch,
                sch_plc.`sch_name`,
                ad_role.`adr_name`,
                ad_list.`pp`,
                ad_list.`parent`,
                ad_list.adl_leor,
                ad_list.adl_name');
            $this->db->from('ad_list');
            $this->db->join('ad_role','ad_list.adl_leor = ad_role.id_adr', 'left');
            $this->db->join('sch_plc','sch_plc.id_schplc = ad_list.adl_sch' , 'left');
            $this->db->where('ad_list.`suser`', 1);
            $this->db->where('ad_list.adl_name', $user);
            $this->db->where('ad_list.adl_dwp', $pwd);
            return $this->db->get()->row();
        }
    }
    
    /* End of file LoginModel.php */
    