<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Sekolahmod extends CI_Model {
        function getall(){
            $this->db->where('status',0);
            $this->db->order_by('id_schplc', 'desc');
            return $this->db->get('sch_plc')->result();
        }

        function getall_cat(){
            return $this->db->get('sch_category')->result();
        }

        function add_sch($array=NULL){
            if($this->db->insert('sch_plc', $array)){
                return true;
            }
            else{
                return false;
            }
        }
        
        function update_sch($id,$array=NULL){
            $this->db->where('id_schplc', $id);
            if($this->db->update('sch_plc', $array)){
                return true;
            }
            else{
                return false;
            }
        }

        function del_sch($array){
            if(empty($array)){
                return array('status'=>false,'note'=>'Data Kosong');
            }else{
                $save=array();
                foreach ($array as $key) {
                    // if($key['id']==58){
                    //     $save['gagal'][]=array('data'=>$key['sch']);
                    // }
                    // else{
                        $this->db->where('id_schplc',$key['id']);
                        if($this->db->update('sch_plc',array('status'=>1))){
                            $save['berhasil'][]=array('data'=>$key['sch']);
                        }
                        else{
                            $save['gagal'][]=array('data'=>$key['sch']);
                        }
                        
                    // }
                    // else{
                    //     $save[]=array('status'=>false,'data'=>$key['sch']);
                    // }
                }
                return array('status'=>true,'cek'=>$save);
                // return array('status'=>false,'note'=>'Data Kosong');
            }
        }
    }
    