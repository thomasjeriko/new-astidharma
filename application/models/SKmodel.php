<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class SKmodel extends CI_Model {
        
        function add($table,$array){
            if($this->db->insert($table, $array)){
                return true;
            }else{
                return false;
            }
        }

        function update($table,$id,$array){
            $field=$this->db->field_data($table);
            $pk=0;
            foreach ($field as $key) {
                if($key->primary_key==1){
                    $pk=$key->name;
                }
            }
            $this->db->where($pk, $id);
            if($this->db->update($table, $array)){
                return true;
            }
            else{
                return false;
            }
        }

        function delete($table,$array){
            $field=$this->db->field_data($table);
            $pk=0;
            foreach ($field as $key) {
                if($key->primary_key==1){
                    $pk=$key->name;
                }
            }
            $save=array();
            foreach ($array as $key) {
                $this->db->where($pk, $key['id']);
                if($this->db->delete($table)){
                    $save['berhasil'][]=array('data'=>$key['sch']);
                }
                else{
                    $save['gagal'][]=array('data'=>$key['sch']);
                }
            }
            return array('status'=>true,'cek'=>$save);
        }

        function getall($table,$array){
            $field=$this->db->field_data($table);
            $pk=0;
            foreach ($field as $key) {
                if($key->primary_key==1){
                    $pk=$key->name;
                }
            }
            $this->db->order_by($pk, 'desc');
            return $this->db->get_where($table,$array)->result();
        }
    }
    
    /* End of file SKmodel.php */
    