<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class KSmodel extends CI_Model {

    #SPP
    public function CekSpp($idsp,$bulan)
    {
        $cek=$this->db->get_where('spp_2',array('id_spp'=>$idsp,'bulan'=>$bulan))->num_rows();
        if($cek>0){
            return false;
        }
        else{
            return true;
        }
    }

    public function AddSpp($object)
    {
        if($this->CekSpp($object['id_spp'],$object['bulan'])){
            if($this->db->insert('spp_2', $object)){
                return true;
            }else{
                return false;
            }
        }
        else{
            return "11";
        }
    }

    #DPP
    public function CekDpp($idpp,$keta)
    {
        $cek=$this->db->get_where('dpp_2',array('id_dpp'=>$idpp,'ket_ang'=>$keta))->num_rows();
        if($cek>0){
            return false;
        }
        else{
            return true;
        }
    }

    public function AddDpp($object)
    {
        if($this->CekDpp($object['id_dpp'],$object['ket_ang'])){
            if($this->db->insert('dpp_2', $object)){
                return true;
            }else{
                return false;
            }
        }
        else{
            return "11";
        }
	}
	
	#Riwayat
	function history($data=NULL){
		// $offset=$data['page']*$data['limit'];
		
		// return $this->db->get('riwayat', $data['limit'], $offset)->result();
		return $this->db->get('riwayat')->result();
	}

	#Sekolah
	function sch(){
		// $offset=$data['page']*$data['limit'];
		
		// return $this->db->get('riwayat', $data['limit'], $offset)->result();
		$this->db->select('id_schplc,sch_name');
		$this->db->where('status',0);
		return $this->db->get('sch_plc')->result();
	}

	#Siswa
	function siswa($data=NULL){
		$this->db->select('pelajar_list.id_pl,pelajar_list.nama_pl');
		$this->db->where('pelajar_list.id_schp', $data);
		return $this->db->get('pelajar_list')->result();
	}

	#Tahun Ajaran
	function tahun(){
		// $offset=$data['page']*$data['limit'];
		
		// return $this->db->get('riwayat', $data['limit'], $offset)->result();
		$this->db->order_by('id_tha', 'desc');
		return $this->db->get('tahun_ajaran')->result();
	}

}

/* End of file KSModel.php */
