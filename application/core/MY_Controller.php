<?php
class MY_Controller extends CI_Controller
{
	public $rlName;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if ($this->session->idadl) {
			$rlId = $this->session->userdata('rlid');
			$this->rlName = ($rlId ==1) ? "admin" : (($rlId == 2) ? "cabang" : "sekolah");
		} else {
			redirect('', 'refresh');
		}
	}

	function render_page($content, $dataIn = NULL){
		// print_r($this->rlName);
		$data=array(
			'_role'=>$this->rlName,
			'_sidebar'=>'sidebar/'.$this->rlName,
			'_view'=>$content,
		);
		$data=$data+$dataIn;
        $this->load->view('layouts/main', $data);
    }
}
