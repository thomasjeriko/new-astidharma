<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmenu extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
    }

    #Sekolah
    public function sekolah()
    {
        $flash=array(
            'act1'=>'skl',
            'act2'=>'skl'
        );
        $this->session->set_flashdata($flash);
        $data=array(
            'kategori'=>$this->Sekolahmod->getall_cat(),
		);
		$this->render_page('menu/sekolah',$data);
		
        // $this->load->view('layouts/main',$data);
    }
    
    #Kelas
    public function kelas()
    {
        $flash=array(
            'act1'=>'skl',
            'act2'=>'kls'
        );
        $this->session->set_flashdata($flash);
        $data=array(
            'kategori'=>$this->SKmodel->getall('sch_plc',array("sch_cat NOT IN(12,23)"=>NULL,'status'=>0)),
		);
		$this->render_page('menu/kelas',$data);
        // $this->load->view('layouts/main',$data);
    }

    #Tahun Ajaran
    public function tahun()
    {
        $flash=array(
            'act1'=>'th',
            'act2'=>'th',
        );
		$this->session->set_flashdata($flash);
		$data=array();
		$this->render_page('menu/tahun',$data);
        // $this->load->view('layouts/main',$data);
    }

    #Siswa
    public function siswa()
    {
        $flash=array(
            'act1'=>'siswa',
            'act2'=>'siswa',
        );
        $this->session->set_flashdata($flash);
        $data=array(
            'kategori'=>$this->SKmodel->getall('sch_plc',array("sch_cat NOT IN(12,23)"=>NULL,'status'=>0)),
		);
		$this->render_page('menu/siswa',$data);
        // $this->load->view('layouts/main',$data);
    }

    #Naik Kelas
    public function upsiswa()
    {
        $flash=array(
            'act1'=>'siswa',
            'act2'=>'upsiswa',
        );
        $this->session->set_flashdata($flash);
        $data=array();
		$this->render_page('menu/upsiswa',$data);
        // $this->load->view('layouts/main',$data);
    }
}

/* End of file Sekolah.php */

