<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function index()
	{
		if (!empty($_POST) && count($_POST) > 0) {
			$this->_login($_POST['usname'], $_POST['pname']);
		} else {
			if ($this->session->idadl) {
				$this->session->sess_destroy();
				redirect('', 'refresh');
			} else {
				$this->load->view('login');
			}
		}
	}
	
	function _login($usname, $pname){
		$data = $this->LoginModel->Login($usname, $pname);
			if (count($data) > 0) {
				$datauser=array(
					'rlid'=>$data->adl_leor,
					'rl'=>$data->adr_name,
					'schn'=>$data->sch_name,
					'sch'=>$data->adl_sch,
					'name'=>$data->adl_name,
					'idadl'=>$data->id_adl
				);
				$this->session->set_userdata($datauser);
				if($data->adl_leor==1){
					$link="admin/home";
				}
				elseif($data->adl_leor==2){
					$link="cabang/home";
				}
				elseif($data->adl_leor==3){
					$link="sekolah/home";
				}
				$this->session->set_userdata('link', $link);
				$this->session->set_userdata('par', $data->parent);
				if ($data->pp == null) {
					$this->session->set_userdata('pp', 'resources/img/' . "user.png"); //Foto Profil Default kalau Null
				} else {
					$this->session->set_userdata('pp', 'resources/img/' . $data->pp);
				}
				redirect('admin/home', 'location');
			} else {
				$this->session->set_flashdata('cek', 'Username atau Password Salah');
				$this->load->view('login');
			}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('', 'refresh');
	}
}
