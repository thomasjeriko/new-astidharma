<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class KScon extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
    }
    

    public function index($menu)
    {
		$data=array();
		$flash=array(
			'act1'=>'ks'
		);
		switch ($menu) {
			case 'his':
				$flash=$flash+array('act2'=>'his');
				$view='menu/ks/riwayat';
				break;
			case 'list':
				$flash=$flash+array('act2'=>'list');
				$view='menu/ks/daftar';
				break;
			case 'set':
				$flash=$flash+array('act2'=>'set');
				$view='menu/ks/pengaturan';
				break;
			default:
				# code...
				break;
		}
		$this->session->set_flashdata($flash);
		$this->render_page($view,$data);
	}
	
	function jdata($info){
		switch ($info) {
			case 'his':
				// echo "TEST";
				$data=$this->KSmodel->history();
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				break;
			case 'sch':
				// echo "TEST";
				$data=$this->KSmodel->sch();
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				break;
			case 'siswa':
				// echo "TEST";
				$idSekolah=$this->input->post('id');
				$data=$this->KSmodel->siswa($idSekolah);
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				break;
			case 'tahun':
				// echo "TEST";
				$data=$this->KSmodel->tahun();
				$this->output->set_content_type('application/json')->set_output(json_encode($data));
				break;
			default:
				echo "ERROR";
				break;
		}
	}

    #SPP
    // public function AddSpp(){
    //     if(!empty($_POST) && count($_POST)>0){
    //         $date=date("Y-m-d h:i:s");
    //         $data=array(
    //             'id_spp'=>$this->input->post('idss'),
    //             'nom_spp'=>$this->input->post('nomsp'),
    //             'bulan'=>$this->input->post('bulan'),
    //             'tgl_pembayaran'=>$date
    //         );
    //         $sppret=$this->KSModel->AddSpp($data);
    //         if($sppret){
    //             if($sppret==11){
    //                 // sudah di input
    //             }else{
    //                 // Berhasil input
    //             }
    //         }
    //         else{
    //             //Gagal input
    //         }
    //     }
    //     else{

    //     }
    // }

    #DPP
    // public function AddDpp(){
    //     if(!empty($_POST) && count($_POST)>0){
    //         $date=date("Y-m-d h:i:s");
    //         $data=array(
    //             'id_dpp'=>$this->input->post('idss'),
    //             'angs_dpp'=>$this->input->post('dppay'),
    //             'ket_ang'=>$this->input->post('angsurr'),
    //             'tgl_pembayaran'=>$date
    //         );
    //         $dppret=$this->KSModel->AddDpp($data);
    //         if($dppret){
    //             if($dppret==11){
    //                 // sudah di input
    //             }else{
    //                 // Berhasil input
    //             }
    //         }
    //         else{
    //             //Gagal input
    //         }
    //     }
    //     else{

    //     }
    // }

    #Uang Kegiatan
}

/* End of file Spp.php */
