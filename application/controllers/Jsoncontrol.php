<?php
    
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Jsoncontrol extends MY_Controller {
    
        public function __construct()
        {
            parent::__construct();
            //Load Dependencies
    
        }
    
        // List all your items
        public function index($get=NULL){
            if(!$this->input->is_ajax_request()){
                redirect('404');
            }else{
                switch ($get) {
                    case 'sch':
                        $test=$this->Sekolahmod->getall();
                        $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($test));
                        break;
                    case 'kat':
                        $test=$this->Sekolahmod->getall_cat();
                        $test2=array();
                        foreach ($test as $okay){
                            $test2+=array(
                                $okay->id_schcat=>$okay->cat_name,
                            );
                        }
                        $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($test2));
                        break;
                    case 'scl':
                            $test=$this->SKmodel->getall('sch_plc',array("sch_cat NOT IN(12,23)"=>NULL,'status'=>0));
                            $test2=array();
                            foreach ($test as $okay){
                                $test2+=array(
                                    $okay->id_schplc=>$okay->sch_name,
                                );
                            }
                            $this->output
                                ->set_content_type('application/json')
                                ->set_output(json_encode($test2));
                        break;
                    case 'kls':
                            $test=$this->SKmodel->getall('kelas',array());
                            $this->output
                                ->set_content_type('application/json')
                                ->set_output(json_encode($test));
                        break;
                    case 'th':
                        $test=$this->SKmodel->getall('tahun_ajaran',array());
                        $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($test));
                        break;
                    case 'siswa':
                        $test=$this->SKmodel->getall('pelajar_list',array());
                        $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($test));
                        break;
                    default:
                        header('HTTP/1.1 500 Internal Server Booboo');
                        header('Content-Type: application/json; charset=UTF-8');
                        die(json_encode(array('message' => 'Data Not Found')));
                        break;
                }
            }
        }

        public function add($get=NULL){
            if(!$this->input->is_ajax_request()){
                redirect('404');
            }else{
                switch ($get) {
                    case 'sch':
                        $array=$this->input->post();
                        if($this->Sekolahmod->add_sch($array)){
                            $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($array));
                        }else{
                            header('HTTP/1.1 500 Internal Server Booboo');
                            header('Content-Type: application/json; charset=UTF-8');
                            die(json_encode(array('message' => 'Data gagal di tambahkan 😞 ')));
                        }
                        // $this->output
                        //     ->set_content_type('application/json')
                        //     ->set_output(json_encode($test));
                        break;
                    case 'kls':
                        $array=$this->input->post();
                        $this->_repeat_add('kelas',$array);
                        break;
                    case 'th':
                        $array=$this->input->post();
                        $this->_repeat_add('tahun_ajaran',$array);
                        break;
                    case 'siswa':
                        $array=$this->input->post();
                        $this->_repeat_add('pelajar_list',$array);
                        break;
                    default:
                        header('HTTP/1.1 500 Internal Server Booboo');
                        header('Content-Type: application/json; charset=UTF-8');
                        die(json_encode(array('message' => 'Data Not Found')));
                        break;
                }
            }
        }

        public function update($update=NULL){
            if(!$this->input->is_ajax_request()){
                redirect('404');
            }else{
                switch ($update) {
                    case 'sch': 
                        $array=$this->input->post('id');
                        $id=$array['id_schplc'];
                        unset($array['id_schplc']);
                        // print_r(json_encode($array));
                        if($this->Sekolahmod->update_sch($id,$array)){
                            $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode(array('message' =>'Pembaharuan berhasil 😊')));
                        }else{
                            header('HTTP/1.1 500 Internal Server Booboo');
                            header('Content-Type: application/json; charset=UTF-8');
                            die(json_encode(array('message' =>'Terjadi Kesalahan 😕')));
                        }
                        break;
                    case 'kls': 
                        $array=$this->input->post('id');
                        $id=$array['id_kelas'];
                        unset($array['id_kelas']);
                        // print_r(json_encode($array));
                        $this->_repeat_update('kelas',$id,$array);
                        break;
                    case 'th': 
                        $array=$this->input->post('id');
                        $id=$array['id_tha'];
                        unset($array['id_tha']);
                        // print_r(json_encode($array));
                        $this->_repeat_update('tahun_ajaran',$id,$array);
                        break;
                    case 'siswa': 
                        $array=$this->input->post('id');
                        $id=$array['id_pl'];
                        unset($array['id_pl']);
                        // print_r(json_encode($array));
                        $this->_repeat_update('pelajar_list',$id,$array);
                        break;
                    default:
                        header('HTTP/1.1 500 Internal Server Booboo');
                        header('Content-Type: application/json; charset=UTF-8');
                        die(json_encode(array('message' => 'Data Not Found')));
                        break;
                }
            }
        }

        public function delete($del=NULL){
            if(!$this->input->is_ajax_request()){
                redirect('404');
            }else{
                switch ($del) {
                    case 'sch': 
                        $array=$this->input->post('id');
                        // print_r($this->Sekolahmod->del_sch($array));
                        $cek=$this->Sekolahmod->del_sch($array);
                        if($cek['status']){
                            $this->output
                            ->set_content_type('application/json')
                            ->set_output(json_encode($cek));
                        }elseif($cek['status']==false){
                            header('HTTP/1.1 500 Internal Server Booboo');
                            header('Content-Type: application/json; charset=UTF-8');
                            die(json_encode(array('message' => $cek['note'].' 😞')));
                        }else{
                            header('HTTP/1.1 500 Internal Server Booboo');
                            header('Content-Type: application/json; charset=UTF-8');
                            die(json_encode(array('message' =>'Terjadi Kesalahan 😕')));
                        }
                        break;
                    case 'kls': 
                        $array=$this->input->post('id');
                        $this-> _repeat_delete('kelas',$array);
                        break;
                    case 'th':
                        $array=$this->input->post('id');
                        $this-> _repeat_delete('tahun_ajaran',$array);
                        break;
                    case 'siswa':
                        $array=$this->input->post('id');
                        $this-> _repeat_delete('pelajar_list',$array);
                        break;
                    default:
                        header('HTTP/1.1 500 Internal Server Booboo');
                        header('Content-Type: application/json; charset=UTF-8');
                        die(json_encode(array('message' => 'Data Not Found')));
                        break;
                }
            }
        }

        function _repeat_update($tabel,$id,$array){
            if($this->SKmodel->update($tabel,$id,$array)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('message' =>'Pembaharuan berhasil 😊')));
            }else{
                header('HTTP/1.1 500 Internal Server Booboo');
                header('Content-Type: application/json; charset=UTF-8');
                die(json_encode(array('message' =>'Terjadi Kesalahan 😕')));
            }
        }

        function _repeat_add($tabel,$array){
            if($this->SKmodel->add($tabel,$array)){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($array));
            }else{
                header('HTTP/1.1 500 Internal Server Booboo');
                header('Content-Type: application/json; charset=UTF-8');
                die(json_encode(array('message' => 'Data gagal di tambahkan 😞 ')));
            }
        }

        function _repeat_delete($tabel,$array){
            $cek=$this->SKmodel->delete($tabel,$array);
            if($cek['status']){
                $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($cek));
            }elseif($cek['status']==false){
                header('HTTP/1.1 500 Internal Server Booboo');
                header('Content-Type: application/json; charset=UTF-8');
                die(json_encode(array('message' => $cek['note'].' 😞')));
            }else{
                header('HTTP/1.1 500 Internal Server Booboo');
                header('Content-Type: application/json; charset=UTF-8');
                die(json_encode(array('message' =>'Terjadi Kesalahan 😕')));
            }
        }
    }
    
    /* End of file Jsoncontrol.php */
    
    