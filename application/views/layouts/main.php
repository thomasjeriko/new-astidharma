<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Asti Dharma - <?= $this->session->userdata('schn'); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url('resources/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('resources/css/font-awesome.min.css'); ?>">
    <!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url('vendor/css/ionicons.min.css'); ?>">
    <!-- Datetimepicker -->
    <link rel="stylesheet" href="<?php echo base_url('resources/css/bootstrap-datetimepicker.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('resources/css/AdminLTE.min.css'); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('resources/css/_all-skins.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('vendor/css/selectize.default.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('vendor/css/selectize.bootstrap3.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('vendor/css/toastr.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('vendor/css/jquery-confirm.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('vendor/css/tabulator.min.css'); ?>">
    <script src="<?php echo base_url('resources/js/moment.js'); ?>"></script>
</head>

<body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">AD</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">Asti Dharma</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= base_url($this->session->userdata('pp')); ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $this->session->userdata('schn'); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= base_url($this->session->userdata('pp')); ?>" class="img-circle" alt="User Image">
                                    <p>
                                        <?=$this->session->userdata('name')?> - <?=$this->session->userdata('rl');?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?=base_url($this->session->userdata('link'))."#profil"?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="javascript:void(0)" class="btn btn-default btn-flat" id="logout">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
			<?php 
			if (isset($_sidebar) && $_sidebar){
				$this->load->view($_sidebar);
			}
			?>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <?php 
                if (isset($_view) && $_view)
                    $this->load->view($_view);
                ?>
                <div id="con"></div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Developed By <a href="https://stikomyos.ac.id" target="_blank">STIKOM Yos Sudarso Purwokerto</a></strong>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane" id="control-sidebar-home-tab">

                </div>
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url('resources/js/jquery-2.2.3.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url('resources/js/bootstrap.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('resources/js/fastclick.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('resources/js/app.min.js'); ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('resources/js/demo.js'); ?>"></script>
    <!-- DatePicker -->
	<script src="<?php echo base_url('vendor/js/standalone/selectize.min.js'); ?>"></script>
	<script src="<?php echo base_url('vendor/js/toastr.min.js'); ?>"></script>
	<script src="<?php echo base_url('vendor/js/jquery-confirm.min.js'); ?>"></script>
	<script src="<?php echo base_url('vendor/js/tabulator.min.js'); ?>"></script>
    <script src="<?php echo base_url('resources/js/bootstrap-datetimepicker.min.js'); ?>"></script>
    <script src="<?php echo base_url('resources/js/global.js');?>"></script>
    <script src="<?php echo base_url('resources/js/app.use.js');?>"></script>
	<script src="<?=base_url('resources/js/app.'.$_role.'.js')?>"></script>
</body>

</html> 
