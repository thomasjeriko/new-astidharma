<section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= base_url($this->session->userdata('pp')); ?>" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?= $this->session->userdata('name') ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="<?= base_url($this->session->userdata('link')) ?>">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li <?=($this->session->flashdata('act1')=='skl') ? 'class="active"':'';?>>
                        <a href="#">
                            <i class="fa fa-university" aria-hidden="true"></i> <span>Daftar Sekolah dan Kelas</span>
                            <ul class="treeview-menu">
                                <li <?=($this->session->flashdata('act2')=='skl') ? 'class="active"':'';?> data-menue="sekolah">
                                    <a href="<?=base_url('admin/skl')?>"><i class="fa fa-university" aria-hidden="true"></i>Sekolah/Lembaga</a>
                                </li>
                                <li <?=($this->session->flashdata('act2')=='kls') ? 'class="active"':'';?> data-menue="kelas">
                                    <a href="<?=base_url('admin/kl')?>"><i class="fa fa-list-ol" aria-hidden="true"></i> Kelas</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-user" aria-hidden="true"></i> <span> Data User</span>
                        </a>
                    </li>
                    <li <?=($this->session->flashdata('act1')=='th') ? 'class="active"':'';?>>
                         <a href="#">
                            <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> <span>Daftar Tahun Ajaran</span>
                            <ul class="treeview-menu">
                                <li <?=($this->session->flashdata('act2')=='th') ? 'class="active"':'';?> data-menue="tahun">
                                    <a href="<?=base_url('admin/tahun')?>">
                                        <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> <span>Data Tahun Ajaran</span>
                                    </a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li <?=($this->session->flashdata('act1')=='siswa') ? 'class="active"':'';?>>
                        <a href="#">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i> <span>Data Siswa Sekolah</span>
                            <ul class="treeview-menu">
                                <li <?=($this->session->flashdata('act2')=='siswa') ? 'class="active"':'';?> data-menue="siswa">
                                    <a href="<?=base_url('admin/siswa')?>"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Data Siswa</a>
                                </li>
                                <li <?=($this->session->flashdata('act2')=='upsiswa') ? 'class="active"':'';?> data-menue="upsiswa">
                                    <a href="<?=base_url('admin/upsiswa')?>"><i class="fa fa-cog" aria-hidden="true"></i>Naik Kelas/Mutasi Siswa</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li <?=($this->session->flashdata('act1')=='ks') ? 'class="active"':'';?>>
                        <a href="#">
                            <i class="fa fa-money" aria-hidden="true"></i> <span>Keuangan Siswa</span>
                            <ul class="treeview-menu">
                                <li <?=($this->session->flashdata('act2')=='list') ? 'class="active"':'';?>>
                                    <a href="<?=base_url('admin/keusiswa/list')?>"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Pembayaran Keuangan Siswa</a>
                                </li>
                                <li <?=($this->session->flashdata('act2')=='set') ? 'class="active"':'';?>>
                                    <a href="<?=base_url('admin/keusiswa/set')?>"><i class="fa fa-cog" aria-hidden="true"></i> Pengaturan Keuangan Siswa</a>
                                </li>
                                <li <?=($this->session->flashdata('act2')=='his') ? 'class="active"':'';?>>
                                    <a href="<?=base_url('admin/keusiswa/his')?>"><i class="fa fa-history" aria-hidden="true"></i> Riwayat Keuangan Siswa</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-book" aria-hidden="true"></i> <span>Buku Kas</span>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="#"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Daftar Buku Kas</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Pengaturan Akun</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-history" aria-hidden="true"></i> Riwayat Buku Kas</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-h-square" aria-hidden="true"></i> <span>Data Bantuan</span>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="#"><i class="fa fa-plus-square-o" aria-hidden="true"></i>Daftar Dana Bantuan</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-history" aria-hidden="true"></i> Riwayat Dana Bantuan</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-tasks" aria-hidden="true"></i> <span>Laporan</span>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="#"><i class="fa fa-book" aria-hidden="true"></i> Buku Kas</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-book" aria-hidden="true"></i> Dana Bantuan</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-book" aria-hidden="true"></i> Pendapatan dan Beban</a>
                                </li>
                            </ul>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-th-list" aria-hidden="true"></i> <span>Data RAPBS</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-question-circle" aria-hidden="true"></i> <span>Credits</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" id="logout1">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </section>
