<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Asti Dharma</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?= base_url('vendor/') ?>css/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?= base_url('vendor/') ?>css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?= base_url('vendor/') ?>css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?= base_url('vendor/') ?>img/favicon.ico">
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <!-- Font Icons CSS-->
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>
    <div class="page login-page">
        <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <div class="row">
                    <!-- Logo & Information Panel-->
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center">
                            <div class="content">
                                <div class="logo">
                                    <h1>Selamat Datang</h1>
                                    <p>
                                        <div style="text-align: right">
                                            <h1>di Web
                                                Yayasan Asti Dharma
                                            </h1>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Form Panel    -->
                    <div class="col-lg-6 bg-white">
                        <div class="form d-flex align-items-center">
                            <div class="content">
                                <form id="login-form" method="post">
                                    <?php
                                        $test=$this->session->flashdata('cek');
                                        if($test){
                                            ?>
                                                <div class="alert alert-danger">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <strong>Gagal!</strong> <?=$test?>
                                                </div>
                                            <?php
                                        }
                                    ?>
                                    <div class="form-group">
                                        <input id="login-username" type="text" name="usname" class="input-material" autocomplete="off" required>
                                        <label for="login-username" class="label-material">Username</label>
                                    </div>
                                    <div class="form-group">
                                        <input id="login-password" type="password" name="pname" class="input-material" autocomplete="off" required>
                                        <label for="login-password" class="label-material">Password</label>
                                    </div><input type="submit" value="Login" class="btn btn-primary">
                                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyrights text-center">
            <p><?php 
                $year = date("Y");
                $year2 = $year + 1;
                echo '<h5>Copyright &copy; ' . $year . ' Yayasan Asti Dharma<br>Developed by</h5>';
                ?>
                <script type="text/javascript">
                    function openInNewTab(url) {
                        var win = window.open(url, '_blank');
                        win.focus();
                    }
                </script>
                <div style="color:white" onclick="openInNewTab('http://stikomyos.ac.id');">STIKOM Yos Sudarso Purwokerto</div>
            </p>
        </div>
    </div>
    <!-- Javascript files-->
    <script src="<?= base_url('vendor/') ?>js/jquery.js"></script>
    <script src="<?= base_url('vendor/') ?>js/jquery.min.js"></script>
    <script src="<?= base_url('vendor/') ?>js/jquery.cookie.js"> </script>
    <script src="<?= base_url('vendor/') ?>js/jquery.validate.min.js"></script>
    <script src="<?= base_url('vendor/') ?>js/front.js"></script>
    <script src="<?= base_url('vendor/') ?>js/tether.min.js"></script>
    <script src="<?= base_url('vendor/') ?>js/bootstrap.min.js"></script>
</body>

</html> 