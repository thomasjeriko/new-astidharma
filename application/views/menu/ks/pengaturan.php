<section class="content-header">
    <h1>
        Pengaturan Keuangan Siswa
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan Siswa</li>
        <li class="active">Pengaturan</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tambah Tagihan Siswa</h3>
        </div>
        <div class="panel-body">
            <form id="add" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Sekolah</label>
                            <select name="sekolah" id="sekolah" class="form-control" required>
                            </select>
                        </div>
						<div class="form-group">
                            <label for="">Tahun Ajaran</label>
                            <select name="tahun" id="tahun" class="form-control" required>
                            </select>
                        </div>
						<div class="form-group">
                            <label for="">SPP per Bulan</label>
                            <input type="number" class="form-control" name="spp" id="spp" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Siswa</label>
                            <select name="siswa" id="siswa" class="form-control" disabled="true" required>
                            </select>
                        </div>
						<div class="form-group">
                            <label for="">DPP</label>
                            <input type="number" class="form-control" name="dpp" id="dpp" required>
                        </div>
						<div class="form-group">
                            <label for="">Uang Kegiatan</label>
                            <input type="number" class="form-control" name="kegiatan" id="kegiatan" required>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="tambah"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
            </form>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" id="profile">Data Tagihan Siswa</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <div id="con"></div>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section> 
