<section class="content-header">
    <h1>
        Keuangan Siswa/Riwayat Transaksi
        <small>Daftar Keuangan Siswa/Riwayat Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Keuangan Siswa/Riwayat Transaksi</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" id="profile">Riwayat Transaksi</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <div id="con"></div>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section> 
