<section class="content-header">
    <h1>
        Siswa
        <small>Daftar Siswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Siswa</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tambah Siswa</h3>
        </div>
        <div class="panel-body">
            <form id="add" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">NISN Siswa</label>
                            <input type="number" class="form-control" name="nisn_pl" id="nama" placeholder="Masukan NISN Siswa" required>
                        </div>
                        <div class="form-group">
                            <label for="">Alamat Siswa</label>
                            <textarea class="form-control" name="alamat_pl" id="nama" placeholder="Masukan Alamat Siswa" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Tahun Masuk</label>
                            <input type="number" class="form-control" min="2015" max="9999" name="tahun_masuk" id="nama" placeholder="Masukan Tahun" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Siswa</label>
                            <input type="text" class="form-control" name="nama_pl" id="nama" placeholder="Masukan Nama Siswa" required>
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Kelamin</label>
                            <select name="jk_pl" id="jk_pl" class="form-control" required>
                                <option value="">Pilih Sekolah</option>
                                <option value="P">Perempuan</option>
                                <option value="L">Laki-laki</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Sekolah</label>
                            <select name="id_schp" id="kategori" class="form-control" required>
                                <option value="">Pilih Sekolah</option>
                                <?php
                                foreach ($kategori as $key) {
                                    ?>
                                <option value="<?= $key->id_schplc ?>"><?= $key->sch_name ?></option>
                                <?php

                            }
                            ?>
                            </select>
                        </div>
                        <input type="hidden" name="status" value="1">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="tambah"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
            </form>
        </div>
    </div>

    <div class="box box-primary"> 
        <div class="box-header with-border">
            <h3 class="box-title" id="profile">Data Siswa</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
                <button type="button" class="btn btn-danger" id="hapus"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <h5>Data dipilih untuk hapus : <span id="seld">0</span></h5>
            <div id="con"></div>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section> 