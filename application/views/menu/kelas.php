<section class="content-header">
    <h1>
        Kelas
        <small>Daftar Kelas pada sekolah</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kelas</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tambah Kelas</h3>
        </div>
        <div class="panel-body">
            <form id="add" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Sekolah</label>
                            <select name="id_sch" id="kategori" class="form-control" required>
                                <option value="">Pilih Sekolah</option>
                                <?php
                                foreach ($kategori as $key) {
                                    ?>
                                <option value="<?= $key->id_schplc ?>"><?= $key->sch_name ?></option>
                                <?php

                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Kelas</label>
                            <input type="text" class="form-control" name="kelas_ket" id="nama" placeholder="Masukan nama kelas" required>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="tambah"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
            </form>
        </div>
    </div>

    <div class="box box-primary"> 
        <div class="box-header with-border">
            <h3 class="box-title" id="profile">Data Kelas</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
                <button type="button" class="btn btn-danger" id="hapus"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <h5>Data dipilih untuk hapus : <span id="seld">0</span></h5>
            <div id="con"></div>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section> 