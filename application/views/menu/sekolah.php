<section class="content-header">
    <h1>
        Sekolah/Yayasan
        <small>Daftar Sekolah/Yayasan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sekolah/Yayasan</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Tambah Sekolah/Yayasan</h3>
        </div>
        <div class="panel-body">
            <form id="add" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Nama Sekolah/Yayasan</label>
                            <input type="text" class="form-control" name="sch_name" id="nama" placeholder="Masukan nama sekolah" required>
                        </div>
                        <div class="form-group">
                            <label for="">Alamat Sekolah/Yayasan</label>
                            <textarea class="form-control" name="sch_addrs" id="alamat" placeholder="Alamat Sekolah" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Kota</label>
                            <input type="text" class="form-control" name="kota" id="kota" placeholder="Masukan Kota asal" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Penanggung Jawab</label>
                            <input type="text" class="form-control" name="penjwb" id="pj" placeholder="Masukan Penanggung Jawab" required>
                        </div>
                        <div class="form-group">
                            <label for="">Kepala Sekolah/Yayasan</label>
                            <input type="text" class="form-control" name="kepala" id="kepala" placeholder="Masukan Kepala" required>
                        </div>
                        <div class="form-group">
                            <label for="">Kategori Sekolah/Yayasan</label>
                            <select name="sch_cat" id="kategori" class="form-control" required>
                                <option value="">Pilih Kategori</option>
                                <?php
                                foreach ($kategori as $key) {
                                    ?>
                                <option value="<?= $key->id_schcat ?>"><?= $key->cat_name ?></option>
                                <?php
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="tambah"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
            </form>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title" id="profile">Data Sekolah/Yayasan</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
                <button type="button" class="btn btn-danger" id="hapus"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <h5>Data dipilih untuk hapus : <span id="seld">0</span></h5>
            <div id="con"></div>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section> 