<section class="content-header">
    <h1>
        Naik Kelas/Mutasi Siswa
        <small>Daftar Siswa-Kelas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Naik Kelas/Mutasi</li>
    </ol>
</section>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="box box-primary"> 
        <div class="box-header with-border">
            <h3 class="box-title" id="profile">Data Kelas</h3>
            <div class="box-tools">
                <button type="button" class="btn btn-info" id="refresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
                <button type="button" class="btn btn-danger" id="hapus"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
            </div>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <h5>Data dipilih untuk hapus : <span id="seld">0</span></h5>
            <div id="con"></div>
        </div>
    </div>
    <!-- /.row -->
    <!-- Main row -->
</section> 